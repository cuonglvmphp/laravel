<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use DB;
use View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
            Schema::defaultStringLength(191);
        $ma_nhung_header = DB::table('embed_code')->where('em_option', 1)->first();
        $ma_nhung_body   = DB::table('embed_code')->where('em_option', 2)->first();
        $ma_nhung_footer = DB::table('embed_code')->where('em_option', 3)->first();

        $setting = DB::table('setting')->first();
        $logo    = DB::table('logo')->first();
        $video   = DB::table('video')->get();
        $more_st = DB::table('more_setting')->first();

        $cates_prod = DB::table('category')->where('cat_parent_id', 0)->get();

        View::share([
            'ma_nhung_header'   => $ma_nhung_header,
            'ma_nhung_body'     => $ma_nhung_body,
            'ma_nhung_footer'   => $ma_nhung_footer,
            'setting'           => $setting,
            'logo'              => $logo,
            'video'             => $video,
            'more_st'           => $more_st,
            'cates_prod'        => $cates_prod
        ]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
