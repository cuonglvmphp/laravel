<?php

namespace App\Http\Controllers\Frontend;

use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ShoppingCartController extends Controller
{
    public function getDis(Request $request) {
        $district = DB::table('devvn_quanhuyen')->where('matp', $request->province )->get();
        $html = '';
        foreach ($district as $val) {
            $html .= "<option value='$val->maqh'>$val->name</option>";
        }
        return $html;
    }
    public function getward(Request $request) {
        $ward = DB::table('devvn_xaphuongthitran')->where('maqh', $request->province )->get();
        $html = '';
        foreach ($ward as $val) {
            $html .= "<option value='$val->xaid'>$val->name</option>";
        }
        return $html;
    }
    public function index()
    {

        if ( \Cart::count() <= 0 ) return redirect()->back()->with('danger','Không tồn tại hoạc ko có số lượng trong giỏ hàng ');
        $cart = \Cart::content();
        $viewData = [
            'cart' => $cart
        ];

        return view('FrontEnd.cart.index',$viewData);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function add(Request $request)
    {
        $id = $request->id;
        $qty = $request->qtybutton;

        if ($id && $qty)
        {
            $product = \DB::table('products')->where('pro_id',$id)->first();

            $pice = $product->price ? $product->price * $qty : 0;
            \Cart::add(['id' => $id, 'name' => $product->pro_title, 'qty' => $qty, 'price' => $pice , 'options' => ['hinhanh' => $product->pro_avatar]]);
            // return redirect('/cart')->with('success','Thêm giỏ hàng thành công');
            return back()->with('success','Thêm giỏ hàng thành công');
        }

        return back()->with('danger','Không thành công');
    }
    public function addReturn(Request $request)
    {
        $id = $request->id;
        $qty = $request->qtybutton;

        if ($id && $qty)
        {
            $product = \DB::table('products')->where('pro_id',$id)->first();

            $pice = $product->price ? $product->price * $qty : 0;
            \Cart::add(['id' => $id, 'name' => $product->pro_title, 'qty' => $qty, 'price' => $pice , 'options' => ['hinhanh' => $product->pro_avatar]]);
            return redirect('/cart')->with('success','Thêm giỏ hàng thành công');
        }

        return back()->with('danger','Không thành công');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request)
    {
        $data   = $request->except('_token');
        $qty    = array_get($data,'qty');
        $rowId  = array_get($data,'rowID');

        foreach($qty as $key => $item)
        {
            \Cart::update($rowId[$key],$item);
        }

        return redirect()->back()->with('success','Cập nhật thành công');
    }

    public function getPay()
    {
        if ( \Cart::count() <= 0 ) return redirect()->back()->with('danger','Không tồn tại hoạc ko có số lượng trong giỏ hàng ');
        $cart = \Cart::content();
        $viewData = [
            'cart' => $cart
        ];

        return view('FrontEnd.cart.pay',$viewData);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postPay(Request $request)
    {
        $getPX = DB::table('devvn_xaphuongthitran')->where('xaid', $request->ward_dt )->first();
        $getQH = DB::table('devvn_quanhuyen')->where('maqh', $request->district_dt )->first();
        $getTP = DB::table('devvn_tinhthanhpho')->where('matp', $request->province_dt )->first();
        
        $data = [
            'tst_email'    => $request->tst_email,
            'tst_name'     => $request->tst_name,
            'tst_phone'    => $request->tst_phone,
            'tst_address'  => $request->tst_address.' - '.$getPX->name.' - '.$getQH->name.' - '.$getTP->name,
            'tst_messages' => $request->tst_messages,
            'tst_total'    => number_format((int)Cart::subtotal(0,-3,''),0,'','')
        ];
        $data['created_at'] = $data['updated_at'] = date(now());
        $idTransaction = DB::table('transaction')->insertGetId($data);
        if( ! $idTransaction)
        {
            return redirect()->back()->with('danger',' Lưu thông tin thất bại');
        }
        $cart = Cart::content();
        foreach ($cart as $item)
        {
            $info = [
                'od_transaction_id' => $idTransaction,
                'od_product_id' => $item->id,
                'od_qty' => $item->qty,
                'od_price' => $item->price
            ];
            $info['created_at'] = $info['updated_at'] = date(now());
            $idOrder = DB::table('orders')->insertGetId($info);
        }
        Cart::destroy();
        return redirect('/')->with('success','Cập nhật thành công');
    }

    public function delete($id)
    {
        \Cart::remove($id);
        return redirect()->back()->with('success',' Xoá sản phẩm trong giỏ hàng thành công');
    }
}
