<?php

namespace App\Http\Controllers\FrontEnd;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB, Session;
use Mail, Auth;
use App\User;

class PagesController extends Controller
{
    public function home()
    {
        $data['slider']     = DB::table('banner')->get();
        $data['high_pro']   = DB::table('products')->where('pro_hl', 1)->orderby('pro_id', 'DESC')->limit(6)->get();
        $data['san_pham_moi'] = DB::table('products')->orderby('pro_id', 'DESC')->limit(6)->get();
        $data['khuyen_mai']   = DB::table('products')->whereNotNull('pro_sales')->orderby('pro_id', 'DESC')->limit(6)->get();
        $data['danh_gia_cao']   = DB::table('products')->orderby('pro_rate', 'DESC')->limit(6)->get();
    	return view('FrontEnd.pages.home', $data);
    }
    public function getCategory($id, $slug)
    {
        $getCate = DB::table('category')->where('cat_id', $id)->first();
        if ($getCate->cat_parent_id == 0) {
            $getCateChil = DB::table('category')->where('cat_parent_id', $id)->get();
            $arrID = [];
            foreach ($getCateChil as $value)
            {
                $arrID[] = $value->cat_id;
            }
            $data['products']    = DB::table('products')->whereIn('pro_cate', $arrID)->orWhere('pro_cate', $id)->orderby('pro_id', 'DESC')->paginate(18);
        }
        else
        {
            $data['products']    = DB::table('products')->where('pro_cate', $id)->orderby('pro_id', 'DESC')->paginate(18);
        }
        $data['catas_by_id'] = $getCate;
        $data['cat_id'] = $id;
        return view('FrontEnd.pages.category', $data);
    }
    public function locSP(Request $request)
    {
        $id = $request->cat_id;
        $getCate = DB::table('category')->where('cat_id', $id)->first();
        if ($getCate->cat_parent_id == 0) {
            $arrID = [];
            $getCateChil = DB::table('category')->where('cat_parent_id', $id)->get();
            foreach ($getCateChil as $value)
            {
                $arrID[] = $value->cat_id;
            }
            $products = DB::table('products')->whereIn('pro_cate', $arrID)->orWhere('pro_cate', $id);
        }
        else
        {
            $products = DB::table('products')->where('pro_cate', $id);
        }

        if($request->thuong_hieu)
        {
            $th = $request->thuong_hieu;
            $arrth = explode(',',$th);
            $products = $products->whereIn('pro_thuonghieu', $arrth );
        }

        if($request->xuatxu)
        {
            $xx = $request->xuatxu;
            $arrxx = explode(',',$xx);
            $products->whereIn('pro_xuatxu', $arrxx );
        }
        if($request->khoang_gia)
        {
            $kg = explode('_',$request->khoang_gia);
            $products->whereBetween('price', [ $kg[0], $kg[1] ]);
        }
        
        $queryData = $products->orderby('pro_id', 'DESC')->limit(20)->get();
        $html = '';
        if(count($queryData) > 0){
            foreach($queryData as $itemPr) {
                $html.= '<div class="single-product-item col-md-3">';
                    $html.= '<a href="/san-pham/'.$itemPr->pro_id.'-'.$itemPr->pro_slug.'.html" class="item-img">';
                        $html.= '<img src="/uploads/images/Products/'.$itemPr->pro_avatar.'" alt="'.$itemPr->pro_title.'">';
                    $html.= '</a>';
                    $html.= '<div class="item-info text-center">';
                        $html.= '<h2>';
                            $html.= '<a href="/san-pham/'.$itemPr->pro_id.'-'.$itemPr->pro_slug.'.html" class="item-title">'.$itemPr->pro_title.'</a>';
                        $html.= '</h2>';
                        $html.= '<h3 class="item-price"><span>Giá:</span>';
                if($itemPr->price == '') $html.= '<span style="color: #f33;font-weight: 600">Call</span>';
                else $html.= '<span style="color: #f33;font-weight: 600"> '.number_format((int)$itemPr->price, 0,',','.').' VND</span>';
                        $html.= '</h3>';
                    $html.= '</div>';
                $html.= '</div>';
            }
        }
        return response()->json( array('success' => true, 'html'=> $html ) );         
    }
    public function getDetails($id,$slug)
    {
        $data['comments'] = \DB::table('comments')->where('c_product_id',$id)->get();
        $data['detail']    = DB::table('products')->where('pro_id',$id)->first();
        $data['orth_prod'] = DB::table('products')->whereNotIn('pro_id', [$id])->where('pro_cate', $data['detail']->pro_cate )->orderby('pro_id', 'DESC')->limit(5)->get();
        $data['id'] =  $id;
        return view('FrontEnd.pages.details', $data);
    }
    public function list_news()
    {
    	$data['news'] = DB::table('news')->orderby('ne_id', 'DESC')->paginate(10);
    	return view('FrontEnd.pages.list_news', $data);
    }
    public function details_news($id,$slug)
    {

        $data['news'] = DB::table('news')->where('ne_id', $id)->first();
        $data['list_news'] = DB::table('news')->whereNotIn('ne_id', [$id])->orderby('ne_id', 'DESC')->limit(5)->get();
        return view('FrontEnd.pages.details_news', $data);
    }
    public function contact()
    {
        return view('FrontEnd.pages.contact');
    }
    public function technology()
    {
        return view('FrontEnd.pages.technology');
    }
    public function introduce()
    {
        return view('FrontEnd.pages.introduce');
    }
    public function product()
    {
        return view('FrontEnd.pages.product');
    }

    public function news1(){
        return view('FrontEnd.pages.loiichdualuoi');
    }
    public function news2(){
        return view('FrontEnd.pages.trongdua');
    }
    public function news3(){
        return view('FrontEnd.pages.tuoinuoc');
    }
    public function news4(){
        return view('FrontEnd.pages.dualuoinhamang');
    }

    public function search(Request $request)
    {
        $keyword = trim($request->keyword);
        $cates = $request->cate;

        $data['keyword'] = $keyword;
        $data['cates']   = $cates;

        $query =  DB::table('products');
        if($cates)
        {
            $query = $query->where('pro_cate', $cates);
        }
        if($keyword)
        {
            $query = $query->where('pro_title','LIKE',"%".$keyword."%");
        }
        $data['products'] = $query->paginate(32);
        return view('FrontEnd.pages.search', $data);
    }

    public function save_comment(Request $request)
    {
        $data = $request->except(['_token', 'rating']);
        if(in_array(null, $data, true)){
            // 
        }
        else
        {
            $data['created_at'] = $data['updated_at'] = Carbon::now();
            \DB::table('comments')->insert($data);
        }
        

        $getProduct = DB::table('products')->where('pro_id', $request->c_product_id)->first();
        $tong_danh_gia = $getProduct->pro_rate_count;
        $tong_so_sao  = $getProduct->pro_total ;
        $danh_gia_trung_binh = $getProduct->pro_rate;
        $danh_gia_1_sao = $getProduct->pro_rate_mot;
        $danh_gia_2_sao = $getProduct->pro_rate_hai;
        $danh_gia_3_sao = $getProduct->pro_rate_ba;
        $danh_gia_4_sao = $getProduct->pro_rate_bon;
        $danh_gia_5_sao = $getProduct->pro_rate_nam;

        $new_tong_dg = $tong_danh_gia+1;
        $new_tong_so_sao = $tong_so_sao+$request->rating;
        $new_trung_binh  = round(($new_tong_so_sao/$new_tong_dg), 2);

        switch ($request->rating) {
            case '1': $danh_gia_1_sao = $danh_gia_1_sao+1;
                break;
            case '2': $danh_gia_2_sao = $danh_gia_2_sao+1;
                break;
            case '3': $danh_gia_3_sao = $danh_gia_3_sao+1;
                break;
            case '4': $danh_gia_4_sao = $danh_gia_4_sao+1;
                break;
            case '5': $danh_gia_5_sao = $danh_gia_5_sao+1;
                break;

        }
        $updateProduct = DB::table('products')->where('pro_id', $request->c_product_id)->update([
            'pro_rate_count' => $new_tong_dg,
            'pro_total' => $new_tong_so_sao,
            'pro_rate' => $new_trung_binh,
            'pro_rate_mot' => $danh_gia_1_sao,
            'pro_rate_hai' => $danh_gia_2_sao,
            'pro_rate_ba' => $danh_gia_3_sao,
            'pro_rate_bon' => $danh_gia_4_sao,
            'pro_rate_nam' => $danh_gia_5_sao,
        ]);
        return redirect()->back()->with('success','Cảm ơn bạn đã đánh giá sản phẩm');
    }
    public function dkEmail(Request $request) {
         DB::table('dk_nhan_email')->insert([ 'dk_mail' => $request->email ]);
         return redirect()->back()->with('success',' Đăng ký thành công ! Cảm ơn bạn rất nhiêu ');
    }
    public function phoneCallBack(Request $request) {
         DB::table('phone_call_back')->insert([ 'ph_number' => $request->callback ]);
         return redirect()->back()->with('success',' Đăng ký thành công ! Cảm ơn bạn rất nhiêu ');
    }

    public function postlogin(Request $request)
    {
        $arr = [
            'email' => $request->email,
            'password' => $request->password
        ];
        if($request->remember == 'remember'){
            $remember = true;
        }
        else{
            $remember = false;
        }
        if(Auth::guard('user')->attempt($arr, $remember)){
            return redirect('/');
        }
        else{
            return back()->with('error', 'Tài khoản hoặc mật khẩu không đúng');
        }
    }
    public function logout()
    {
        Auth::logout();
        Session::flush();
        return redirect('/');
    }
    public function dangKy(Request $request)
    {
        $name       = trim($request->fullname);
        $email      = trim($request->email);
        $phone      = trim($request->phone);
        $address    = trim($request->address);
        $password   = trim($request->password);
        $repassword = trim($request->repassword);

        if(str_slug($name) == '' || str_slug($email) == '' || str_slug($password) == '') {
            return back()->with('error', 'Vui lòng điền đầy đủ thông tin');
        }
        if (strlen($name) < 3) {
            return back()->with('error', 'Tên phải nhiều hơn 3 ký tự');
        }
        if (strlen($password) < 3) {
            return back()->with('error', 'Mật khẩu phải nhiều hơn 3 ký tự');
        }
        if ($password != $repassword) {
            return back()->with('error', 'Mật khẩu không khớp');
        }

        $isEmail = User::where('email', $email)->count();
        if ($isEmail > 0) {
            return back()->with('error', 'Email đã được sử dụng, vui lòng nhập email khác');
        }
        $isName = User::where('name', $name)->count();
        if ($isName > 0) {
            return back()->with('error', 'Tên tài khoản đã được sử dụng, vui lòng nhập tên khác');
        }

        $idUser = User::insertGetId([
            'name'      => $name,
            'email'     => $email,
            'phone'     => $phone,
            'address'   => $address,
            'password'  => bcrypt($password)
        ]);
        if ($idUser) {
            return back()->with('error', 'Tạo mới thành công');
        }
        else {
            return back()->with('error', 'Tạo mới không thành công vui lòng kiểm tra lại');
        }
    }
    public function morePages($id, $slug)
    {
        $data['more_page'] = DB::table('more_page')->where('id', $id)->first();
        return view('FrontEnd.pages.details_pages', $data);
    }
}
