<?php

namespace App\Http\Controllers\BackEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class ThuongHieuController extends Controller
{
	public function ThuongHieu()
    {
        $data['cates'] = DB::table('thuong_hieu')->get();
        return view('BackEnd/Pages/ThuongHieu/list', $data);
    }

    public function addThuongHieu()
    {
        return view('BackEnd/Pages/ThuongHieu/add');
    }
    public function postAddThuongHieu(Request $request)
    {
        $name = trim($request->name);
        if ($name == '') {
            return back()->with('error', 'Tên danh mục không được để trống');
        }
        if (strlen($name) < 3) {
            return back()->with('error', 'Tên danh mục không được ít hơn 3 ký tự');
        }
        $isCat = DB::table('thuong_hieu')->where('cat_slug', str_slug($name))->count();

        if($isCat > 0) {
            return back()->with('error', 'Danh mục đã tồn tại');
        }
        $idCate = DB::table('thuong_hieu')->insertGetId([
            'cat_name'      => $name,
            'cat_slug'      => str_slug($name),
            'cat_parent_id' => 0
        ]);

        if ($idCate) {
            return redirect('mx-admin/thuong-hieu');
        }
        else {
            return back()->with('error', 'Tạo mới thất bại, vui lòng kiểm tra lại');
        }
    }
    
    public function deleteThuongHieu($cate_id)
    {
        DB::table('thuong_hieu')->where('cat_id', $cate_id)->delete();
        return back()->with('success', 'Đã xóa');
    }

    public function editThuongHieu($cate_id)
    {
        $data['get_cate']   = DB::table('thuong_hieu')->where('cat_id', $cate_id)->get();
        return view('BackEnd/Pages/ThuongHieu/edit', $data);
    }
    public function postEditThuongHieu(Request $request, $cate_id)
    {
        $name = trim($request->name);
        if ($name == '') {
            return back()->with('error', 'Tên thương hiệu không được để trống');
        }
        
        $isCat = DB::table('thuong_hieu')->whereNotIn('cat_id', [ $cate_id ])->where('cat_slug', str_slug($name))->count();
        if($isCat > 0) {
            return back()->with('error', 'Thương hiệu đã tồn tại');
        }
        $idCate = DB::table('thuong_hieu')->where('cat_id', $cate_id)->update([
            'cat_name'      => $name,
            'cat_slug'      => str_slug($name),
        ]);
        return redirect('mx-admin/thuong-hieu');
    }
}
