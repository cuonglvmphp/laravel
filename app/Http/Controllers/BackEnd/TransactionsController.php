<?php

namespace App\Http\Controllers\BackEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TransactionsController extends Controller
{
    public function index()
    {
        $transactions = \DB::table('transaction')->paginate(10);

        $viewData = [
            'transactions' => $transactions
        ];
        return view('BackEnd/Pages/transaction/index',$viewData);
    }

    public function delete(Request $request)
    {
        \DB::table('transaction')->where('id',$request->id)->delete();
        return redirect()->back()->with('success','Xoá thành công ');
    }

    public function view( Request $request)
    {
        $products = \DB::table('orders')
            ->leftJoin('products','products.pro_id','orders.od_product_id')
            ->where('od_transaction_id',$request->id)->get();

        return view('BackEnd/Pages/transaction/product',compact('products'));
    }

    public function edit($id)
    {
        $data['don_hang'] = \DB::table('transaction')->where('id', $id)->first();
        return view('BackEnd/Pages/transaction/edit', $data);
    }
    public function update(Request $request, $id)
    {
        $name   = $request->name;
        $email  = $request->email;
        $phone  = $request->phone;
        $adress = $request->adress;
        $notes  = $request->notes;
        $status = $request->status;
        $total_price = $request->total_price;
        \DB::table('transaction')->where('id', $id)->update([
            'tst_email'     => $email,
            'tst_name'      => $name,
            'tst_phone'     => $phone,
            'tst_address'   => $adress,
            'tst_total'     => $total_price,
            'tst_messages'  => $notes,
            'tst_status'    => $status
        ]);
        return redirect('mx-admin/don-hang');
    }
}
