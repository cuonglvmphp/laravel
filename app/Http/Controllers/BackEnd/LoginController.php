<?php

namespace App\Http\Controllers\BackEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth, Session;

class LoginController extends Controller
{
    public function login()
    {
    	return view('login');
    }
    public function postlogin(Request $request)
    {
    	$arr = [
            'name' => $request->name,
            'password' => $request->password
        ];
        if($request->remember == 'remember'){
            $remember = true;
        }
        else{
            $remember = false;
        }
        if(Auth::guard('admin')->attempt($arr, $remember)){
            return redirect('mx-admin');
        }
        else{
            return back()->with('error', 'Tài khoản hoặc mật khẩu không đúng');
        }
    }
    public function logout()
    {
        Auth::logout();
        Session::flush();
        return redirect('mx-login');
    }
}
