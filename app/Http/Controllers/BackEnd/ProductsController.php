<?php

namespace App\Http\Controllers\BackEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB, Auth;

class ProductsController extends Controller
{
    public function addProduct()
    {
        $data['cates'] = DB::table('category')->get();
        $data['xuatxu'] = DB::table('xuat_xu')->get();
        $data['thuonghieu'] = DB::table('thuong_hieu')->get();
        return view('BackEnd/Pages/Products/add', $data);
    }
    public function postAddProduct(Request $request)
    {
        $title = trim($request->title);
        $slug  = str_slug($request->title);

        if ($title == '') {
           return back()->with('error', 'Tên sản phẩm không được để trống');
        }
        $isPro = DB::table('products')->where('pro_slug', $slug)->count();
        if ($isPro > 0) {
            return back()->with('error', 'Tên sản phẩm đã tồn tại');
        }

        $arrInsert = [];

        $arrInsert['pro_title']         = $title;
        $arrInsert['pro_slug']          = $slug;
        $arrInsert['pro_tags']          = $request->tags;
        $arrInsert['pro_keyword']       = $request->keyword;
        $arrInsert['pro_description']   = $request->description;
        $arrInsert['pro_cate']          = $request->cate;
        $arrInsert['pro_xuatxu']        = $request->xuatxu;
        $arrInsert['pro_thuonghieu']    = $request->thuonghieu;
        $arrInsert['pro_code']          = $request->code_prod;
        $arrInsert['pro_sales']         = $request->sales_prod;
        $arrInsert['pro_des']           = $request->des_prod;
        $arrInsert['pro_content']       = $request->detail_prod;
        $arrInsert['pro_tech']          = $request->tech_prod;
        $arrInsert['price']             = $request->price_prod;
        $arrInsert['pro_hl']            = $request->pro_hl;
        $arrInsert['pro_auth']          = $request->pro_auth;
        $arrInsert['pro_video']         = $request->link_youtube;
        $arrInsert['pro_can_nang']      = $request->khoi_luong;


        if($imgAvatar = $request->file('avatar'))
        {
            $nameAvatar = rand().$imgAvatar->getClientOriginalName();
            $imgAvatar->move('uploads/images/Products', $nameAvatar);
            $arrInsert['pro_avatar'] = $nameAvatar;
        }

        $idPro = DB::table('products')->insertGetId($arrInsert);
        if ($idPro) {
            return redirect('mx-admin/san-pham');
        }
        else
        {
            return back()->with('error', 'Lỗi! Vui lòng kiểm tra lại');
        }
    }
    public function product()
    {
        $data['total'] = DB::table('products')->count();
        $data['products'] = DB::table('products')->orderby('pro_id', 'desc')->paginate(10);
        return view('BackEnd/Pages/Products/list', $data);
    }

    public function editProduct($pro_id)
    {
        $data['cates'] = DB::table('category')->get();
        $data['xuatxu'] = DB::table('xuat_xu')->get();
        $data['thuonghieu'] = DB::table('thuong_hieu')->get();
        $data['products'] = DB::table('products')->where('pro_id', $pro_id)->first();
        return view('BackEnd/Pages/Products/edit', $data);
    }

    public function postEditProduct(Request $request, $pro_id)
    {

        $title = trim($request->title);
        $slug  = str_slug($request->title);

        $isPro = DB::table('products')
                    ->whereNotIn('pro_id', [$pro_id])
                    ->where('pro_slug', $slug)->count();
        if ($isPro > 0) {
            return back()->with('error', 'Tên sản phẩm đã tồn tại');
        }

        $getCate = $request->cate;
        $arrTach = explode('-',$getCate);
        if($arrTach[0] == 0 ) {
            return back()->with('error', 'Không được chọn danh mục cấp 1 làm danh mục sản phẩm');
        }

        $arrUpdate = [];
        $arrUpdate['pro_title']         = $title;
        $arrUpdate['pro_slug']          = $slug;
        $arrUpdate['pro_tags']          = $request->tags;
        $arrUpdate['pro_keyword']       = $request->keyword;
        $arrUpdate['pro_description']   = $request->description;
        $arrUpdate['pro_cate']          = $request->cate;
        $arrUpdate['pro_xuatxu']        = $request->xuatxu;
        $arrUpdate['pro_thuonghieu']    = $request->thuonghieu;
        $arrUpdate['pro_code']          = $request->code_prod;
        $arrUpdate['pro_sales']         = $request->sales_prod;
        $arrUpdate['pro_des']           = $request->des_prod;
        $arrUpdate['pro_content']       = $request->detail_prod;
        $arrUpdate['pro_tech']          = $request->tech_prod;
        $arrUpdate['price']             = $request->price;
        $arrUpdate['pro_hl']            = $request->pro_hl;
        $arrUpdate['pro_video']         = $request->link_youtube;
        $arrUpdate['pro_can_nang']      = $request->khoi_luong;

        if($imgAvatar = $request->file('avatar'))
        {
            $nameAvatar = rand().$imgAvatar->getClientOriginalName();
            $imgAvatar->move('uploads/images/Products', $nameAvatar);
            $arrUpdate['pro_avatar'] = $nameAvatar;
        }
        DB::table('products')->where('pro_id', $pro_id)->update($arrUpdate);
        return redirect('mx-admin/san-pham');
    }

    public function deleteProduct($pro_id)
    {
        $getProd = DB::table('products')->where('pro_id', $pro_id);
        $getImg  = $getProd->first()->pro_avatar;
        if (file_exists(public_path('uploads/images/Products/'.$getImg))) {
            unlink(public_path('uploads/images/Products/'.$getImg));
        }
        DB::table('products')->where('pro_id', $pro_id)->delete();
        return redirect('mx-admin/san-pham');
    }
}
