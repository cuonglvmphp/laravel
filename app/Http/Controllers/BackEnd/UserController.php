<?php

namespace App\Http\Controllers\BackEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Auth,DB;

class UserController extends Controller
{
    /* Quản lý thành viên */
    public function listUsers()
    {
        $data['list_user']  = User::paginate(20);
        return view('BackEnd/Pages/Users/list', $data);
    }
    public function addUser()
    {
        return view('BackEnd/Pages/Users/add');
    }
    public function postAddUser(Request $request)
    {
        $phone      = trim($request->phone);
        $address    = trim($request->address);
        $name       = trim($request->name);
        $email      = trim($request->email);
        $password   = trim($request->password);
        if(str_slug($name) == '' || str_slug($email) == '' || str_slug($password) == '') {
            return back()->with('error', 'Vui lòng điền đầy đủ thông tin');
        }
        if (strlen($name) < 3) {
            return back()->with('error', 'Tên phải nhiều hơn 3 ký tự');
        }
        if (strlen($password) < 3) {
            return back()->with('error', 'Mật khẩu phải nhiều hơn 3 ký tự');
        }

        $isEmail = User::where('email', $email)->count();
        if ($isEmail > 0) {
            return back()->with('error', 'Email đã được sử dụng, vui lòng nhập email khác');
        }
        $isName = User::where('name', $name)->count();
        if ($isName > 0) {
            return back()->with('error', 'Tên tài khoản đã được sử dụng, vui lòng nhập tên khác');
        }

        $idUser = User::insertGetId([
            'name'      => $name,
            'email'     => $email,
            'phone'     => $phone,
            'address'   => $address,
            'password'  => bcrypt($password)
        ]);
        if ($idUser) {
            return redirect('mx-admin/quan-ly-thanh-vien');
        }
        else {
            return back()->with('error', 'Tạo mới không thành công vui lòng kiểm tra lại');
        }
    }

    public function editUser($user_id)
    {
        $data['user_info'] = User::where('id', $user_id)->get()[0];
        return view('BackEnd/Pages/Users/edit', $data);
    }
    public function postEditUser(Request $request, $user_id)
    {
        $phone      = trim($request->phone);
        $address    = trim($request->address);
        $isUser = User::where('id', $user_id)->count();
        if ($isUser < 1) {
            return back()->with('error', 'Không tồn tại thành viên này');
        }
        $name       = trim($request->name);
        $password   = trim($request->password);
        $email      = trim($request->email);
        if(str_slug($name) == '' || str_slug($email) == '') {
            return back()->with('error', 'Vui lòng điền đầy đủ thông tin');
        }
        if (strlen($name) < 3) {
            return back()->with('error', 'Tên phải nhiều hơn 3 ký tự');
        }
        if ($password != '' && strlen($password) < 3) {
            return back()->with('error', 'Mật khẩu phải nhiều hơn 3 ký tự');
        }
        $isEmail = User::whereNotIn('id', [ $user_id ])->where('email', $email)->count();
        if ($isEmail > 0) {
            return back()->with('error', 'Email đã được sử dụng, vui lòng nhập email khác');
        }
        $isName = User::whereNotIn('id', [ $user_id ])->where('name', $name)->count();
        if ($isName > 0) {
            return back()->with('error', 'Tên tài khoản đã được sử dụng, vui lòng nhập tên khác');
        }
        $arrUpdate = [
            'name'  => $name,
            'email' => $email,
            'phone'     => $phone,
            'address'   => $address,
        ];
        if($password != '' && strlen($password) >= 3 ) {
            $arrUpdate['password'] = bcrypt($password);
        }
        User::where('id', $user_id)->update($arrUpdate);
        return redirect('mx-admin/quan-ly-thanh-vien');
    }
    public function deleteUser($user_id)
    {
        $isUser = User::where('id', $user_id)->count();
        if ($isUser < 1) {
            return back()->with('error', 'Không tồn tại thành viên này');
        }
        User::where('id', $user_id)->delete();
        return redirect('mx-admin/quan-ly-thanh-vien');
    }

    /****************************************************************************/

    /* Tài khoản quản trị  */
    public function listAdmin()
    {
        $data['list_user']  = DB::table('sp_admin')->paginate(20);
        return view('BackEnd/Pages/Users/list_admin', $data);
    }
    public function addAdmin()
    {
        return view('BackEnd/Pages/Users/add_admin');
    }
    public function postAddAdmin(Request $request)
    {
        $name       = trim($request->name);
        $email      = trim($request->email);
        $password   = trim($request->password);
        if(str_slug($name) == '' || str_slug($email) == '' || str_slug($password) == '') {
            return back()->with('error', 'Vui lòng điền đầy đủ thông tin');
        }
        if (strlen($name) < 3) {
            return back()->with('error', 'Tên phải nhiều hơn 3 ký tự');
        }
        if (strlen($password) < 3) {
            return back()->with('error', 'Mật khẩu phải nhiều hơn 3 ký tự');
        }

        $isEmail = DB::table('sp_admin')->where('email', $email)->count();
        if ($isEmail > 0) {
            return back()->with('error', 'Email đã được sử dụng, vui lòng nhập email khác');
        }
        $isName = DB::table('sp_admin')->where('name', $name)->count();
        if ($isName > 0) {
            return back()->with('error', 'Tên tài khoản đã được sử dụng, vui lòng nhập tên khác');
        }

        $idUser = DB::table('sp_admin')->insertGetId([
            'name'      => $name,
            'email'     => $email,
            'password'  => bcrypt($password)
        ]);
        if ($idUser) {
            return redirect('mx-admin/ban-quan-tri');
        }
        else {
            return back()->with('error', 'Tạo mới không thành công vui lòng kiểm tra lại');
        }
    }
    public function editAdmin($user_id)
    {
        $data['user_info'] = DB::table('sp_admin')->where('id', $user_id)->first();
        return view('BackEnd/Pages/Users/edit_admin', $data);
    }
    public function postEditAdmin(Request $request, $user_id)
    {
        $isUser = DB::table('sp_admin')->where('id', $user_id)->count();
        if ($isUser < 1) {
            return back()->with('error', 'Không tồn tại thành viên này');
        }
        $name       = trim($request->name);
        $password   = trim($request->password);
        $email      = trim($request->email);
        if(str_slug($name) == '' || str_slug($email) == '') {
            return back()->with('error', 'Vui lòng điền đầy đủ thông tin');
        }
        if (strlen($name) < 3) {
            return back()->with('error', 'Tên phải nhiều hơn 3 ký tự');
        }
        if ($password != '' && strlen($password) < 3) {
            return back()->with('error', 'Mật khẩu phải nhiều hơn 3 ký tự');
        }
        $isEmail = DB::table('sp_admin')->whereNotIn('id', [ $user_id ])->where('email', $email)->count();
        if ($isEmail > 0) {
            return back()->with('error', 'Email đã được sử dụng, vui lòng nhập email khác');
        }
        $isName = DB::table('sp_admin')->whereNotIn('id', [ $user_id ])->where('name', $name)->count();
        if ($isName > 0) {
            return back()->with('error', 'Tên tài khoản đã được sử dụng, vui lòng nhập tên khác');
        }
        $arrUpdate = [
            'name'  => $name,
            'email' => $email,
        ];
        if($password != '' && strlen($password) >= 3 ) {
            $arrUpdate['password'] = bcrypt($password);
        }
        DB::table('sp_admin')->where('id', $user_id)->update($arrUpdate);
        return redirect('mx-admin/ban-quan-tri');
    }
    public function deleteAdmin($user_id)
    {
        $isUser = DB::table('sp_admin')->where('id', $user_id)->count();
        if ($isUser < 1) {
            return back()->with('error', 'Không tồn tại thành viên này');
        }
        DB::table('sp_admin')->where('id', $user_id)->delete();
        return redirect('mx-admin/ban-quan-tri');
    }
}
