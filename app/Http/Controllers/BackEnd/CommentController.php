<?php

namespace App\Http\Controllers\BackEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CommentController extends Controller
{
    public function index()
    {
        $comments = \DB::table('comments')->paginate(10);

        $viewData = [
            'comments' => $comments
        ];
        return view('BackEnd/Pages/comments/index',$viewData);
    }

    public function delete(Request $request)
    {
        \DB::table('comments')->where('id',$request->id)->delete();
        return redirect()->back()->with('success','Xoá thành công ');
    }

}
