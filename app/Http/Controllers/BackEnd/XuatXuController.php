<?php

namespace App\Http\Controllers\BackEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class XuatXuController extends Controller
{
   	public function XuatXu()
    {
        $data['cates'] = DB::table('xuat_xu')->get();
        return view('BackEnd/Pages/XuatXu/list', $data);
    }

    public function addXuatXu()
    {
        return view('BackEnd/Pages/XuatXu/add');
    }
    public function postAddXuatXu(Request $request)
    {
        $name = trim($request->name);
        if ($name == '') {
            return back()->with('error', 'Tên không được để trống');
        }
        if (strlen($name) < 3) {
            return back()->with('error', 'Tên không được ít hơn 3 ký tự');
        }
        $isCat = DB::table('xuat_xu')->where('cat_slug', str_slug($name))->count();

        if($isCat > 0) {
            return back()->with('error', 'Danh mục đã tồn tại');
        }
        $idCate = DB::table('xuat_xu')->insertGetId([
            'cat_name'      => $name,
            'cat_slug'      => str_slug($name),
            'cat_parent_id' => 0
        ]);

        if ($idCate) {
            return redirect('mx-admin/xuat-xu');
        }
        else {
            return back()->with('error', 'Tạo mới thất bại, vui lòng kiểm tra lại');
        }
    }
    
    public function deleteXuatXu($cate_id)
    {
        DB::table('xuat_xu')->where('cat_id', $cate_id)->delete();
        return back()->with('success', 'Đã xóa');
    }

    public function editXuatXu($cate_id)
    {
        $data['get_cate']   = DB::table('xuat_xu')->where('cat_id', $cate_id)->get();
        return view('BackEnd/Pages/XuatXu/edit', $data);
    }
    public function postEditXuatXu(Request $request, $cate_id)
    {
        $name = trim($request->name);
        if ($name == '') {
            return back()->with('error', 'Tên không được để trống');
        }
        
        $isCat = DB::table('xuat_xu')->whereNotIn('cat_id', [ $cate_id ])->where('cat_slug', str_slug($name))->count();
        if($isCat > 0) {
            return back()->with('error', 'Đã tồn tại');
        }
        $idCate = DB::table('xuat_xu')->where('cat_id', $cate_id)->update([
            'cat_name'      => $name,
            'cat_slug'      => str_slug($name),
        ]);
        return redirect('mx-admin/xuat-xu');
    }
}
