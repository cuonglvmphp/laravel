<?php

namespace App\Http\Controllers\BackEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class QLPagesController extends Controller
{
	public function listPages()
    {
    	return view('BackEnd/Pages/Widget/list_page');
    }
    public function updateContentPages($id)
    {
    	$data['more_page'] = DB::table('more_page')->where('id', $id)->first();
    	return view('BackEnd/Pages/Widget/edit_page', $data);
    }
    public function updateContent(Request $request, $id)
    {
    	$content =  $request->content;
    	DB::table('more_page')->where('id', $id)->update([
    		'content' => $content
    	]);
    	return back();
    }
}
