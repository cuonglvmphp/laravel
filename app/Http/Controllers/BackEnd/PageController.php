<?php

namespace App\Http\Controllers\BackEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use DB;

class PageController extends Controller
{
    public function nhanDK()
    {
        $data['email_dk'] = DB::table('dk_nhan_email')->paginate(20);
        return view('BackEnd/Pages/nhan_dk', $data);
    }

    public function xoaNhanDK($id)
    {
        DB::table('dk_nhan_email')->where('dk_id', $id)->delete();
        return back();
    }

    public function phoneCall()
    {
        $data['phone'] = DB::table('phone_call_back')->paginate(20);
        return view('BackEnd/Pages/phone_callback', $data);
    }

    public function xoaphoneCall($id)
    {
        DB::table('phone_call_back')->where('ph_id', $id)->delete();
        return back();
    }

    public function dashboard()
    {
        return view('BackEnd/Pages/dashboard');
    }

    public function settingWebsite()
    {
        $data['st_info'] = DB::table('setting')->first();
        return view('BackEnd/Pages/setting_website', $data);
    }

    public function postSettingWebsite(Request $request)
    {
        $arrInsert = [
            'st_title' => trim($request->title),
            'st_company' => trim($request->company),
            'st_address' => trim($request->address),
            'st_phone' => trim($request->phone),
            'st_phone2' => trim($request->phone2),
            'st_email' => trim($request->email),
            'st_facebook' => trim($request->facebook),
            'st_google' => trim($request->google)
        ];
        DB::table('setting')->delete();
        $idIn = DB::table('setting')->insertGetId($arrInsert);
        if ($idIn) {
            return redirect()->back()->with('success', ' Cập nhập thành công !');
        }else{
            return redirect()->back()->with('success', ' Cập nhập thất bại, Vui lòng thử lại !');
        }
    }

    public function banner()
    {
        $data['banner'] = DB::table('banner')->orderby('bn_id', 'DESC')->limit(5)->get();
        return view('BackEnd/Pages/Banner/setting', $data);
    }

    public function setBanner(Request $request)
    {
        $getID = $request->id_bn;
        if ($getID == null) {
            DB::table('banner')->delete();
        } elseif (count($getID) > 0) {
            DB::table('banner')->whereNotIn('bn_id', $getID)->delete();
        }
        if ($files = $request->file('avatar')) {
            foreach ($files as $file) {
                $name = rand() . $file->getClientOriginalName();
                $file->move('uploads/images/banner', $name);
                DB::table('banner')->insert(['bn_image' => $name]);
            }
        }
        return redirect()->back()->with('success', ' Cập nhập thành công !');
    }

    public function video()
    {
        return view('BackEnd/Pages/Video/setting');
    }

    public function postVideo(Request $request)
    {
        $ma_nhung = trim($request->ma_nhung_youtube);
        if ($ma_nhung == '') {
            return back()->with('error', 'Bạn chưa dán iframe');
        }
        DB::table('video')->insertGetId([
            'vd_src' => $ma_nhung
        ]);
        return redirect('mx-admin/video');
    }

    public function updateVideo(Request $request, $id_video)
    {
        dd('1111');
        $ma_nhung = trim($request->ma_nhung_youtube);
        if ($ma_nhung == '') {
            return back()->with('error', 'Bạn chưa dán iframe');
        }
        DB::table('video')->where('vd_id', $id_video)->update([
            'vd_src' => $ma_nhung
        ]);
        return redirect('mx-admin/video');
    }

    public function listVideo()
    {
        $data['video'] = DB::table('video')->paginate(4);
        return view('BackEnd/Pages/Video/list', $data);
    }

    public function deleteVideo($id_video)
    {
        DB::table('video')->where('vd_id', $id_video)->delete();
        return redirect('mx-admin/video');
    }

    public function logo()
    {
        $data['picture'] = DB::table('logo')->get();
        return view('BackEnd/Pages/Logo/setting', $data);
    }

    public function postLogo(Request $request)
    {
        DB::table('logo')->delete();

        if ($imgLogo = $request->file('img_logos')) {
            $name = rand() . $imgLogo->getClientOriginalName();
            $idLogo = DB::table('logo')->insertGetId([
                'lg_image' => $name
            ]);
            if ($idLogo) {
                $imgLogo->move('images', $name);
            }
        }
        return back()->with('success', ' Cập nhập thành công !');
    }

    public function widgetSetting()
    {
        $data['getCode'] = DB::table('more_setting')->first();
        return view('BackEnd/Pages/Widget/setting', $data);
    }

    public function pWidgetSetting(Request $request)
    {
        $getCode = DB::table('more_setting');
        $select = $request->option_sl;
        if ($select == 0) {
            // Google Maps
            if ($getCode->count() < 1) {
                $getCode->insert([
                    'ms_maps' => $request->maps
                ]);
            } else {
                $getCode->update([
                    'ms_maps' => $request->maps
                ]);
            }
        }
        if ($select == 1) {
            // Facebook
            if ($getCode->count() < 1) {
                $getCode->insert([
                    'ms_face' => $request->plugin_fb
                ]);
            } else {
                $getCode->update([
                    'ms_face' => $request->plugin_fb
                ]);
            }
        }
        return back();
    }

    public function popupLeftRight()
    {
        $data['getPopupLeft'] = DB::table('popup')->where('pp_type', 1)->first();
        $data['getPopupRight'] = DB::table('popup')->where('pp_type', 2)->first();
        return view('BackEnd/Pages/popup_left_right', $data);
    }

    public function updatePopupLeftRight(Request $request)
    {
        $arrDataLeft = [];
        $acPopUpLeft = $request->ac_left;
        if ($acPopUpLeft == 'on') {
            $arrDataLeft['pp_active'] = 1;
        }

        $arrDataRight = [];
        $acPopUpRight = $request->ac_right;
        if ($acPopUpRight == 'on') {
            $arrDataRight['pp_active'] = 1;
        }

        if ($imgLeft = $request->file('pop_left')) {
            $nameL = rand() . $imgLeft->getClientOriginalName();
            $arrDataLeft['pp_img'] = $nameL;
            $imgLeft->move('images', $nameL);
        }

        if ($imgRight = $request->file('pop_right')) {
            $nameR = rand() . $imgRight->getClientOriginalName();
            $arrDataRight['pp_img'] = $nameR;
            $imgRight->move('images', $nameR);
        }

        $arrDataLeft['pp_type'] = 1;
        $isLe = DB::table('popup')->where('pp_type', 1);
        if (count($isLe->get()) > 0) {
            $isLe->update($arrDataLeft);
        } else {
            $isLe->insert($arrDataLeft);
        }

        $arrDataRight['pp_type'] = 2;
        $isRI = DB::table('popup')->where('pp_type', 2);
        if (count($isRI->get()) > 0) {
            $isRI->update($arrDataRight);
        } else {
            $isRI->insert($arrDataRight);
        }
        return back();
    }

    public function vanchuyen()
    {
        $data['le_phi'] = DB::table('phi_van_chuyen')->first();
        return view('BackEnd.Pages.van_chuyen', $data);
    }

    public function updatevanchuyen(Request $request)
    {
        DB::table('phi_van_chuyen')->update([
            'phi' => $request->phi_van_chuyen
        ]);
        return back();
    }
}
