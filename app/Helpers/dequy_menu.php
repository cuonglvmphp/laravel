<?php
/**
 * [showCategories description] Đệ quy bên quản lý danh mục
 */
function showCategories($categories, $cat_parent_id = 0, $string = '')
{
    $cate_child = array();
    foreach ($categories as $key => $item)
    {
        if ($item->cat_parent_id == $cat_parent_id)
        {
            $cate_child[] = $item;
            unset($categories[$key]);
        }
    }
    
    if ($cate_child)
    {
        foreach ($cate_child as $key => $item)
        {
            echo '<li><a class="link-cate" title="Sửa" href="sua-danh-muc/'.$item->cat_id.'">'.$string.$item->cat_name.'</a><div style="float:right">';
            echo '<a class="link-cate" title="Sửa" href="sua-danh-muc/'.$item->cat_id.'"><i class="fa fa-pencil"></i> Sửa </a>&nbsp;&nbsp;';
            echo '&nbsp;&nbsp;<a class="link-cate" title="Xóa" href="xoa-danh-muc/'.$item->cat_id.'"><i class="fa fa-remove"></i> Xóa </a>';
            echo '</div>';

                showCategories($categories, $item->cat_id, $string.'---| ');
            echo '</li>';
        }
    }
}

function showCategoriesTH($categories, $cat_parent_id = 0, $string = '')
{
    $cate_child = array();
    foreach ($categories as $key => $item)
    {
        if ($item->cat_parent_id == $cat_parent_id)
        {
            $cate_child[] = $item;
            unset($categories[$key]);
        }
    }
    
    if ($cate_child)
    {
        foreach ($cate_child as $key => $item)
        {
            echo '<li><a class="link-cate" title="Sửa" href="sua-thuong-hieu/'.$item->cat_id.'">'.$string.$item->cat_name.'</a><div style="float:right">';
            echo '<a class="link-cate" title="Sửa" href="sua-thuong-hieu/'.$item->cat_id.'"><i class="fa fa-pencil"></i> Sửa </a>&nbsp;&nbsp;';
            echo '&nbsp;&nbsp;<a class="link-cate" title="Xóa" href="xoa-thuong-hieu/'.$item->cat_id.'"><i class="fa fa-remove"></i> Xóa </a>';
            echo '</div>';

                showCategoriesTH($categories, $item->cat_id, $string.'---| ');
            echo '</li>';
        }
    }
}

function showCategoriesXX($categories, $cat_parent_id = 0, $string = '')
{
    $cate_child = array();
    foreach ($categories as $key => $item)
    {
        if ($item->cat_parent_id == $cat_parent_id)
        {
            $cate_child[] = $item;
            unset($categories[$key]);
        }
    }
    
    if ($cate_child)
    {
        foreach ($cate_child as $key => $item)
        {
            echo '<li><a class="link-cate" title="Sửa" href="sua-xuat-xu/'.$item->cat_id.'">'.$string.$item->cat_name.'</a><div style="float:right">';
            echo '<a class="link-cate" title="Sửa" href="sua-xuat-xu/'.$item->cat_id.'"><i class="fa fa-pencil"></i> Sửa </a>&nbsp;&nbsp;';
            echo '&nbsp;&nbsp;<a class="link-cate" title="Xóa" href="xoa-xuat-xu/'.$item->cat_id.'"><i class="fa fa-remove"></i> Xóa </a>';
            echo '</div>';

                showCategoriesXX($categories, $item->cat_id, $string.'---| ');
            echo '</li>';
        }
    }
}

function checkBoxCate($categories, $cat_parent_id = 0, $string = '', $cate = null)
{
    $cate_child = array();
    foreach ($categories as $key => $item)
    {
        if ($item->cat_parent_id == $cat_parent_id)
        {
            $cate_child[] = $item;
            unset($categories[$key]);
        }
    }

    if ($cate_child)
    {
        foreach ($cate_child as $key => $item)
        {
            if($cate != null) {
                $arrCate = explode(',', $cate);
                if (in_array( $item->cat_id, $arrCate )) {
                    $checked = 'checked';
                }
                else
                {
                    $checked = '';
                }
            }
            else { $checked = ''; }

            echo '<div class="checkbox checkbox-info checkbox-circle">
                    <input id="checkbox'.$item->cat_id.'" type="checkbox" '.$checked.' name="cate[]" multiple value="'.$item->cat_id.'" style="outline: none!important">
                    <label for="checkbox'.$item->cat_id.'" style="font-weight: 100!important">'.$string.$item->cat_name.'</label>
                </div>';
            checkBoxCate($categories, $item->cat_id, $string.' ---| ', $cate);
        }
    }
}

function selectCates($categories, $cat_parent_id = 0, $string = '', $cate = null)
{
    $cate_child = array();
    foreach ($categories as $key => $item)
    {
        if ($item->cat_parent_id == $cat_parent_id)
        {
            $cate_child[] = $item;
            unset($categories[$key]);
        }
    }

    if ($cate_child)
    {
        
        foreach ($cate_child as $key => $item)
        {
            if($item->cat_id == $cate) {
                $checked = 'selected';
            }
            else { $checked = ''; }
            echo '<option '.$checked.' value="'.$item->cat_id.'">'.$string.$item->cat_name.'</option>';
            selectCates($categories, $item->cat_id, $string.'---| ', $cate);
        }
    }
}