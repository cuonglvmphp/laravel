<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="{{ asset('backend/css/bootstrap.min.css') }}" />
<link rel="stylesheet" href="{{ asset('backend/css/bootstrap-responsive.min.css') }}" />
<link rel="stylesheet" href="{{ asset('backend/css/fullcalendar.css') }}" />
<link rel="stylesheet" href="{{ asset('backend/css/matrix-style.css') }}" />
<link rel="stylesheet" href="{{ asset('backend/css/matrix-media.css') }}" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="{{ asset('backend/css/maxgo-admin.css') }}"  />
<link rel="stylesheet" href="{{ asset('backend/css/checkbox.css') }}"  />