<!DOCTYPE html>
<html lang="en">
<head>
    <title>@yield('title')</title>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    @include('BackEnd.Vendor.inc_css')
</head>
<body>
<style type="text/css">
    table, tr, th, td {
        background: #fff !important;
    }

    .text-center {
        text-align: center !important;
    }
</style>
<div id="header">
    <h1>
        <a href="{{ url('/')}}">
            <img src="{{ asset('backend/img/artboard.png') }}">
        </a>
    </h1>
</div>
@include('BackEnd.LayOut.nav_top')
@include('BackEnd.LayOut.sidebar')
@yield('main-content')
@include('BackEnd.Vendor.inc_js')
<div class="row-fluid">
    <div id="footer" class="span12"> 2018 &copy; Tuấn Anh nVa</div>
</div>
<script type="text/javascript">
    function goPage(newURL) {
        if (newURL != "") {
            if (newURL == "-") {
                resetMenu();
            } else {
                document.location.href = newURL;
            }
        }
    }

    function resetMenu() {
        document.gomenu.selector.selectedIndex = 2;
    }
</script>
@yield('script')
</body>
</html>