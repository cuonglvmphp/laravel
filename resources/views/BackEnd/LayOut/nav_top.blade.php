<div id="user-nav" class="navbar navbar-inverse">
    <ul class="nav">
        <li class="">
           {{--  <a href="{{ url('mx-admin/tai-khoan') }}">
                <span class="text" style="color: #eee">Xin chào  <span style="color: #fff">{{ Auth::guard('admin')->user()->name }}</span> </span>
            </a> --}}
        </li>
        <li class="">
            <a href="{{ url('/') }}" target="_blank">
                <i class="fa fa-desktop" style="color: #eee"></i>
                <span class="text" style="color: #eee"> &nbsp;Xem website </span>
            </a>
        </li>
        <li class="">
            <a href="{{ url('mx-admin/logout') }}">
                <i class="fa fa-sign-out" style="color: #eee"></i>
                <span class="text" style="color: #eee"> Đăng xuất </span>
            </a>
        </li>
    </ul>
</div>