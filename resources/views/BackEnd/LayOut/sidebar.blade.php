<div id="sidebar">
    <a href="#" class="visible-phone">
        <i class="icon icon-home"></i> Danh mục quản trị
    </a>
    <ul>
        <li class="submenu">
            <a href="javascript::void(0)">
                <i class="fa fa-gear"></i>
                <span> Cấu hình website </span>
                <span class="fa fa-angle-down" style="font-size: 12px;"></span>
            </a>
            <ul>
                <li><a href="{{ url('mx-admin/cau-hinh-chung') }}"> <i class="fa fa-angle-double-right"></i> Cấu hình chung </a></li>
                <li><a href="{{ url('mx-admin/logo') }}"> <i class="fa fa-angle-double-right"></i> Logo </a></li>
                <li><a href="{{ url('mx-admin/slider') }}"> <i class="fa fa-angle-double-right"></i> Slider </a></li>
            </ul>
        </li>

        <li>
            <a href="{{ url('mx-admin/ban-quan-tri') }}">
                <i class="fa fa-group"></i>
                <span> Ban quản trị </span>
            </a>
        </li>

        <li>
            <a href="{{ url('mx-admin/quan-ly-thanh-vien') }}">
                <i class="fa fa-group"></i>
                <span> Quản lý thành viên </span>
            </a>
        </li>
       
        <li class="submenu">
            <a href="javascript::void(0)">
                <i class="fa fa-newspaper-o"></i>
                <span> Tin tức </span>
                <span class="fa fa-angle-down" style="font-size: 12px;"></span>
            </a>
            <ul>
                <li><a href="{{ url('mx-admin/quan-ly-bai-viet') }}"> <i class="fa fa-angle-double-right"></i> Tất cả</a></li>
                <li><a href="{{ url('mx-admin/viet-bai-moi') }}"> <i class="fa fa-angle-double-right"></i> Thêm mới</a></li>
            </ul>
        </li>
        <li class="submenu">
            <a href="javascript::void(0)">
                <i class="fa fa-cart-plus"></i>
                <span> Sản Phẩm </span>
                <span class="fa fa-angle-down" style="font-size: 12px;"></span>
            </a>
            <ul>
                <li><a href="{{ url('mx-admin/san-pham') }}"> <i class="fa fa-angle-double-right"></i> Danh sách sản phẩm </a></li>
                <li><a href="{{ url('mx-admin/san-pham/them-moi') }}"> <i class="fa fa-angle-double-right"></i> Đăng sản phẩm mới </a></li>
                <li><a href="{{ url('mx-admin/danh-muc') }}"> <i class="fa fa-angle-double-right"></i> Danh mục sản phẩm </a></li>
                <li><a href="{{ url('mx-admin/thuong-hieu') }}"> <i class="fa fa-angle-double-right"></i> Thương hiệu </a></li>
                <li><a href="{{ url('mx-admin/xuat-xu') }}"> <i class="fa fa-angle-double-right"></i> Xuất xứ </a></li>
{{--                <li><a href="{{ route('AdminTransactionIndex') }}"> <i class="fa fa-angle-double-right"></i> Đơn hàng </a></li>--}}
{{--                <li><a href="{{ url('mx-admin/phi-van-chuyen') }}"> <i class="fa fa-angle-double-right"></i> Phí vận chuyển </a></li>--}}
            </ul>
        </li>

        <li class="submenu">
            <a href="javascript::void(0)">
                <i class="fa fa-snowflake-o"></i>
                <span> Khách hàng </span>
                <span class="fa fa-angle-down" style="font-size: 12px;"></span>
            </a>
            <ul>
                <li>
                    <a href="{{ url('mx-admin/email-dang-ky') }}"><i class="fa fa-angle-double-right"></i> 
                        <span>Đăng kí nhận khuyến mãi</span>
                    </a>
                </li>
                <li>
                    <a href="{{ url('mx-admin/yeu-cau-goi-lai') }}"><i class="fa fa-angle-double-right"></i> 
                        <span>Yêu cầu gọi lại</span>
                    </a>
                </li>
            </ul>
        </li>
        
        
{{--        <li>--}}
{{--            <a href="{{ route('AdminCommentIndex') }}">--}}
{{--                <i class="fa fa-comments"></i>--}}
{{--                <span>Bình luận</span>--}}
{{--            </a>--}}
{{--        </li>--}}
        <li>
            <a href="{{ url('mx-admin/quan-ly-ma-nhung') }}">
                <i class="fa fa-code"></i>
                <span>Quản lý mã nhúng</span>
            </a>
        </li>
        <li class="submenu">
            <a href="#">
                <i class="fa fa-file-video-o"></i>
                <span>Video</span>
                <span class="fa fa-angle-down" style="font-size: 12px;"></span>
            </a>
            <ul>
                <li>
                    <a href="{{ url('mx-admin/video') }}"> Tất cả </a>
                </li>
                <li>
                    <a href="{{ url('mx-admin/video/dang-moi') }}"> Đăng mới </a>
                </li>
            </ul>
        </li>
        <li>
            <a href="{{ url('mx-admin/mo-rong') }}">
                <i class="fa fa-map-o"></i>
                <span>Phần mở rộng</span>
            </a>
        </li>
        <li>
            <a href="{{ url('mx-admin/pages') }}">
                <i class="fa fa-map-o"></i>
                <span>Pages</span>
            </a>
        </li>
    </ul>
</div>