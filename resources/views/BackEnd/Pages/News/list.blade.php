@extends('BackEnd.LayOut.master')
@section('title', 'Quản lý bài viết - Hệ thống quản trị website')
@section('main-content')

<div id="content">
    <!--breadcrumbs-->
    <div id="content-header">
        <div id="breadcrumb">
            <a href="{{ url('mx-admin') }}" title="Trang chủ" class="tip-bottom">
                <i class="icon-home"></i> Trang chủ
            </a>
            <a href="">
               Tin tức
            </a>
        </div>
    </div>
    <style type="text/css">
        th, td {
            border-left: 1px solid #ccc!important;
            border-top: 1px solid #ccc!important;
        }
        table {
            border-bottom: 1px solid #ccc!important;
            border-right: 1px solid #ccc!important;
        }
    </style>

    <div class="container-fluid">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-block">
                    <h4 class="card-title"> Danh Sách Bài Viết </h4>
                    <h6 class="card-subtitle">Tổng: <code>{{ $total }}</code></h6>
                    <div>
                        <a href="{{ url('mx-admin/viet-bai-moi') }}" class="btn btn-sm btn-success use-btn"><i class="fa fa-plus"></i> Viết bài mới </a>
                    </div>
                    <div class="table-responsive" style="padding-top: 20px">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th style="width:20px">ID</th>
                                    <th class="hidden-xs" style="width:120px">Ảnh đại diện</th>
                                    <th style="width:200px;">Tiêu đề</th>
                                    <th style="width:285px;">Mô tả</th>
                                    <th class="hidden-xs">Chuyên mục</th>
                                    <th class="hidden-xs">Ngày đăng</th>
                                    <th style="width: 14.4%"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($news as $nw)
                                <tr>
                                    <td style="text-align:center;">{{ $nw->ne_id }}</td>
                                    <td class="hidden-xs text-center" style="text-align:center;width:120px">
                                        <img src="{{ asset('uploads/images/news/'.$nw->ne_avatar) }}" style="height:50px">
                                    </td>
                                    <td style="width:200px;text-align:justify">{{ $nw->ne_title }}</td>
                                    <td style="width:285px;text-align:justify">{!! $nw->ne_des !!}</td>
                                    <td style="text-align:center;width:80px"> Tin tức </td>
                                    <td class="hidden-xs" style="text-align:center;width:60px">{{ $nw->ne_created_at }}</td>
                                    <td>
                                        <a href="{{ url('mx-admin/xem-bai-viet/'.$nw->ne_id) }}" class="btn btn-sm btn-info use-btn"><i class="fa fa-eye"></i> Xem </a>
                                        <a href="{{ url('mx-admin/sua-bai-viet/'.$nw->ne_id) }}" class="btn btn-sm btn-info use-btn"><i class="fa fa-pencil"></i> Sửa </a>
                                        <a href="{{ url('mx-admin/xoa-bai-viet/'.$nw->ne_id) }}" class="btn btn-sm btn-danger use-btn"><i class="fa fa-remove"></i> Xóa </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $news->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop