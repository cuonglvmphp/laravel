@extends('BackEnd.LayOut.master')
@section('title', 'Quản lý sản phẩm - Hệ thống quản trị website')
@section('main-content')

<div id="content">
    <!--breadcrumbs-->
    <div id="content-header">
        <div id="breadcrumb">
            <a href="{{ url('mx-admin') }}" title="Trang chủ" class="tip-bottom">
                <i class="icon-home"></i> Trang chủ
            </a>
            <a href="{{ url('mx-admin/san-pham') }}">
                Sản phẩm
            </a>
            <a href="">
                Sửa
            </a>
        </div>
    </div>
    <!--End-breadcrumbs-->
    <!--Action boxes-->
    <div class="container-fluid">

        @include('Notify.note')

        <div class="row-fluid">
            <div class="widget-box">
                <div class="widget-title">
                    <h5> Thông tin sản phẩm </h5>
                </div>
                <div class="widget-content">
                    <div class="widget-content nopadding">
                        <form  class="form-horizontal form-material" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="control-group">
                                <label class="control-label">Tiêu đề:</label>
                                <div class="controls">
                                    <input type="text" class="span10" name="title" required value="{{ $products->pro_title }}">
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Ảnh đại diện:</label>
                                <div class="controls">
                                    @if($products->pro_avatar != '')
                                        <img src="{{ asset('uploads/images/Products/'.$products->pro_avatar) }}" style="max-width: 195px; display: block">
                                    @else
                                        <img src="{{ asset('backend/img/no_images.png') }}" style="max-width: 195px; display: block">
                                    @endif
                                    <input class="inp-upload-img hide" type="file" name="avatar" onchange="changeImg(this)">
                                    <span class="btn btn-sm btn-primary" onclick="$('.inp-upload-img').click()" style="display: block; max-width: 110px; margin: 20px 0"> Upload Image </span>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label"> Danh mục: </label>
                                <div class="controls">
                                    <select class="form-control form-control-line" name="cate">
                                        @php
                                            selectCates($cates, 0, ' ', $products->pro_cate);
                                        @endphp 
                                    </select>
                                    <span style="margin-left: 20px"> Sản phẩm nổi bật &nbsp<input type="checkbox" @if($products->pro_hl == 1) checked @endif name="pro_hl" value="1" style="margin: -2px 0 0"></span>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label"> Thương hiệu: </label>
                                <div class="controls">
                                    <select class="form-control form-control-line" name="thuonghieu">
                                        @php
                                            selectCates($thuonghieu, 0, ' ', $products->pro_thuonghieu);
                                        @endphp 
                                    </select>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Xuất xứ: </label>
                                <div class="controls">
                                    <select class="form-control form-control-line" name="xuatxu">
                                        @php
                                            selectCates($xuatxu, 0, ' ', $products->pro_xuatxu);
                                        @endphp 
                                    </select>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Mã sản phẩm</label>
                                <div class="controls">
                                    <input type="text" class="span10" name="ma_sp" value="{{ $products->pro_code }}">
                                </div>
                            </div>
                            
                            <div class="control-group">
                                <label class="control-label">Khuyến mãi:</label>
                                <div class="controls">
                                    <textarea name="sales_prod" class="span10">{{ $products->pro_sales }}</textarea>
                                </div>
                            </div>
                            <style type="text/css">
                                #cke_des_prod, #cke_detail_prod, #cke_tech_prod {
                                    width:82.90598290598291%;
                                }
                            </style>
                            <div class="control-group">
                                <label class="control-label">Mô tả ngắn:</label>
                                <div class="controls">
                                    <textarea name="des_prod" class="ckeditor" cols="30" rows="10" style="width: 82.90598290598291%;" required>{{ $products->pro_des }}</textarea>
                                </div>
                            </div>
                            
                            <div class="control-group">
                                <label for="control-label">Chi Tiết Sản Phẩm:</label>
                                <div class="controls">
                                   <textarea name="detail_prod" class="ckeditor" cols="30" rows="10" style="width: 82.90598290598291%;" required>{{ $products->pro_content }}</textarea>
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="control-label">Thông số kỹ thuật:</label>
                                <div class="controls">
                                   <textarea name="tech_prod" class="ckeditor" cols="30" rows="10" style="width: 82.90598290598291%;" required>{{ $products->pro_tech }}</textarea>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Sản phẩm nổi bật:</label>
                                <div class="controls">
{{--                                    <input value="{{ $products->price }}" type="number" min="0" name="price" class="form-control form-control-line" autocomplete="off" style="padding-left: 10px">--}}
                                    <input type="checkbox" {{$products->pro_auth == 1 ? 'checked' : ''}} name="pro_auth" value="1" style="margin: 8px 0 0">
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Khối lượng (*kg):</label>
                                <div class="controls">
                                    <input type="number" value="{{ $products->pro_can_nang }}" name="khoi_luong" class="form-control form-control-line" autocomplete="off" style="padding-left: 10px">
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Link Youtube: </label>
                                <div class="controls">
                                    <input value="{{ $products->pro_video }}" type="text" name="link_youtube" class="span11 form-control form-control-line" autocomplete="off" style="padding-left: 10px">
                                    <br>
                                    <span style="color:#999;font-size:12px">VD: https://youtu.be/D32eLVvn7tw</span>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Tags:</label>
                                <div class="controls">
                                    <input value="{{ $products->pro_tags }}" type="text" name="tags" class="form-control form-control-line span10" autocomplete="off" style="padding-left: 10px">
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Meta keyword:</label>
                                <div class="controls">
                                    <input value="{{ $products->pro_keyword }}" type="text" name="keyword" class="form-control form-control-line span10" autocomplete="off" style="padding-left: 10px">
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Meta description:</label>
                                <div class="controls">
                                    <textarea name="description" class="span10">{{ $products->pro_description }}</textarea>
                                </div>
                            </div>
                            
                            <div class="controls">
                                <button type="submit" class="btn btn-success"> Cập nhật </button> 
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection
@section('script')
    <script src="{{ asset('ckeditor/ckeditor.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        function changeImg(input){
            if(input.files && input.files[0]){
                var reader = new FileReader();
                reader.onload = function(e){
                    $(input).prev().attr('src',e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endsection