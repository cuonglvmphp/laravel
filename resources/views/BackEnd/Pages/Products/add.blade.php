@extends('BackEnd.LayOut.master')
@section('title', 'Đăng sản phẩm mới - Hệ thống quản trị website')
@section('main-content')

<div id="content">
    <!--breadcrumbs-->
    <div id="content-header">
        <div id="breadcrumb">
            <a href="{{ url('mx-admin') }}" title="Trang chủ" class="tip-bottom">
                <i class="icon-home"></i> Trang chủ
            </a>
            <a href="{{ url('mx-admin/san-pham') }}">
                Sản phẩm
            </a>
            <a href="">
                Đăng mới
            </a>
        </div>
    </div>
    <!--End-breadcrumbs-->
    <!--Action boxes-->
    <div class="container-fluid">

        @include('Notify.note')

        <div class="row-fluid">
            <div class="widget-box">
                <div class="widget-title">
                    <h5> Đăng mới sản phẩm </h5>
                </div>
                <div class="widget-content">
                    <div class="widget-content nopadding">
                        <form  class="form-horizontal form-material" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="control-group">
                                <label class="control-label">Tiêu đề:</label>
                                <div class="controls">
                                    <input type="text" class="span11" name="title" required>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Ảnh đại diện:</label>
                                <div class="controls">
                                    <img src="{{ asset('backend/img/no_images.png') }}" style="max-width: 195px; display: block">
                                    <input class="inp-upload-img hide" type="file" name="avatar" onchange="changeImg(this)">
                                    <span class="btn btn-sm btn-primary" onclick="$('.inp-upload-img').click()" style="display: block; max-width: 110px; margin: 20px 0"> Upload Image </span>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label"> Danh mục: </label>
                                <div class="controls">
                                    <select class="form-control form-control-line" name="cate">
                                        @php
                                            selectCates($cates);
                                        @endphp 
                                    </select>
                                    <span style="margin-left: 20px"> Sản phẩm nổi bật &nbsp<input type="checkbox" name="pro_hl" value="1" style="margin: -2px 0 0"></span>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label"> Thương hiệu: </label>
                                <div class="controls">
                                    <select class="form-control form-control-line" name="thuonghieu">
                                        @php
                                            selectCates($thuonghieu);
                                        @endphp 
                                    </select>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Xuất xứ: </label>
                                <div class="controls">
                                    <select class="form-control form-control-line" name="xuatxu">
                                        @php
                                            selectCates($xuatxu);
                                        @endphp 
                                    </select>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Mã sản phẩm</label>
                                <div class="controls">
                                    <input type="text" class="span11" name="code_prod">
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Khuyến mãi:</label>
                                <div class="controls">
                                    <textarea name="sales_prod" class="span11"></textarea>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Mô tả ngắn:</label>
                                <div class="controls">
                                    <textarea name="des_prod" class="ckeditor" cols="30" rows="10" required></textarea>
                                </div>
                            </div>
                            
                            <div class="control-group">
                                <label for="control-label">Chi Tiết Sản Phẩm:</label>
                                <div class="controls">
                                   <textarea name="detail_prod" class="ckeditor" cols="30" rows="10" required></textarea>
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="control-label">Thông số kỹ thuật:</label>
                                <div class="controls">
                                   <textarea name="tech_prod" class="ckeditor" cols="30" rows="10" required></textarea>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Sản phẩm nổi bật:</label>
                                <div class="controls">
{{--                                    <input type="number" min="0" name="price_prod" class="form-control form-control-line" autocomplete="off" value="0" style="padding-left: 10px">--}}
                                    <input type="checkbox" name="pro_auth" value="1" style="margin: 8px 0 0">
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Khối lượng (*kg):</label>
                                <div class="controls">
                                    <input type="number" name="khoi_luong" class="form-control form-control-line" autocomplete="off" style="padding-left: 10px">
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Link Youtube: </label>
                                <div class="controls">
                                    <input type="text" name="link_youtube" class="span11 form-control form-control-line" autocomplete="off" style="padding-left: 10px">
                                    <br>
                                    <span style="color:#999;font-size:12px">VD: https://youtu.be/D32eLVvn7tw</span>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Tags:</label>
                                <div class="controls">
                                    <input type="text" name="tags" class="span11 form-control form-control-line" autocomplete="off" style="padding-left: 10px">
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Meta keyword:</label>
                                <div class="controls">
                                    <input type="text" name="keyword" class="span11 form-control form-control-line" autocomplete="off" style="padding-left: 10px">
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Meta description:</label>
                                <div class="controls">
                                    <textarea name="description" style="width: 82.90598290598291%;"></textarea>
                                </div>
                            </div>
                            
                            <div class="controls">
                                <button type="submit" class="btn btn-success"> Đăng </button> 
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection
@section('script')
    <script src="{{ asset('ckeditor/ckeditor.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        function changeImg(input){
            if(input.files && input.files[0]){
                var reader = new FileReader();
                reader.onload = function(e){
                    $(input).prev().attr('src',e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endsection