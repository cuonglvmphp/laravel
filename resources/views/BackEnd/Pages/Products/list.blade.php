@extends('BackEnd.LayOut.master')
@section('title', 'Quản lý sản phẩm - Hệ thống quản trị website')
@section('main-content')

<div id="content">
    <!--breadcrumbs-->
    <div id="content-header">
        <div id="breadcrumb">
            <a href="{{ url('mx-admin') }}" title="Trang chủ" class="tip-bottom">
                <i class="icon-home"></i> Trang chủ
            </a>
            <a href="">
                Sản phẩm
            </a>
        </div>
    </div>

    <div class="container-fluid">

        @include('Notify.note')

         <div class="widget-box">
            <div class="widget-title">
                <h5> Danh sách sản phẩm </h5>
                <span style="font-weight:600;font-size:10px;margin:8px 30px;float:right;display:inline-block;"> Tổng số <span style="color:#f33">{{ $total }}</span> sp</span>
            </div>
            <div class="widget-content nopadding">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="width:20px">ID</th>
                            <th style="width:50px">Mã SP</th>
                            {{-- <th>ID</th> --}}
                            <th class="hidden-xs" style="width:120px">Ảnh đại diện</th>
                            <th>Tên</th>
                            <th>Mô tả</th>
                            <th class="hidden-xs" style="width: 200px">Danh mục</th>
                            {{-- <th class="hidden-xs">Ngày đăng</th> --}}
                            <th style="width: 120px">Thao tác</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($products as $key => $prod)
                           
                            <tr>
                                <td style="text-align:center;width:20px">{{ $prod->pro_id }}</td>
                                {{-- <td style="text-align: center;">{{ $prod->pro_id }}</td> --}}
                                <td style="text-align:center;width:50px">{{ $prod->pro_code }}</td>
                                <td class="hidden-xs" style="text-align: center;">
                                    <a class="tip" href="{{ url('mx-admin/sua-san-pham/'.$prod->pro_id) }}" data-original-title="Sửa">
                                        <img src="{{ asset('uploads/images/Products/'.$prod->pro_avatar) }}" style="height:50px">
                                    </a>
                                </td>
                                <td style="width: 250px">
                                    <a class="tip" href="{{ url('mx-admin/sua-san-pham/'.$prod->pro_id) }}" data-original-title="Sửa">
                                        {{ $prod->pro_title }}
                                    </a>
                                </td>
                                <td style="text-align:justify;width:300px">{!! str_limit($prod->pro_des,100) !!}</td>
                                <td class="hidden-xs" style="text-align: center;width: 200px">
                                    @php
                                        $getCate = DB::table('category')->where('cat_id', $prod->pro_cate)->get();
                                        foreach ($getCate as $value) {
                                            echo $value->cat_name;
                                        }
                                    @endphp
                                </td>
                                {{-- <td class="hidden-xs" style="text-align: center;">{{ $prod->pro_created_at }}</td> --}}
                                <td style="text-align: center;">
                                    <a class="tip" href="{{ url('mx-admin/sua-san-pham/'.$prod->pro_id) }}" data-original-title="Sửa"><i class="fa fa-pencil"></i> Sửa </a>
                                    &nbsp;
                                    <a class="tip" onclick="return confirm('Bạn có muốn xóa ?')" href="{{ url('mx-admin/xoa-san-pham/'.$prod->pro_id) }}" data-original-title="Xóa"><i class="fa fa-trash"></i> Xóa </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $products->links() }}
            </div>
        </div>
    </div>

</div>
@endsection
