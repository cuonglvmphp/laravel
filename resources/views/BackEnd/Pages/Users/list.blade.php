@extends('BackEnd.LayOut.master')
@section('title', 'Quản lý thành viên - Hệ thống quản trị website')
@section('main-content')

<div id="content">
    <!--breadcrumbs-->
    <div id="content-header">
        <div id="breadcrumb">
            <a href="{{ url('mx-admin') }}" title="Trang chủ" class="tip-bottom">
                <i class="icon-home"></i> Trang chủ
            </a>
            <a href="">
                Quản lý thành viên
            </a>
        </div>
    </div>
    <!--End-breadcrumbs-->
    <!--Action boxes-->
    <div class="container-fluid">

        @include('Notify.note')
        
        <div class="form-group" style="margin-top:25px">
            <a href="{{ url('mx-admin/them-thanh-vien') }}" class="btn btn-warning"><i class="fa fa-plus"></i> Thêm </a>
        </div>

        <div class="widget-box" style="max-width: 1000px;">
            <div class="widget-title">
                <h5> Thành viên </h5>
            </div>
            <div class="widget-content nopadding">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Tên</th>
                            <th>Email</th>
                            <th>Điện thoại</th>
                            <th>Địa chỉ</th>
                            <th style="width: 120px">Thao tác</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($list_user as $us)
                            <tr>
                                <td style="text-align: center;"><a class="tip" href="{{ url('mx-admin/sua-thanh-vien/'.$us->id) }}" data-original-title="Sửa">{{ $us->name }}</a></td>
                                <td style="text-align: center;">{{ $us->email }}</td>
                                <td style="text-align: center;">{{ $us->phone }}</td>
                                <td style="text-align: center;">{{ $us->address }}</td>
                                <td style="text-align: center;">
                                    <a class="tip" href="{{ url('mx-admin/sua-thanh-vien/'.$us->id) }}" data-original-title="Sửa"><i class="fa fa-pencil"></i> Sửa </a>
                                    &nbsp;
                                    <a class="tip" href="{{ url('mx-admin/xoa-thanh-vien/'.$us->id) }}" data-original-title="Xóa"><i class="fa fa-remove"></i> Xóa </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection