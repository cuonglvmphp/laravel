@extends('BackEnd.LayOut.master')
@section('title', 'Ban quản trị - Hệ thống quản trị website')
@section('main-content')

<div id="content">
    <!--breadcrumbs-->
    <div id="content-header">
        <div id="breadcrumb">
            <a href="{{ url('mx-admin') }}" title="Trang chủ" class="tip-bottom">
                <i class="icon-home"></i> Trang chủ
            </a>
            <a href="">
                Ban quản trị
            </a>
        </div>
    </div>
    <!--End-breadcrumbs-->
    <!--Action boxes-->
    <div class="container-fluid">

        @include('Notify.note')
        
        <div class="form-group" style="margin-top:25px">
            <a href="{{ url('mx-admin/ban-quan-tri/them') }}" class="btn btn-warning"><i class="fa fa-plus"></i> Thêm </a>
        </div>

        <div class="widget-box" style="max-width: 600px;">
            <div class="widget-title">
                <h5> Quản trị viên </h5>
            </div>
            <div class="widget-content nopadding">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Tên</th>
                            <th>Email</th>
                            <th style="width: 120px">Thao tác</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($list_user as $us)
                            <tr>
                                <td style="text-align: center;"><a class="tip" href="{{ url('mx-admin/ban-quan-tri/sua/'.$us->id) }}" data-original-title="Sửa">{{ $us->name }}</a></td>
                                <td style="text-align: center;">{{ $us->email }}</td>
                                <td style="text-align: center;">
                                    <a class="tip" href="{{ url('mx-admin/ban-quan-tri/sua/'.$us->id) }}" data-original-title="Sửa"><i class="fa fa-pencil"></i> Sửa </a>
                                    &nbsp;
                                    <a class="tip" href="{{ url('mx-admin/ban-quan-tri/xoa/'.$us->id) }}" data-original-title="Xóa"><i class="fa fa-remove"></i> Xóa </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection