@extends('BackEnd.LayOut.master')
@section('title', 'Quản lý comments - Hệ thống quản trị website')
@section('main-content')

    <div id="content">
        <!--breadcrumbs-->
        <div id="content-header">
            <div id="breadcrumb">
                <a href="{{ url('mx-admin') }}" title="Trang chủ" class="tip-bottom">
                    <i class="icon-home"></i> Trang chủ
                </a>
                <a href="">
                     Đơn hàng
                </a>
            </div>
        </div>

        <div class="container-fluid">

            @include('Notify.note')

            <div class="widget-box">
                <div class="widget-title">
                    <span class="icon"><i class="icon-ok"></i></span>
                    <h5> Danh sách đơn hàng </h5>
                </div>
                <div class="widget-content nopadding">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Tên</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Nội dung </th>
                            <th>Thời gian </th>
                            <th style="width: 120px">Thao tác</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($comments as $prod)
                            <tr>
                                <td style="text-align: center;">{{ $prod->id }}</td>
                                <td style="text-align: center;">{{ $prod->c_name }}</td>
                                <td style="text-align: center;">{{ $prod->c_email }}</td>
                                <td style="text-align: center;">{{ $prod->c_phone }}</td>
                                <td style="text-align: center;">{{ $prod->c_messages }}</td>
                                <td class="hidden-xs" style="text-align: center;">{{ $prod->created_at }}</td>

                                <td style="text-align: center;">
                                    <a class="tip" onclick="return confirm('Bạn có muốn xóa ?')" href="{{ route('AdminCommentDelete',$prod->id) }}" data-original-title="Xóa"><i class="icon-remove"></i> Xóa </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $comments->links() }}
                </div>
            </div>
        </div>

    </div>
@endsection
