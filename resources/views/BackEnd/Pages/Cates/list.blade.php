@extends('BackEnd.LayOut.master')
@section('title', 'Danh mục sản phẩm - Hệ thống quản trị website')
@section('main-content')

<div id="content">
    <!--breadcrumbs-->
    <div id="content-header">
        <div id="breadcrumb">
            <a href="{{ url('mx-admin') }}" title="Trang chủ" class="tip-bottom">
                <i class="icon-home"></i> Trang chủ
            </a>
            <a href="">
                 Danh mục sản phẩm
            </a>
        </div>
    </div>
    <!--End-breadcrumbs-->
    <!--Action boxes-->
    <div class="container-fluid" style="padding-top: 20px">

        @include('Notify.note')
        
        <a href="{{ url('mx-admin/them-danh-muc') }}" class="btn btn-default" style="background: #0072C2;color: #fff">Thêm mới</a>

        <div class="row-fluid">
            <div class="widget-box" style="max-width: 600px;">
                <div class="widget-title">
                    <h5> Danh mục sản phẩm </h5>
                </div>
                <div class="widget-content">
                    <div class="todo">
                        <ul>
                            @php
                                showCategories($cates);
                            @endphp
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection