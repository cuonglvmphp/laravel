@extends('BackEnd.LayOut.master')
@section('title', 'Quản lý Email đăng kí nhận khuyến mãi - Hệ thống quản trị website')
@section('main-content')

    <div id="content">
        <!--breadcrumbs-->
        <div id="content-header">
            <div id="breadcrumb">
                <a href="{{ url('mx-admin') }}" title="Trang chủ" class="tip-bottom">
                    <i class="icon-home"></i> Trang chủ
                </a>
                <a href="">
                      Email đăng kí nhận khuyến mãi
                </a>
            </div>
        </div>

        <div class="container-fluid">

            @include('Notify.note')

            <div class="widget-box">
                <div class="widget-title">
                    <span class="icon"><i class="icon-ok"></i></span>
                    <h5> Danh sách Email đăng kí nhận khuyến mãi </h5>
                </div>
                <div class="widget-content nopadding">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Email</th>
                            <th>Thời gian </th>
                            <th style="width: 120px">Thao tác</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($email_dk as $item)
                            <tr>
                                <td style="text-align: center;">{{ $item->dk_id }}</td>
                                <td style="text-align: center;">{{ $item->dk_mail }}</td>
                                <td class="hidden-xs" style="text-align: center;">{{ $item->dk_foo }}</td>

                                <td style="text-align: center;">
                                    <a class="tip" onclick="return confirm('Bạn có muốn xóa ?')" href="{{ url('mx-admin/xoa-email/'.$item->dk_id) }}" data-original-title="Xóa"><i class="icon-remove"></i> Xóa </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $email_dk->links() }}
                </div>
            </div>
        </div>

    </div>
@endsection
