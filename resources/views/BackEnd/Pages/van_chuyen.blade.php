@extends('BackEnd.LayOut.master')
@section('title', 'Phí vận chuyển - Hệ thống quản trị website')
@section('main-content')

<div id="content">
    <!--breadcrumbs-->
    <div id="content-header">
        <div id="breadcrumb">
            <a href="{{ url('mx-admin') }}" title="Trang chủ" class="tip-bottom">
                <i class="icon-home"></i> Trang chủ
            </a>
            <a href="">
               Phí vận chuyển
            </a>
        </div>
    </div>

    <div class="container-fluid">

        @include('Notify.note')

        <div class="row-fluid">
            <div class="span10">
                <form class="form-horizontal form-material" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="box-item">
                        <input type="text" name="phi_van_chuyen" placeholder="Phí vận chuyển" value="{{ $le_phi->phi }}"> đ/Kg
                        <button class="btn btn-sm btn-warning pull-left" style="clear:both;display:block;margin:20px 0;float:none"> Cập Nhật </button>
                    </div>
                </form>
                <div style="clear: both;"></div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script src="{{ asset('ckeditor/ckeditor.js') }}" type="text/javascript"></script>
@endsection