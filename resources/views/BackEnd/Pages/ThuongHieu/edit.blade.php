@extends('BackEnd.LayOut.master')
@section('title', 'Sửa Thương hiệu - Hệ thống quản trị website')
@section('main-content')

<div id="content">
    <!--breadcrumbs-->
    <div id="content-header">
        <div id="breadcrumb">
            <a href="{{ url('mx-admin') }}" title="Trang chủ" class="tip-bottom">
                <i class="icon-home"></i> Trang chủ
            </a>
            <a href="">
                Thương hiệu sản phẩm
            </a>
            <a href="">
                Sửa
            </a>
        </div>
        <h1 class="ttl-add-use" style="font-size: 25px"> Sửa Thương hiệu </h1>
    </div>
    <!--End-breadcrumbs-->
    <!--Action boxes-->
    <div class="container-fluid">

        @include('Notify.note')

        <div class="row-fluid">
            <div class="widget-box" style="max-width: 600px;">
                <div class="widget-title">
                    <h5> Thương hiệu sản phẩm </h5>
                </div>
                <div class="widget-content">
                    <div class="widget-content nopadding">
                        <form method="post" class="form-horizontal" style="max-width: 800px;">
                            {{ csrf_field() }}
                            <div class="control-group">
                                <div class="form-group">
                                    <label class="control-label">Tên Thương hiệu: </label>
                                    <div class="controls">
                                        <input type="text" class="span11" name="name" value="{{ $get_cate[0]->cat_name }}">
                                    </div>
                                </div>
                                <div class="controls">
                                    <button type="submit" class="btn btn-success">Cập nhật</button> 
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection