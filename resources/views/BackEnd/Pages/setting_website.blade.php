@extends('BackEnd.LayOut.master')
@section('title', 'Cấu hình chung - Hệ thống quản trị website')
@section('main-content')

<div id="content">

    <!--breadcrumbs-->
    <div id="content-header">
        <div id="breadcrumb">
            <a href="{{ url('mx-admin') }}" title="Trang chủ" class="tip-bottom">
                <i class="icon-home"></i> Trang chủ
            </a>
            <a href="{{ url('mx-admin/cau-hinh-chung') }}" title="Cấu hình chung" class="tip-bottom">
                Cấu hình chung
            </a>
        </div>
        <h1 class="ttl-add-use" style="font-size: 25px"> Cấu hình chung </h1>
    </div>
    <!--End-breadcrumbs-->
    <div class="container-fluid">
        @include('Notify.note')
        <hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title">
                        <span class="icon"> <i class="icon-align-justify"></i> </span>
                        <h5>Thông tin website</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form method="post" class="form-horizontal" style="max-width: 800px;">
                            {{ csrf_field() }}
                            <div class="control-group">
                                <label class="control-label">Tên website:</label>
                                <div class="controls">
                                    <input type="text" class="span11" name="title" value="{{ $st_info->st_title }}">
                                    <br>
                                    <span style="color: #999;font-size: 12px;">VD: Hệ thống bán hàng</span>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Tên Công ty:</label>
                                <div class="controls">
                                    <input type="text" name="company" class="span11" value="{{ $st_info->st_company }}">
                                    <br>
                                    <span style="color: #999;font-size: 12px;">VD: Công ty phát triển phần mền abc</span>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Địa chỉ:</label>
                                <div class="controls">
                                    <input type="text" value="{{ $st_info->st_address }}" name="address" class="span11">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Hotline:</label>
                                <div class="controls">
                                    <input type="text" value="{{ $st_info->st_phone }}" name="phone" class="span11">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Điện thoại:</label>
                                <div class="controls">
                                    <input type="text" value="{{ $st_info->st_phone2 }}" name="phone2" class="span11">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Email:</label>
                                <div class="controls">
                                    <input type="email" value="{{ $st_info->st_email }}" name="email" class="span11">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Facebook:</label>
                                <div class="controls">
                                    <input type="text" value="{{ $st_info->st_facebook }}" name="facebook" class="span11">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Google+ :</label>
                                <div class="controls">
                                    <input type="text" value="{{ $st_info->st_google }}" name="google" class="span11">
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn btn-success">Cập nhật</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection