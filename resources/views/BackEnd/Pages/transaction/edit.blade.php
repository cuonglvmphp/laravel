@extends('BackEnd.LayOut.master')
@section('title', 'Quản lý đơn hàng - Hệ thống quản trị website')
@section('main-content')

    <div id="content">
        <!--breadcrumbs-->
        <div id="content-header">
            <div id="breadcrumb">
                <a href="{{ url('mx-admin') }}" title="Trang chủ" class="tip-bottom">
                    <i class="icon-home"></i> Trang chủ
                </a>
                <a href=""> Sửa đơn hàng </a>
            </div>
        </div>

        <div class="container-fluid">

            @include('Notify.note')

            <div class="widget-box">
                <div class="widget-title">
                    <h5> Thông tin đơn hàng </h5>
                </div>
                <div class="widget-content nopadding">
                    <div class="widget-content nopadding">
                        <form method="post" class="form-horizontal" style="max-width: 800px;">
                            {{ csrf_field() }}
                            <div class="control-group">
                                <div class="form-group">
                                    <label class="control-label">Khách hàng: </label>
                                    <div class="controls">
                                        <input type="text" class="span6" name="name" value="{{ $don_hang->tst_name }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Email: </label>
                                    <div class="controls">
                                        <input type="text" class="span6" name="email" value="{{ $don_hang->tst_email }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Điện thoại: </label>
                                    <div class="controls">
                                        <input type="text" class="span6" name="phone" value="{{ $don_hang->tst_phone }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Địa chỉ: </label>
                                    <div class="controls">
                                        <input type="text" class="span6" name="adress" value="{{ $don_hang->tst_address }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Ghi chú: </label>
                                    <div class="controls">
                                        <textarea class="span6" name="notes">{{ $don_hang->tst_messages }}</textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Khối lượng ( *kg): </label>
                                    <div class="controls">
                                        <input type="text" class="span6" name="adress" value="{{ $don_hang->tst_address }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Phí vận chuyển( *đồng): </label>
                                    <div class="controls">
                                        <input type="text" class="span6" name="adress" value="{{ $don_hang->tst_address }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Trạng thái: </label>
                                    <div class="controls">
                                        <select class="span6" name="status">
                                            <option @if($don_hang->tst_status == 0) selected @endif value="0"> Đang chờ </option>
                                            <option @if($don_hang->tst_status == 1) selected @endif value="1"> Hoàn tất </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Tổng tiền: </label>
                                    <div class="controls">
                                        <input type="text" class="span6" name="total_price" value="{{ $don_hang->tst_total }}">
                                    </div>
                                </div>
                                <div class="controls">
                                    <button type="submit" class="btn btn-success"> Cập nhật </button> 
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
