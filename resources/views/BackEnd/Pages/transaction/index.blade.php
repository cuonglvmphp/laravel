@extends('BackEnd.LayOut.master')
@section('title', 'Quản lý đơn hàng - Hệ thống quản trị website')
@section('main-content')

    <div id="content">
        <!--breadcrumbs-->
        <div id="content-header">
            <div id="breadcrumb">
                <a href="{{ url('mx-admin') }}" title="Trang chủ" class="tip-bottom">
                    <i class="icon-home"></i> Trang chủ
                </a>
                <a href=""> Đơn hàng </a>
            </div>
        </div>

        <div class="container-fluid">

            @include('Notify.note')

            <div class="widget-box">
                <div class="widget-title">
                    <h5> Danh sách đơn hàng </h5>
                </div>
                <div class="widget-content nopadding">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th style="width:20px"> ID </th>
                            <th style="width:200px;"> Khách hàng </th>
                            <th style="width:200px;"> Email</th>
                            <th style="width:115px;"> Điện thoại </th>
                            <th> Địa chỉ</th>
                            <th style="width:60px"> Khối lượng </th>
                            <th style="width:100px"> Phí vận chuyển </th>
                            <th> Ngày tạo</th>
                            <th style="width:128px;"> Trạng thái </th>
                            <th style="width:110px">Tổng tiền</th>
                            <th style="width:195px"> Thao tác </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($transactions as $prod)
                            <tr>
                                <td style="text-align:center;width:20px">{{ $prod->id }}</td>
                                <td style="text-align:center;width:200px;">{{ $prod->tst_name }}</td>
                                <td style="text-align:center;width:200px;">{{ $prod->tst_email }}</td>
                                <td style="text-align:center;width:115px;">{{ $prod->tst_phone }}</td>
                                <td style="width:300px"><p>{{ $prod->tst_address }}</p></td>
                                <td>{{ $prod->tst_khoi_luong }} kg</td>
                                <td> 200đ</td>
                                <td class="hidden-xs" style="text-align:center;width:120px">{{ $prod->created_at }}</td>
                                <td class="hidden-xs" style="text-align:center;width:128px;">
                                    @if($prod->tst_status == 0) {{ 'Đang chờ' }}
                                    @else {{ 'Hoàn tất' }}
                                    @endif
                                </td>
                                <td style="width:110px">
                                    {{ formatPrice($prod->tst_total) }} đ
                                </td>
                                <td style="text-align:center;width:195px">
                                    <a href="{{ route('AdminTransactionDetail',$prod->id) }}">
                                        <i class="fa fa-eye"></i> Chi tiết
                                    </a>
                                    &nbsp
                                    <a href="{{ url('mx-admin/sua-don-hang/'.$prod->id) }}">
                                        <i class="fa fa-pencil"></i> Sửa
                                    </a>
                                    &nbsp
                                    <a class="tip" onclick="return confirm('Bạn có muốn xóa ?')" href="{{ route('AdminTransactionDelete',$prod->id) }}" data-original-title="Xóa">
                                        <i class="fa fa-remove"></i> Xóa 
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $transactions->links() }}
                </div>
            </div>
        </div>

    </div>
@endsection
