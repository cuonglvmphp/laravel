@extends('BackEnd.LayOut.master')
@section('title', 'Quản lý sản phẩm - Hệ thống quản trị website')
@section('main-content')

    <div id="content">
        <!--breadcrumbs-->
        <div id="content-header">
            <div id="breadcrumb">
                <a href="{{ url('mx-admin') }}" title="Trang chủ" class="tip-bottom">
                    <i class="icon-home"></i> Trang chủ
                </a>
                <a href="{{ route('AdminTransactionIndex') }}">
                    Đơn hàng
                </a>
                <a href="">Chi tiết</a>
            </div>
        </div>

        <div class="container-fluid">

            @include('Notify.note')

            <div class="widget-box">
                <div class="widget-title">
                    <span class="icon"><i class="icon-ok"></i></span>
                    <h5> Danh sách sản phẩm </h5>
                </div>
                <div class="widget-content nopadding">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Tên</th>
                            <th class="hidden-xs">Ảnh đại diện</th>
                            <th class="hidden-xs">Danh mục</th>
                            <th class="hidden-xs">Số Lượng</th>
                            <th>Tổng khối lượng</th>
                            <th class="hidden-xs">Số Tiền</th>
                            <th class="hidden-xs">Ngày đăng</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($products as $prod)
                            <tr>
                                <td style="text-align: center;">{{ $prod->pro_id }}</td>
                                <td style="text-align: center;">{{ $prod->pro_title }}</td>
                                <td class="hidden-xs" style="text-align: center;">
                                    <img src="{{ asset('uploads/images/Products/'.$prod->pro_avatar) }}" style="max-width: 200px;">
                                </td>
                                <td class="hidden-xs" style="text-align: center;">
                                    @php
                                        $getCate = DB::table('category')->where('cat_id',$prod->pro_cate)->get();
                                        foreach ($getCate as $value) {
                                            echo "<span class='label label-info'>".$value->cat_name."</span>";
                                        }
                                    @endphp
                                </td>
                                <td>{{ $prod->od_qty }}</td>
                                <td>
                                    @if($prod->pro_can_nang > 0) {{ $prod->od_qty*$prod->pro_can_nang }}
                                    @else {{ $prod->pro_can_nang }}
                                    @endif
                                     kg
                                </td>
                                <td>{{ $prod->od_price == 0 ? 'Liên hệ ' : formatPrice($prod->od_price).' ' }}</td>
                                <td class="hidden-xs" style="text-align: center;">{{ $prod->pro_created_at }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>

    </div>
@endsection
