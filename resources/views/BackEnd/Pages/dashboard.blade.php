@extends('BackEnd.LayOut.master')
@section('title', 'Hệ thống quản trị website')
@section('main-content')

<div id="content">
    <!--breadcrumbs-->
    <div id="content-header">
        <div id="breadcrumb">
            <a href="{{ url('mx-admin') }}" title="Trang chủ" class="tip-bottom">
                <i class="icon-home"></i> Trang chủ
            </a>
        </div>
    </div>
    <!--End-breadcrumbs-->
    <!--Action boxes-->
    <div class="container-fluid">
        <div class="row-fluid">
            <h2 class="ttl-hi">Chúc mừng bạn đã đăng nhập thành công hệ thống quản trị </h2>
        </div>
    </div>
</div>

@endsection