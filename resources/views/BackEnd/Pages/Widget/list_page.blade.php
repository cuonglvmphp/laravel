@extends('BackEnd.LayOut.master')
@section('title', 'Quản lý Pages - Hệ thống quản trị website')
@section('main-content')

<div id="content">
    <!--breadcrumbs-->
    <div id="content-header">
        <div id="breadcrumb">
            <a href="{{ url('mx-admin') }}" title="Trang chủ" class="tip-bottom">
                <i class="icon-home"></i> Trang chủ
            </a>
            <a href="">
               Pages
            </a>
        </div>
    </div>
    <style type="text/css">
        th, td {
            border-left: 1px solid #ccc!important;
            border-top: 1px solid #ccc!important;
        }
        table {
            border-bottom: 1px solid #ccc!important;
            border-right: 1px solid #ccc!important;
        }
    </style>

    <div class="container-fluid">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-block">
                    <div class="table-responsive" style="padding-top:20px">
                        <table class="table span8">
                            <thead>
                                <tr>
                                    <th style="width:20px">STT</th>
                                    <th style="width:200px;">Thông tin Công ty</th>
                                    <th style="width:25%;text-align:center">Thao tác</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-center">1</td>
                                    <td> Giới thiệu công ty </td>
                                    <td class="text-center"><a href="{{ url('mx-admin/cap-nhat-noi-dung/1') }}"><i class="fa fa-pencil"></i> Cập nhật nội dung </a></td>
                                </tr>
                                <tr>
                                    <td class="text-center">2</td>
                                    <td> Tiêu chí bán hàng </td>
                                    <td class="text-center"><a href="{{ url('mx-admin/cap-nhat-noi-dung/2') }}"><i class="fa fa-pencil"></i> Cập nhật nội dung </a></td>
                                </tr>
                                <tr>
                                    <td class="text-center">3</td>
                                    <td> Đối tác chiến lược </td>
                                    <td class="text-center"><a href="{{ url('mx-admin/cap-nhat-noi-dung/3') }}"><i class="fa fa-pencil"></i> Cập nhật nội dung </a></td>
                                </tr>
                                <tr>
                                    <td class="text-center">4</td>
                                    <td> Showroom </td>
                                    <td class="text-center"><a href="{{ url('mx-admin/cap-nhat-noi-dung/4') }}"><i class="fa fa-pencil"></i> Cập nhật nội dung </a></td>
                                </tr>
                            </tbody>
                        </table>
                        <table class="table span8">
                            <thead>
                                <tr>
                                    <th style="width:20px">STT</th>
                                    <th style="width:200px;">Chính sách</th>
                                    <th style="width:25%;text-align:center">Thao tác</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-center">1</td>
                                    <td> Chính sách & Quy định chung </td>
                                    <td class="text-center"><a href="{{ url('mx-admin/cap-nhat-noi-dung/5') }}"><i class="fa fa-pencil"></i> Cập nhật nội dung </a></td>
                                </tr>
                                <tr>
                                    <td class="text-center">2</td>
                                    <td> Hình thức thanh toán </td>
                                    <td class="text-center"><a href="{{ url('mx-admin/cap-nhat-noi-dung/6') }}"><i class="fa fa-pencil"></i> Cập nhật nội dung </a></td>
                                </tr>
                                <tr>
                                    <td class="text-center">3</td>
                                    <td> Chính sách vận chuyển </td>
                                    <td class="text-center"><a href="{{ url('mx-admin/cap-nhat-noi-dung/7') }}"><i class="fa fa-pencil"></i> Cập nhật nội dung </a></td>
                                </tr>
                                <tr>
                                    <td class="text-center">4</td>
                                    <td> Chính sách bảo mật thông tin </td>
                                    <td class="text-center"><a href="{{ url('mx-admin/cap-nhat-noi-dung/8') }}"><i class="fa fa-pencil"></i> Cập nhật nội dung </a></td>
                                </tr>
                            </tbody>
                        </table>
                        <table class="table span8">
                            <thead>
                                <tr>
                                    <th style="width:20px">STT</th>
                                    <th style="width:200px;">Hỗ trợ khách hàng</th>
                                    <th style="width:25%;text-align:center">Thao tác</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-center">1</td>
                                    <td> Hợp tác đại lý </td>
                                    <td class="text-center"><a href="{{ url('mx-admin/cap-nhat-noi-dung/9') }}"><i class="fa fa-pencil"></i> Cập nhật nội dung </a></td>
                                </tr>
                                <tr>
                                    <td class="text-center">2</td>
                                    <td> Chính sách bán buôn </td>
                                    <td class="text-center"><a href="{{ url('mx-admin/cap-nhat-noi-dung/10') }}"><i class="fa fa-pencil"></i> Cập nhật nội dung </a></td>
                                </tr>
                                <tr>
                                    <td class="text-center">3</td>
                                    <td> Hướng dẫn mua hàng </td>
                                    <td class="text-center"><a href="{{ url('mx-admin/cap-nhat-noi-dung/11') }}"><i class="fa fa-pencil"></i> Cập nhật nội dung </a></td>
                                </tr>
                                <tr>
                                    <td class="text-center">4</td>
                                    <td> Quy định bảo hành - Sửa chữa </td>
                                    <td class="text-center"><a href="{{ url('mx-admin/cap-nhat-noi-dung/12') }}"><i class="fa fa-pencil"></i> Cập nhật nội dung </a></td>
                                </tr>
                            </tbody>
                            <table class="table span8">
                            <thead>
                                <tr>
                                    <th style="width:20px">STT</th>
                                    <th style="width:200px;"> Các pages khác </th>
                                    <th style="width:25%;text-align:center">Thao tác</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-center">1</td>
                                    <td> Kiến thức sản phẩm </td>
                                    <td class="text-center"><a href="{{ url('mx-admin/cap-nhat-noi-dung/13') }}"><i class="fa fa-pencil"></i> Cập nhật nội dung </a></td>
                                </tr>
                                <tr>
                                    <td class="text-center">2</td>
                                    <td> Khuyến mại và Sự kiện </td>
                                    <td class="text-center"><a href="{{ url('mx-admin/cap-nhat-noi-dung/14') }}"><i class="fa fa-pencil"></i> Cập nhật nội dung </a></td>
                                </tr>
                                <tr>
                                    <td class="text-center">3</td>
                                    <td> Tuyển dụng </td>
                                    <td class="text-center"><a href="{{ url('mx-admin/cap-nhat-noi-dung/15') }}"><i class="fa fa-pencil"></i> Cập nhật nội dung </a></td>
                                </tr>
                                <tr>
                                    <td class="text-center">4</td>
                                    <td> Liên hệ </td>
                                    <td class="text-center"><a href="{{ url('mx-admin/cap-nhat-noi-dung/16') }}"><i class="fa fa-pencil"></i> Cập nhật nội dung </a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop