@extends('BackEnd.LayOut.master')
@section('title', 'Cài đặt trang giới thiệu - Hệ thống quản trị website')
@section('main-content')

<div id="content">
    <!--breadcrumbs-->
    <div id="content-header">
        <div id="breadcrumb">
            <a href="{{ url('mx-admin') }}" title="Trang chủ" class="tip-bottom">
                <i class="icon-home"></i> Trang chủ
            </a>
            <a href="">
                Trang giới thiệu
            </a>
        </div>
        <h1 class="ttl-add-use" style="font-size: 25px"> Cấu hình trang giới thiệu </h1>
    </div>
    <!--End-breadcrumbs-->
    <!--Action boxes-->
    <div class="container-fluid">

        @include('Notify.note')

        <div class="row-fluid">
            <div class="">
                <style type="text/css">
                    .item-video {
                        padding: 10px;
                        border: 1px solid #ddd;
                        margin-bottom: 20px;
                        background: #fff;
                    }
                    .box-item {
                        border: 1px solid #ddd;
                        padding: 25px;
                    }
                </style>
                <div class="span10 item-video">
                    <form class="form-horizontal form-material" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="box-item">
                            <textarea name="content" id="content" class="ckeditor" cols="30" rows="10" required>@if(isset($about[0]->pa_content)) {!! $about[0]->pa_content !!} @endif</textarea>
                        </div>
                        <div class="span12 text-center">
                            <button class="btn btn-sm btn-warning pull-left"> Cập Nhật </button>
                        </div>
                    </form>
                    <div style="clear: both;"></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
    <script src="{{ asset('ckeditor/ckeditor.js') }}" type="text/javascript"></script>
@endsection