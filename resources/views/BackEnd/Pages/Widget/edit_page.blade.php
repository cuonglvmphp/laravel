@extends('BackEnd.LayOut.master')
@section('title', 'Quản lý Pages - Hệ thống quản trị website')
@section('main-content')

<div id="content">
    <!--breadcrumbs-->
    <div id="content-header">
        <div id="breadcrumb">
            <a href="{{ url('mx-admin') }}" title="Trang chủ" class="tip-bottom">
                <i class="icon-home"></i> Trang chủ
            </a>
            <a href="">
               Pages
            </a>
        </div>
    </div>

    <div class="container-fluid">

        @include('Notify.note')

        <div class="row-fluid">
            <div class="">
                <style type="text/css">
                    .item-video {
                        padding: 10px;
                        border: 1px solid #ddd;
                        margin-bottom: 20px;
                        background: #fff;
                    }
                    .box-item {
                        border: 1px solid #ddd;
                        padding: 25px;
                    }
                </style>
                <div class="span10" style="padding-bottom: 20px">
                    <a class="btn btn-sm btn-info" href="/mx-admin/pages"> Quay lại </a>
                </div>
                <div class="span10 item-video">
                    <form class="form-horizontal form-material" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="box-item">
                            <h3 style="font-size:18px"> {{ $more_page->title }} </h3>
                            <textarea name="content" id="content" class="ckeditor" cols="30" rows="10" required>{{ $more_page->content }}</textarea>
                            <button class="btn btn-sm btn-warning pull-left" style="clear:both;display:block;margin:20px 0;float:none"> Cập Nhật </button>
                        </div>
                    </form>
                    <div style="clear: both;"></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script src="{{ asset('ckeditor/ckeditor.js') }}" type="text/javascript"></script>
@endsection