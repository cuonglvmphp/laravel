@extends('BackEnd.LayOut.master')
@section('title', 'Quản lý video - Hệ thống quản trị website')
@section('main-content')

<div id="content">
    <!--breadcrumbs-->
    <div id="content-header">
        <div id="breadcrumb">
            <a href="{{ url('mx-admin') }}" title="Trang chủ" class="tip-bottom">
                <i class="icon-home"></i> Trang chủ
            </a>
            <a href="">
                Quản lý video 
            </a>
            <a href="">
                Đăng mới 
            </a>
        </div>
    </div>
    <!--End-breadcrumbs-->
    <!--Action boxes-->
    <div class="container-fluid">

        @include('Notify.note')

        <div class="row-fluid">
            <div class="widget-box" style="max-width: 600px;">
                <div class="widget-title"> <span class="icon"><i class="icon-ok"></i></span>
                    <h5> Đăng mới Video </h5>
                </div>
                <div class="widget-content">
                    <div class="widget-content nopadding">
                        <form class="form-horizontal form-material" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="col-sm-12 form-group">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <textarea style="width: 97.5%; min-height: 250px; " class="videoYt" name="ma_nhung_youtube" onkeypress="creatIf()"></textarea>
                                        <span style="color: red; font-size: 12px"> Dán iframe vào khung</span>
                                        <button class="btn btn-success" style="display: block; margin: 20px 0"> Đăng </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection