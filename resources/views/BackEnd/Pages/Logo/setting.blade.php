@extends('BackEnd.LayOut.master')
@section('title', 'Cài Đặt Logo - Hệ thống quản trị website')
@section('main-content')

<div id="content">
    <!--breadcrumbs-->
    <div id="content-header">
        <div id="breadcrumb">
            <a href="{{ url('mx-admin') }}" title="Trang chủ" class="tip-bottom">
                <i class="icon-home"></i> Trang chủ
            </a>
            <a href="">
                Logo
            </a>
           
        </div>
    </div>
    <!--End-breadcrumbs-->
    <!--Action boxes-->
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="widget-box" style="max-width: 600px;">
                @include('Notify.note')
                <div class="widget-title"> <span class="icon"><i class="icon-ok"></i></span>
                    <h5> Cài Đặt Logo </h5>
                </div>
                <div class="widget-content">
                    <div class="widget-content nopadding">
                        <form class="form-horizontal form-material" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="col-sm-12 form-group">
                                <div>
                                    <div class="form-group">
                                        @if(count($picture) > 0 && file_exists('images/'.$picture[0]->lg_image))
                                            <img src="{{ asset('images/'.$picture[0]->lg_image) }}" style="max-width: 100%; display: block">
                                        @else
                                            <img src="{{ asset('backend/img/no_images.png') }}" style="max-width: 100%; display: block">
                                        @endif
                                        <input class="inp-upload-img hide" type="file" name="img_logos" onchange="changeImg(this)">
                                        <span class="btn btn-sm btn-primary" onclick="$('.inp-upload-img').click()" style="display: block; max-width: 110px; margin: 20px 0"> Chọn ảnh </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 form-group">
                                <div class="col-sm-12">
                                    <button class="btn btn-success">Xác nhận</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
    <script type="text/javascript">
        function changeImg(input){
            if(input.files && input.files[0]){
                var reader = new FileReader();
                reader.onload = function(e){
                    $(input).prev().attr('src',e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endsection