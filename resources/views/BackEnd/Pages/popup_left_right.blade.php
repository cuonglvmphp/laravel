@extends('BackEnd.LayOut.master')
@section('title', 'Cài Đặt Popup - Hệ thống quản trị website')
@section('main-content')
<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="{{ url('mx-admin') }}" title="Trang chủ" class="tip-bottom">
                <i class="icon-home"></i> Trang chủ
            </a>
            <a href="javascript::void(0)"> Popup </a>
        </div>
    </div>
    <div class="container-fluid">
        @include('Notify.note')
        <div class="row-fluid">
            <div class="widget-box">
                <div class="widget-title"> <span class="icon"><i class="icon-ok"></i></span>
                    <h5> Cài Đặt Popup </h5>
                </div>
                <div class="widget-content">
                    <div class="widget-content nopadding">
                        <form class="form-horizontal form-material" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div style="width: 50%;float: left;">
                                <h4>Popup Trái <span><input type="checkbox" @if($getPopupLeft != '' && $getPopupLeft->pp_active == 1) checked @endif name="ac_left" style="margin: 0 10px"></span></h4> 
                                
                                <div class="form-group">
                                    @if($getPopupLeft != '' > 0 && file_exists('images/'.$getPopupLeft->pp_img))
                                        <img src="{{ asset('images/'.$getPopupLeft->pp_img) }}" style="max-width: 60%; display: block">
                                    @else
                                        <img src="{{ asset('backend/img/no_images.png') }}" style="max-width: 60%; display: block">
                                    @endif
                                    <input class="inp-upload-img-1 hide" type="file" name="pop_left" onchange="changeImg(this)">
                                    <span class="btn btn-sm btn-primary" onclick="$('.inp-upload-img-1').click()" style="display: block; max-width: 110px; margin: 20px 0"> Chọn ảnh </span>
                                </div>
                            </div>
                             <div style="width: 50%;float: left;">
                                <h4>Popup Phải <span><input type="checkbox" @if($getPopupRight != '' && $getPopupRight->pp_active == 1) checked @endif name="ac_right" style="margin: 0 10px"></span></h4>
                                <div class="form-group">
                                    @if($getPopupRight != '' > 0 && file_exists('images/'.$getPopupRight->pp_img))
                                        <img src="{{ asset('images/'.$getPopupRight->pp_img) }}" style="max-width: 60%; display: block">
                                    @else
                                        <img src="{{ asset('backend/img/no_images.png') }}" style="max-width: 60%; display: block">
                                    @endif
                                    <input class="inp-upload-img-2 hide" type="file" name="pop_right" onchange="changeImg(this)">
                                    <span class="btn btn-sm btn-primary" onclick="$('.inp-upload-img-2').click()" style="display: block; max-width: 110px; margin: 20px 0"> Chọn ảnh </span>
                                </div>
                            </div>
                            <div class="col-sm-12 form-group" style="width: 100%;clear: both;">
                                <div class="col-sm-12" style="clear: both;margin-bottom: 15px">
                                    <button class="btn btn-success"> Cập Nhật </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
    <script type="text/javascript">
        function changeImg(input){
            if(input.files && input.files[0]){
                var reader = new FileReader();
                reader.onload = function(e){
                    $(input).prev().attr('src',e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endsection