@if(Session::has('success'))
    <div class="alert {{Session::get('alert-class', 'alert-success') }}" style="font-weight: bold;margin-top: 5%;transition: .5s">
        <span style="color: #4e6f38; display: block;">{{ Session::get('success') }}</span>
    </div>
@endif
@if(Session::has('error'))
    <div class="alert {{Session::get('alert-class', 'alert-danger') }}" style="font-weight: bold;margin-top: 5%;transition: .5s">
        <span style="color: #f33; margin-top: 20px; display: block;">{{ Session::get('error') }}</span>
    </div>
@endif

<script>
    setTimeout(function () {
        $('.alert').hide();
    },2000)
</script>