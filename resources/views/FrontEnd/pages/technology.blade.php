@extends('FrontEnd.layouts.master')

@section('main-content')
    <section class="content-congnghe">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-6 col-xs-6 wow fadeInUp"
                     style="height: 340px; visibility: visible; animation-name: fadeInUp;">
                    <div class="item">
                        <div class="images">
                            <a href=""><img class="iblur"
                                            style="height: 196px;"
                                            src="https://vineco.net.vn/uploads/Công nghệ/thoi-tiet-nong-vu-04-09.jpg"
                                            alt=""></a>
                            <div class="icon-cn">
                                <a href="https://vineco.net.vn/cong-nghe-tu-dong-hoa-co-gioi-hoa">
                                    <img src="https://vineco.net.vn/uploads/Icon/icon-cn1.png" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="nav-images" style="height: 110px;">
                            <h3 class="title1"><a href="">Công
                                    nghệ tự động hoá, cơ giới hóa</a></h3>

                            <p class="desc"></p>
                            <p style="text-align: justify;">Công nghệ cơ giới hóa quy mô lớn sử dụng máy móc công nghệ
                                nhập khẩu từ Kubota, Nhật Bản giúp...</p>
                        </div>

                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-6 wow fadeInUp"
                     style="height: 340px; visibility: visible; animation-name: fadeInUp;">
                    <div class="item">
                        <div class="images">
                            <a href=""><img
                                        class="iblur" style="height: 196px;"
                                        src="https://vineco.net.vn/uploads/Công nghệ/_HNE0856.jpg" alt=""></a>
                            <div class="icon-cn">
                                <a href="">
                                    <img src="https://vineco.net.vn/uploads/Icon/icon-cn2.png" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="nav-images" style="height: 110px;">
                            <h3 class="title1"><a
                                        href="">Công
                                    nghệ trồng rau trong nhà kính</a></h3>

                            <p class="desc"></p>
                            <p style="text-align: justify;">Nhà kính/nhà màng/nhà lưới của VinEco được triển khai kết
                                hợp với các công nghệ sản xuất...</p>
                        </div>

                    </div>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-6 wow fadeInUp"
                     style="height: 340px; visibility: visible; animation-name: fadeInUp;">
                    <div class="item">
                        <div class="images">
                            <a href="">
                                <img class="iblur" style="height: 196px; width: 100%" src="{{asset('images/loiichdualuoi.jpg')}}" alt=""></a>
                            <div class="icon-cn">
                                <a href="https://vineco.net.vn/cong-nghe-san-xuat-gia-nam-an-nam-duoc-lieu">
                                    <img src="https://vineco.net.vn/uploads/Icon/icon-cn2.png" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="nav-images" style="height: 110px;">
                            <h3 class="title1">
                                <a href="">Công
                                    nghệ sản xuất dưa lưới trong nhà màng</a>
                            </h3>
                            <p class="desc"></p>
                            <p style="text-align: justify;">Sản xuất theo quy trình tự động từ công nghệ cao&nbsp;của&nbsp;Hàn
                                Quốc</p>
                            <p></p>
                        </div>

                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-6 wow fadeInUp"
                     style="height: 340px; visibility: visible; animation-name: fadeInUp;">
                    <div class="item">
                        <div class="images">
                            <a href="">
                                <img class="iblur" style="height: 196px;" src="https://vineco.net.vn/uploads/Công nghệ/tưới tiêu tự động.jpg" alt=""></a>
                            <div class="icon-cn">
                                <a href="">
                                    <img src="https://vineco.net.vn/uploads/Icon/icon-cn4.png" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="nav-images" style="height: 110px;">
                            <h3 class="title1">
                                <a href="">Công nghệ tưới tiêu tự động, khoa học</a></h3>
                            <p class="desc"></p>
                            <p style="text-align: justify;">Công nghệ tưới tiêu tự động, khoa học, tiết kiệm nước của
                                chúng tôi được bàn giao từ Israel,...</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-6 wow fadeInUp"
                     style="height: 340px; visibility: visible; animation-name: fadeInUp;">
                    <div class="item">
                        <div class="images">
                            <a href=""><img class="iblur" style="height: 196px;"
                                            src="https://vineco.net.vn/uploads/Công nghệ/_HNE2494.jpg" alt=""></a>
                            <div class="icon-cn">
                                <a href="">
                                    <img src="https://vineco.net.vn/uploads/Icon/icon-cn5.png" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="nav-images" style="height: 110px;">
                            <h3 class="title1">
                                <a href="">Công nghệ sau thu hoạch và chế biến</a></h3>
                            <p class="desc"></p>
                            <p>Công nghệ sau thu hoạch và Công nghệ chế biến</p>
                            <p></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection