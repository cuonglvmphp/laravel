@extends('FrontEnd.layouts.master')

@section('main-content')
    <section class="content-info content-new-detailt">
        <div class="khung-content">
            <h1 class="primary-title">13 lợi ích tuyệt vời khi bạn ăn dưa lưới</h1>
            <div class="date-detailt">
                <div class="left">
                    <div class="date">13/07/2019</div>
                </div>
                <div class="right">

                </div>
                <div class="clearfix"></div>
            </div>
            <div class="hc2-item-col hc2-item-col-8 hc2-item-no-margin">
                <div class="entry-content" id="entry-content">
                    <div class="hc2-content-single-featured-image fade-in text-center">
                        <img width="640" height="426" src="{{asset('images/loiichdualuoi.jpg')}}">
                    </div>
                    <div class="entry-content-body">
                        <p><strong>Dưa lưới giờ đây đã trở thành loại trái cây được nhiều
                                người tin dùng vì không chỉ ngon mà còn bổ dưỡng. Vậy những lợi ích của dưa lưới mang
                                lại gồm những gì? Hãy cùng tìm hiểu qua bài viết dưới đây nhé.</strong></p>
                        <p>Quỹ Y tế Thế giới đã liệt kê dưa lưới vào danh sách các loại quả mang lại lợi ích cho phổi vì
                            nó chứa nhiều <a target="_blank" data-event-category="Internal link click on article"
                                             data-event-action="click"
                                             data-event-label="https://hellobacsi.com/thuoc/vitamin-a/"
                                             href="https://hellobacsi.com/thuoc/vitamin-a/" rel="noopener">vitamin A</a>
                            (250mg dưa lưới chứa tới 40% lượng <a target="_blank" title="vitamin A"
                                                                  data-event-category="Internal link click on article"
                                                                  data-event-action="click"
                                                                  data-event-label=" https://hellobacsi.com/thuoc-va-thao-duoc-a-z/thuoc/vitamin-a/"
                                                                  href=" https://hellobacsi.com/thuoc-va-thao-duoc-a-z/thuoc/vitamin-a/">vitamin
                                A</a> cơ thể cần mỗi ngày). Ngoài ra, nhờ có hàm lượng chất beta carotene phong phú mà
                            loại dưa này cũng có thể giúp kiểm soát sự thoái hóa điểm vàng, một bệnh làm suy giảm thị
                            lực ở người có tuổi.</p>
                        <h2>1. Tốt cho mắt</h2>
                        <p>Dưa lưới rất giàu folate và vitamin nhóm B, rất cần thiết cho sự phát triển của thai nhi và
                            ngừa bệnh thiếu máu. Thực tế, thiếu hụt folate có thể dẫn đến chậm phát triển thai nhi, tăng
                            nguy cơ bệnh tật cho não, dị tật thần kinh bẩm sinh và chậm phát triển ở trẻ em. Chất lutein
                            và zeaxanthin trong dưa lưới rất cần cho đôi mắt sáng khỏe, thiếu lutein và zeaxanthin dẫn
                            đến thoái hóa điểm vàng và đục thủy tinh thể.</p>

                        <h2>2. Phòng ngừa bệnh ung thư</h2>
                        <p>Dưa lưới là nguồn chứa chất chống oxy hóa dạng polyphenol, là chất có lợi cho sức khỏe trong
                            việc phòng chống bệnh ung thư và tăng cường hệ miễn dịch.</p>
                        <h2 style="margin-bottom: 15px">3. Tốt cho tim mạch</h2>
                        <p class="text-center">
                            <img class="aligncenter wp-image-71445 size-full "
                                 src="{{asset('images/abc.jpg')}}"
                                 alt="Lợi ích của dưa lưới" width="690" height="460"

                                 sizes="(max-width: 690px) 100vw, 690px"/>
                        </p>
                        <p>Dưa lưới cũng giàu chất adenosine có lợi cho tim vì nó có đặc tính làm loãng máu. Điều này
                            ngăn ngừa máu đông trong hệ thống tim
                            mạch Vitamin
                            Cngăn ngừa tình trạng xơ cứng động mạch, trong khi folate giúp ngăn ngừa các cơn
                            đau tim có thể xảy ra.</p>
                        <h2>4. Hỗ trợ quá trình giảm cân</h2>
                        <p>Chất xơ có lợi cho việc giảm cân vì nó thường ở lại trong dạ dày của bạn lâu hơn các thức ăn
                            khác và giúp bạn luôn thấy no. Quá trình tiêu hóa chậm này ngăn cản bạn ăn quá nhiều.</p>
                        <h2>5. Hỗ trợ trong việc cai thuốc lá</h2>
                        <p>Dưa lưới có lợi cho những ai muốn bỏ hút thuốc lá. Điều này là do khoáng chất và chất dinh
                            dưỡng trong dưa lưới giúp người hút thuốc có thể chống lại cảm giác thèm thuốc và các triệu
                            chứng cai nghiện nicotin một cách hiệu quả. Hơn nữa, nó giúp cơ thể phục hồi nhanh hơn bằng
                            cách bổ sung vitamin A mất đi của cơ thể do hút thuốc liên tục.</p>
                        <h2>6. Tốt cho bệnh nhân tiểu đường</h2>
                        <p>Dưa lưới có nhiều loại vitamin khác nhau, khoáng chất và các protein nhưng lại ít calo, ít
                            đường phù hợp với người bệnh tiểu đường. Nguồn chất kali trong dưa lưới làm tăng lưu lượng
                            máu và oxy tới não giúp bạn giảm căng thẳng, giúp tinh thần thoải mái
                            hơn.</p>
                        <h2>7. Tăng cường hệ miễn dịch</h2>
                        <p>Dưa lưới giàu vitamin C và vitamin A có chức năng làm tăng khả năng hệ miễn dịch bằng cách
                            kích thích các bạch cầu
                            trong cơ thể. Là một chất chống oxy hóa mạnh mẽ, vitamin C cũng có hiệu quả chống lại các
                            gốc tự do trong cơ thể. Các gốc tự do này làm hư da
                            và gây lão hóa sớm.</p>
                        <h2>8. Điều trị các vấn đề về kinh nguyệt ở nữ giới</h2>
                        <p>Vitamin C ở dưa lưới có hiệu quả trong việc điều hòa chu kỳ kinh nguyệt và làm giảm những cơn
                            đau bụng khi có kinh ở phụ nữ. Thường xuyên tiêu thụ dưa lưới trong thời gian kinh nguyệt có
                            thể làm giảm lưu lượng và hạn chế tình trạng đông máu đáng kể.</p>
                        <h2 style="margin-bottom: 15px">9. Có tác dụng làm đẹp da</h2>
                        <p class="text-center">
                            <img class="aligncenter wp-image-71445 size-full"
                                 src="{{asset('images/depda.jpg')}}"
                                 alt="Lợi ích của dưa lưới" width="690" height="460"

                                 sizes="(max-width: 690px) 100vw, 690px"/>
                        </p>
                        <p>Ăn dưa lưới vào buổi sáng giúp cơ thể giải độc, điều tiết tốt hơn. Zeaxanthin trong dưa lưới
                            còn có tác dụng bảo vệ da khỏi tia UV. Ngoài ra, dưa lưới còn là nguồn thực phẩm chứa
                            beta-carotene, axit folic, kali, vitamin C và vitamin A giúp bạn cải thiện làn da sao trẻ
                            trung và khỏe mạnh hơn.</p>
                        <h2>10. Giảm căng thẳng</h2>
                        <p>Theo các nhà nghiên cứu Pháp, trong dưa lưới có enzyme super oxyd dismutase (SOD) giúp cải
                            thiện những dấu hiệu căng thẳng về thể chất lẫn tinh thần. Nó kích thích cơ thể sản xuất
                            kháng thể, giúp làm giảm tỷ lệ cholesterol xấu, ngăn ngừa xơ cứng và giúp giảm cân.
                            Beta-carotene sẽ chuyển thành vitamin A, có vai trò quan trọng đối với thị giác, sức khỏe
                            của da và niêm mạc. Vì vậy một vài miếng dưa lưới cắt miếng, hay một ly nước ép dưa lưới sẽ
                            giúp bạn có một buổi sáng dễ chịu.</p>
                        <h2>11. Điều trị chứng mất ngủ</h2>
                        <p>Dưa lưới có đặc tính nhuận tràng mạnh vì nó chứa một hợp chất làm giảm sự căng thẳng, lo
                            lắng. Vì vậy, nó giúp bạn thoát khỏi rối loạn giấc ngủ bằng cách làm dịu hệ thần kinh và
                            giúp bạn đi vào giấc ngủ dễ dàng hơn.</p>
                        <h2>12. Giảm tình trạng viêm trong cơ thể</h2>
                        <p>Một ly nhỏ (250 ml) dưa lưới có chứa tới 22% lượng vitamin B6 mà cơ thể
                            cần hàng ngày. Vì thế, loại dưa này có thể giúp cơ thể duy trì sự trao đổi chất rất tốt.
                            Nghiên cứu được công bố trong năm 2010 trên tạp chí Dinh dưỡng lâm sàng của Mỹ cho thấy
                            những người bị thiếu vitamin B6 thường có nhiều nguy cơ bị viêm trong cơ thể, dễ dàng gặp
                            tình trạng căng thẳng, oxy hóa và giảm hiệu quả của quá trình trao đổi chất, từ đó dễ dẫn
                            đến bệnh tiểu đường và
                            các bệnh khác.</p>
                        <h2>13. Hạn chế tình trạng viêm</h2>
                        <p>Theo nghiên cứu, chất phytochemical trong dưa lưới có tác dụng chống viêm. Dưa lưới sẽ giúp
                            các bạn ngăn chặn sự thoái hóa của khớp xương, giảm viêm. Với việc thường xuyên ngồi học,
                            ngồi văn phòng như hiện nay, đau lưng, đau vai gáy hay các bệnh về khớp là điều khó tránh
                            khỏi. Vì thế, bạn càng phải bổ sung dưa lưới vào chế độ dinh dưỡng hàng ngày cho mình.</p>
                        <h2>Những ai không thể ăn dưa lưới?</h2>
                        <p>Bà Nhina Taranhenko, chủ nhiệm khoa nội bệnh viện Kiev (Nga), cho rằng những người bị viêm
                            ruột mãn tính, các bệnh về gan và thận không nên ăn loại quả này vì nó có thể làm cho bệnh
                            nặng hơn.</p>
                        <h2>Cách lựa chọn dưa lưới</h2>
                        <p>Để có một quả dưa lưới “đáng đồng tiền”, bạn nên biết cách lựa chọn dưa như sau:</p>
                        <ul>
                            <li>Quả dưa già, chín thường cuống đã rụng một cách tự nhiên. Dưa vàng còn cuống thường chưa
                                đủ độ chín, ngọt.
                            </li>
                            <li>Vỏ của dưa vàng ngon thường dày, khô ráo, nổi gồ lên chứ không bằng phẳng, trơn mướt.
                            </li>
                            <li>Mùi thơm cũng là một dấu hiệu để nhận biết quả dưa đã chín. Tuy nhiên nếu mùi thơm quá
                                ngọt, ấn tay vào thấy mềm nhũn là quả đó đã chín quá, không ngon.
                            </li>
                            <li>Ngoài ra, nên tránh những trái dưa có vết thâm hay những đốm màu khác lạ vì đó là những
                                chỗ hỏng, có thể đã lan vào phần ruột.
                            </li>
                        </ul>
                        <p>Dưa lưới mang lại rất nhiều lợi ích, vì thế hãy dùng dưa lưới để có sức khỏe tốt cho bản thân
                            cũng như các thành viên trong gia đình bạn nhé.</p>
                    </div>
                </div>

            </div>
        </div>
    </section>
@endsection