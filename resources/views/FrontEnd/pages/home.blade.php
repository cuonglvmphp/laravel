@extends('FrontEnd.layouts.master')

@section('main-content')
    <section class="new-home">
        <div class="container">
            <h2 class="title1 wow fadeInUp"><a href="">Tin Tức</a></h2>
            <div class="nav-new-home">
                <div class="row">
                    <div class="col-md-5 col-sm-12 col-xs-12">
                        <div class="left wow bounceInLeft">
                            <div class="images">
                                <iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Ftruyenhinh.dai.5%2Fvideos%2F809437822925541%2F&show_text=0&width=560" width="560" height="250" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>
                            </div>
{{--                            <div class="nav-images">--}}
{{--                                <iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Ftruyenhinh.dai.5%2Fvideos%2F809437822925541%2F&show_text=0&width=560" width="560" height="315" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>--}}
{{--                            </div>--}}
                        </div>
                    </div>
                    <div class="col-md-7 col-sm-12 col-xs-12">
                        <div class="right wow bounceInRight">
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="item-new">
                                        <div class="left1">
                                            <div class="images">
                                                <a href="/loi-ich-dua-luoi"><img
                                                            style="height: 92px"
                                                            src="{{asset('images/loiichdualuoi.jpg')}}"
                                                            alt=""></a>
                                            </div>
                                        </div>
                                        <div class="right1">
                                            <h3 class="title1">
                                                <a href="/loi-ich-dua-luoi">13 lợi ích tuyệt vời khi bạn ăn dưa lưới
                                                </a>
                                            </h3>
                                            <p class="date">13/07/2019</p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="item-new">
                                        <div class="left1">
                                            <div class="images">
                                                <a href="/ky-thuat-trong-dua"><img
                                                            style="height: 92px"
                                                            src="{{asset('images/trongdua.jpg')}}"
                                                            alt=""></a>
                                            </div>
                                        </div>
                                        <div class="right1">
                                            <h3 class="title1"><a
                                                        href="/ky-thuat-trong-dua">Kỹ Thuật Trồng Cây Dưa Lưới Tại Nhà
                                                    Chỉ 3 Tháng Đã Có Quả Ăn</a>
                                            </h3>
                                            <p class="date">06/07/2019</p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="item-new">
                                        <div class="left1">
                                            <div class="images">
                                                <a href="/cong-nghe-tuoi-israel"><img
                                                            style="height: 92px"
                                                            src="{{asset('images/tuoiisrael.png')}}"
                                                            alt=""></a>
                                            </div>
                                        </div>
                                        <div class="right1">
                                            <h3 class="title1"><a
                                                        href="/cong-nghe-tuoi-israel">GIỚI THIỆU HỆ THỐNG TƯỚI NHỎ GIỌT
                                                    CỦA ISRAEL</a>
                                            </h3>
                                            <p class="date">20/06/2019</p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="item-new">
                                        <div class="left1">
                                            <div class="images">
                                                <a href="/dua-luoi-nha-mang"><img
                                                            style="height: 92px"
                                                            src="{{asset('images/duanhamang.jpg')}}"
                                                            alt=""></a>
                                            </div>
                                        </div>
                                        <div class="right1">
                                            <h3 class="title1"><a
                                                        href="/dua-luoi-nha-mang">Dưa lưới trong nhà màng được cung cấp
                                                    dinh dưỡng theo bữa</a>
                                            </h3>
                                            <p class="date">20/06/2019</p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="product-home">
        <div class="container">
            <h2 class="title1 wow fadeInUp"><a href="/san-pham">Nông Sản Tân Hùng có
                    gì?</a></h2>
            <p class="desc-desc wow fadeInUp">Những nhóm sản phẩm chúng tôi phân phối và hợp tác</p>
        </div>
        <div class="item-product wow fadeInUp">
            <div class="item-product1">
                <div class="container">
                    <div class="list-fuproducts">
                        <div class="item">
                            <div class="images">
                                <a href="/san-pham">
                                    <img style="height: 130px;"
                                         src="{{asset('images/dua_luoi.png')}}"
                                         alt=""></a>
                            </div>
                            <h3 class="title1" style="color: #fff;">Dưa Lưới</h3>
                        </div>
                        <div class="item">
                            <div class="images">
                                <a href="/san-pham">
                                    <img style="height: 130px;"
                                         src="{{asset('images/dua_chuot.png')}}"
                                         alt=""></a>
                            </div>
                            <h3 class="title1" style="color: #fff;">Dưa Chuột Nhật</h3>
                        </div>
                        <div class="item">
                            <div class="images">
                                <a href="/san-pham">
                                    <img style="height: 130px;"
                                         src="{{asset('images/thuy-canh.png')}}"
                                         alt=""></a>
                            </div>
                            <h3 class="title1" style="color: #fff;">Thủy Canh</h3>
                        </div>
                        <div class="item">
                            <div class="images">
                                <a href="/san-pham">
                                    <img style="height: 130px;"
                                         src="{{asset('images/buoi.png')}}"
                                         alt=""></a>
                            </div>
                            <h3 class="title1" style="color: #fff;">Bưởi Phúc Trạch</h3>
                        </div>
                        <div class="item">
                            <div class="images">
                                <a href="/san-pham">
                                    <img style="height: 130px;"
                                         src="{{asset('images/oi.png')}}"
                                         alt=""></a>
                            </div>
                            <h3 class="title1" style="color: #fff;">Ổi Nữ Hoàng</h3>
                        </div>
                        <div class="item">
                            <div class="images">
                                <a href="/san-pham">
                                    <img style="height: 130px;"
                                         src="{{asset('images/duahau.png')}}"
                                         alt=""></a>
                            </div>
                            <h3 class="title1" style="color: #fff;">Dưa Hấu</h3>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </section>
    <section class="technology-home">
        <div class="container">
            <h2 class="title1 wow fadeInUp"><a style="color: rgb(255, 255, 255);"
                                               href="/cong-nghe">Công nghệ</a></h2>
            <p class="desc-desc wow fadeInUp">Những sản phẩm trên được sản xuất dựa trên nền tảng công nghệ mới nhất
                của Dưa Lưới Tân</p>
            <div class="nav-technology sliderowl-custom wow fadeInUp owl-carousel">
                <div class="item">
                    <div class="images">
                        <a href="/cong-nghe">
                            <img class="iblur" style="border-radius: 70px; height: 140px;width: auto;"
                                 src="https://vineco.net.vn/uploads/Công nghệ/_HNE2494.jpg" alt="">
                            <img src="https://vineco.net.vn/uploads/Icon/icon-cn5.png" alt="" class="iconshow"/>
                        </a>

                    </div>
                    <a href="/cong-nghe">
                        <h4 class="title">
                            <a style="color: #fff" href="/cong-nghe">Công nghệ sau thu hoạch và chế biến</a></h4>
                    </a>
                </div>
                <div class="item">
                    <div class="images">
                        <a href="/cong-nghe">
                            <img class="iblur" style="border-radius: 70px; height: 140px;width: auto;"
                                 src="https://vineco.net.vn/uploads/Công nghệ/tưới tiêu tự động.jpg" alt="">
                            <img src="https://vineco.net.vn/uploads/Icon/icon-cn4.png" alt="" class="iconshow"/>
                        </a>
                    </div>
                    <a href="/cong-nghe ">
                        <h4 class="title">
                            <a style="color: #fff" href="/cong-nghe">Công nghệ tưới tiêu tự động, khoa học</a>
                        </h4>
                    </a>
                </div>
                <div class="item">
                    <div class="images">
                        <a href="/cong-nghe">
                            <img class="iblur" style="border-radius: 70px; height: 140px;width: auto;"
                                 src="{{asset('images/loiichdualuoi.jpg')}}" alt="">
                            <img src="https://vineco.net.vn/uploads/Icon/icon-cn2.png" alt="" class="iconshow"/>
                        </a>
                    </div>
                    <a href="/cong-nghe">
                        <h4 class="title">
                            <a style="color: #fff" href="/cong-nghe">Công
                                nghệ sản xuất dưa lưới trong nhà màng</a></h4>
                    </a>
                </div>
                <div class="item">
                    <div class="images">
                        <a href="/cong-nghe">
                            <img class="iblur" style="border-radius: 70px; height: 140px;width: auto;"
                                 src="https://vineco.net.vn/uploads/Công nghệ/_HNE0856.jpg" alt="">
                            <img src="https://vineco.net.vn/uploads/Icon/icon-cn2.png" alt="" class="iconshow"/>
                        </a>
                    </div>
                    <a href="/cong-nghe">
                        <h4 class="title">
                            <a style="color: #fff" href="/cong-nghe">
                                Công nghệ trồng các loại cây ăn quả</a></h4>
                    </a>
                </div>
                <div class="item">
                    <div class="images">
                        <a href="/cong-nghe">
                            <img class="iblur" style="border-radius: 70px; height: 140px;width: auto;"
                                 src="https://vineco.net.vn/uploads/Công nghệ/thoi-tiet-nong-vu-04-09.jpg" alt="">
                            <img src="https://vineco.net.vn/uploads/Icon/icon-cn1.png" alt="" class="iconshow"/>
                        </a>
                    </div>
                    <a href="/cong-nghe">
                        <h4 class="title"><a style="color: #fff"
                                             href="/cong-nghe">
                                Công nghệ tự động hoá, cơ giới hóa</a></h4>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <section class="nongtruong">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="left wow bounceInLeft">
                        <h2 class="title1">Trang Trại</h2>
                        <p>Trạng trại với diện tích hàng chục hecta tại vùng đất màu mỡ của xã Hương Trà Huyện Hương
                            Khê</p>
                        <p>Hằng năm trang trại chúng tôi luôn cải tiến công nghệ và chất lượng sản phẩm để mang đến cho
                            người tiêu dùng những sản phẩm nông nghiệp sạch </p>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="right wow bounceInRight">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15162.832308184312!2d105.69570768505422!3d18.177356901755232!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3139aa23557b7f8b%3A0xc7f601adb3bb0529!2zdHQuIEjGsMahbmcgS2jDqiwgSMawxqFuZyBLaMOqIERpc3RyaWN0LCBIYSBUaW5oLCBWaWV0bmFt!5e0!3m2!1sen!2s!4v1601993920345!5m2!1sen!2s"
                                width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""
                                aria-hidden="false" tabindex="0"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection