@extends('FrontEnd.layouts.master')

@section('main-content')
    <section class="content-info">
        <div class="khung-content wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
            <div class="content-main w-clear"><h2>1. Nguồn gốc</h2>
                <p>Dưa lưới tên khoa học là Cucumis melo, thuộc họ bầu bí, có lớp vỏ cứng màu lục với những đường gân
                    trắng đan nhau như lớp lưới rất độc đáo. Quả dưa lưới có trọng lượng trung bình từ 1.5kg đến 3.5kg.
                    Dưa lưới có nguồn gôc từ châu Phi và Ấn Độ. Người Ai Cập là người đầu tiên trồng giống cây này, ban
                    đầu dưa lưới nhỏ và ít ngọt, sau thời gian nó không ngừng phát triển cho đến nay trở thành loại trái
                    to và ngọt.</p>
                <h2>2. Sản xuất dưa lưới bằng công nghệ cao.</h2>

                <p>Dưa lưới hiện nay đang được trồng phổ biến rộng rãi tại nhiều nước trên thế giới như Nhật Bản, Hàn
                    Quốc,... Ở Việt Nam dưa lưới mới được trồng một số năm gần đây tại các khu có áp dụng công nghệ cao
                    như TP HCM, Bình Dương,... Dưa lưới ở đây được trồng trong các nhà màng, kiểu nhà trồng cây công
                    nghệ Israel, mỗi nhà tứ bề có lưới ngăn côn trùng, mái thưng bằng vải nhựa chuyên dùng có khả năng
                    che mưa, gió. Cây dưa được trồng trong các giá thể (bầu) lớn, và được lót bạt cao su nên cây không
                    trực tiếp tiếp đất.Dưa lưới được trồng bằng hệ thống tưới nước nhỏ dọt, nước được dẫn qua hệ thống
                    ống từ máy tưới đến tận gốc dưa theo đúng mức độ yêu cầu, phân bổ số lần tưới trong ngày theo tuổi
                    cây, theo điều kiện thời tiết. Việc bón phân gồm các chủng loại phân, liều dùng, được pha vào hệ
                    thống nước, đảm bảo đủ chất dinh dưỡng, các nguyên tố vi lượng cho cây phát triển tốt nhất.Vậy nên,
                    tuy mới xuất hiện không lâu nhưng dưa lưới được rất nhiều người tin dùng vì chất lượng ăn rất ngon
                    và giá trị dinh dưỡng cao, hơn nữa dưa lưới yêu cầu kỹ thuật canh tác khá cao nên hiện tại đang được
                    trồng và quản lý theo tiêu chuẩn VietGap và Gapglobal vì vậy hoàn toàn đảm bảo an toàn khi sử
                    dụng.</p>

                <h2>3. Giá trị dinh dưỡng</h2>

                <p>Theo các nhà nghiên cứu Pháp, dùng nước ép dưa lưới mỗi ngày có thể giúp chúng ta chống lại mệt mỏi
                    và stress một cách có hiệu quả. Được lớp vỏ dày bảo vệ nên trái luôn mọng nước (88%), hàm lượng
                    potassium (300 mg/100g) đáng kể nên dưa lưới có tính năng thanh lọc, lợi niệu, chất xơ (1g/100g)
                    giúp nhuận trường.Theo kết quả phân tích định lượng các chất khoáng và vitamin thì cứ 100g dưa lưới
                    có chứa: Acid Folic (21 μg), Nianci (0.734 mg), beta-carotene (2020 μg), Magiê (12 mg), sắt (0,21
                    mg), canxi (9mg), Vitamin C (36,7 mg), vitamin A (169 μg), năng lượng (34 kcal)<br>
                    Hiệp hội ung thư Hoa Kỳ khuyến cáo nên ăn nhiều dưa lưới vì chúng được xem là một trong những loại
                    thực phẩm có khả năng đánh bại căn bệnh ưng thu ruột và những khối u ác tính.</p>

                <p>Một lưu ý nhỏ: người bệnh cảm sốt hoặc mới chớm khỏi bệnh, phụ nữ vừa sinh con trong tháng, tạng hàn
                    thì không nên dùng dưa lưới.
                </p>
                <h2>4. Chọn dưa lưới</h2>
                <p>Chọn trái nặng tay, là dấu hiệu dưa ngọt. Cuống bị rụng mất hay nứt ra là dưa vừa mới chín tới.
                    Dưa lưới là loại trái vẫn tiếp tục chín sau khi hái nên sau khi hái từ vườn về, bạn nên để 1 đến 2
                    ngày
                    cho cuống dưa héo đi mới bổ ra ăn. Bạn sẽ cảm nhận được rõ nét nhất vị ngọt và thơm của giống dưa
                    này.Trái dưa lưới được trồng bằng công nghệ nhà màng thông thường để được thời gian tương đối dài,
                    từ 7
                    đến 10 ngày, trong môi trường tự nhiên mà không lo hu hỏng.</p>
                <h2>5. Thưởng thức dưa lưới</h2>
                <p>
                    Có rất nhiều cách thưởng thức dưa lưới, bạn có thể bổ ra ướp lạnh và ăn trực tiếp cũng có thể ép lấy
                    nước hoặc thêm đường, sữa, đá xay bạn sẽ có món sinh tố ngon tuyệt cho ngày hè. Dưa lưới có thể chế
                    biến
                    thành các món ngọt và mặn, bạn có thể làm món khoai từ trộn sốt dưa lưới, hay món salad dưa lưới vô
                    cùng
                    hấp dẫn.</p>
            </div>
        </div>
    </section>
@endsection