@extends('FrontEnd.layouts.master')

@section('main-content')
    <section class="main-contact">
        <div class="container">
            <div class="row">
                <div class="col-md-5 col-sm-6 col-xs-12">
                    <div class="right wow bounceInRight" style="visibility: visible; animation-name: bounceInRight;">
                        <h3 class="top-title1">Thông tin liên hệ</h3>
                        <div class="nav-right">
                            <div class="icon">
                                <img src="https://vineco.net.vn/public/vineco/images/icon-phone.png" alt="">
                            </div>
                            <div class="nav-icon">
                                <ul>
                                    <li><span class="span1">Mr Hùng :</span><span class="span2">0349979888</span> <div class="clearfix"></div></li>
                                    <li><span class="span1">Mr Tân :</span><span class="span2">0357997888</span> <div class="clearfix"></div></li>
                                </ul>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="mang-xh">
                            <h4>Mạng xã hội</h4>
                            <div class="nav-mangxh">
                                <div id="fb-root"></div>
                                <div class="fb-page"
                                     data-href="https://www.facebook.com/mrsimplestyle"
                                     data-width="500"
                                     data-hide-cover="false"
                                     data-show-facepile="false"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection