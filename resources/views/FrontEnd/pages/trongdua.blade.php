@extends('FrontEnd.layouts.master')

@section('main-content')
    <section class="content-info content-new-detailt">
        <div class="khung-content">
            <h1 class="primary-title">Kỹ Thuật Trồng Cây Dưa Lưới Tại Nhà Chỉ 3 Tháng Đã Có Quả Ăn</h1>
            <div class="date-detailt">
                <div class="left">
                    <div class="date">13/07/2019</div>
                </div>
                <div class="right">

                </div>
                <div class="clearfix"></div>
            </div>
            <div class="hc2-item-col hc2-item-col-8 hc2-item-no-margin">
                <div class="entry-content" id="entry-content">
                    <div class="hc2-content-single-featured-image fade-in text-center">
                        <img width="640" height="426" src="{{asset('images/trongdua.jpg')}}">
                    </div>
                    <div class="entry-content-body" style="margin-top: 15px">
                        <div class="content-main w-clear">
                            <h2>Thời vụ</h2>

                            <p>Chúng ta có thể &nbsp;trồng dưa lưới từ tháng 2 đến đầu tháng 3, thu hoạch tháng 4-5. Cũng có thể trồng từ 8- 9, thu tháng 11-12. Tuy nhiên xen giữa 2 vụ này vẫn có thể trồng dưa lưới nên trong khoảng từ tháng 2 – 9 có thể trồng dưa lưới trên chậu.</p>

                            <h2>Ánh sáng</h2>

                            <p>Do là cây ưa sáng nên nếu diện tích ban công hay sân thượng rộng hãy tận dụng.Tuy nhiên nếu ban công nhà quá hẹp, khuất bóng thì không nên trồng vì trái dưa không lớn.</p>

                            <h2>Đất trồng</h2>

                            <p>Trồng cây dưa lưới cần có đất sạch, phân trùn quế, dịch trùn quế, chậu trồng, xơ dừa. Những loại đất này bạn có thể mua tại các vựa kiểng, hoa kiểng trên toàn quốc hoặc các cửa hàng hạt giống dưa lưới.</p>

                            <h2>Kỹ thuật trồng cây dưa lưới</h2>

                            <p>Kỹ thuật trồng cây dưa lưới tại nhà bạn có thể áp dụng bằng cách gieo hạt. Bạn hãy gieo hạt vào chậu đất đã chuẩn bị như trên sau đó gieo hạt vào. &nbsp;Sau 1-2 ngày, hạt sẽ tự nảy mầm. Trong giai đoạn này, bạn không nên tưới nhiều sẽ khiến úng hạt không nảy mầm. Đất ươm hạt thường trộn thên phân trùn hoặc phân chuồng mục để bổ sung thêm dinh dưỡng cho hạt nhanh nảy mầm. Sau vài ngày thấy cây ra lá thật thì mới đem trồng vào thùng lớn.</p>

                            <h2>Chăm sóc</h2>

                            <p>Chăm sóc cây dưa lưới không cần quá cầu kỳ, tốn ít thời gian. Trong thời kì cây con, bạn không cần tưới nhiều mà chờ khi cây ra 3-4 lá thì mới pha dung dịch tưới từ 0.5 – 0.8 lít/ngày cho cây. Cần phải tưới thường xuyên để giữ độ ẩm cho cây phát triển tốt nhưng tránh để cây bị úng nước.</p>

                            <h2>Làm giàn</h2>

                            <p>Quan sát thấy khi cây dưa lưới phát triển được 4-5 lá. Có thể đóng cọc cho cây leo lên hoặc lấy dây ni-long buộc nhẹ vào hàng rào ban công để cây đưa lưới có thể bám vào phát triển tốt. Còn nếu bạn muốn trồng lâu dài thì đầu tư hẵn một giàn leo bằng lưới sắt đảm bảo cho cây phát triển cực mạnh, ra nhiều hoa, đậu nhiều quả.</p>

                            <h2>Cắt tỉa lá và bấm ngọn</h2>

                            <p>Muốn cho cây tiếp tục ra hoa và đậu quả, bạn chỉ cần ngắt hết những chiếc lá dưới gốc cây. Cho đến khi cây ra khoảng 8 đến 10 lá hãy để nhánh đó lại. Khi đó, nách lá đầu tiên ra hoa cái. Khi nhánh phát triển dài ra, cần tiếp tục bấm ngọn, chỉ nên để lại 1 hoa cái và 1 lá cạnh bông hoa cái đó, từ đây hoa sẽ nở và đậu quả.</p>

                            <h2>Thu hoạch quả dưa lưới</h2>

                            <p>Sau khoảng 3 tháng từ khi gieo trồng cây dưa lưới sẽ bắt đầu cho thu hoạch. Sau khi thu hoạch dưa lưới cần để nơi thoáng mát khô ráo, bảo quản dưa lưới thêm một hai ngày nữa khi ăn dưa sẽ ngọt và ngon hơn.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection