@extends('FrontEnd.layouts.master')

@section('main-content')
    <section class="top-product wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
        <div class="container">
            <div class="list-caterm">
                <div class="item-tabs itemhightlight">
                    <a href="">
                        <div class="images">
                            <img src="https://vineco.net.vn/uploads/Icon/hot.png" alt="">
                        </div>
                        <h4 class="title">
                            Sản phẩm HOT
                        </h4>
                    </a>
                </div>

                <div class="slider-top-product owl-carousel owl-loaded owl-drag">
                    <div class="owl-stage-outer">
                        <div class="owl-stage"
                             style="transform: translate3d(-949px, 0px, 0px); transition: all 0s ease 0s; width: 3642px;">
                            <div class="owl-item cloned" style="width: 158.333px;">
                                <div class="item-tabs">
                                    <a href="#">
                                        <div class="images">
                                            <img src="{{asset('images/dua_luoi.png')}}" alt="">
                                        </div>
                                        <h4 class="title">
                                            Dưa Lưới
                                        </h4>
                                    </a>
                                </div>
                            </div>
                            <div class="owl-item cloned" style="width: 158.333px;">
                                <div class="item-tabs">
                                    <a href="">
                                        <div class="images">
                                            <img src="{{asset('images/dua_chuot.png')}}" alt="">
                                        </div>
                                        <h4 class="title">
                                            Dưa Chuột Nhật
                                        </h4>
                                    </a>
                                </div>
                            </div>
                            <div class="owl-item cloned" style="width: 158.333px;">
                                <div class="item-tabs">
                                    <a href="">
                                        <div class="images">
                                            <img src="{{asset('images/thuy-canh.png')}}" alt="">
                                        </div>
                                        <h4 class="title">
                                            Thủy Canh
                                        </h4>
                                    </a>
                                </div>
                            </div>
                            <div class="owl-item cloned" style="width: 158.333px;">
                                <div class="item-tabs">
                                    <a href="">
                                        <div class="images">
                                            <img src="{{asset('images/buoi.png')}}" alt="">
                                        </div>
                                        <h4 class="title">
                                            Bưởi Phúc Trạch
                                        </h4>
                                    </a>
                                </div>
                            </div>
                            <div class="owl-item cloned" style="width: 158.333px;">
                                <div class="item-tabs">
                                    <a href="">
                                        <div class="images">
                                            <img src="{{asset('images/oi.png')}}" alt="">
                                        </div>
                                        <h4 class="title">
                                            Ổi Nữ Hoàng
                                        </h4>
                                    </a>
                                </div>
                            </div>
                            <div class="owl-item cloned" style="width: 158.333px;">
                                <div class="item-tabs">
                                    <a href="">
                                        <div class="images">
                                            <img src="{{asset('images/duahau.png')}}" alt="">
                                        </div>
                                        <h4 class="title">
                                            Dưa Hấu
                                        </h4>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="owl-nav">
                        <button type="button" role="presentation" class="owl-prev"><i class="fa fa-chevron-left"></i>
                        </button>
                        <button type="button" role="presentation" class="owl-next"><i class="fa fa-chevron-right"></i>
                        </button>
                    </div>
                    <div class="owl-dots disabled"></div>
                </div>
            </div>
        </div>
        <div class="nav-slider-product">
            <div class="tab-content ">
                <div class="tab-pane active" id="1">
                    <div class="container">
                        <ul class="nav nav-tabs">
                        </ul>

                    </div>
                </div>
            </div>
        </div>

    </section>
    <div class="tab-content ">
        <section class="tab-pane active main-product" id="1a">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-6 col-xs-6">
                        <div class="item">
                            <div class="images" style="height: 185px">
                                <a href="https://vineco.net.vn/dua-hau-do-co-hat"><img style="height: 185px"
                                                                                       src="{{asset('images/dua_luoi.png')}}"
                                                                                       alt=""></a>
                            </div>
                            <h3 class="title" style="height: 90px;">Dưa Lưới</h3>
                            <p class="maso" style="height: 34px"></p>
                            <p class="date1" style="height:  42px;"><i class="fa fa-calendar"></i>Tháng: 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12
                            </p>

                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-6">
                        <div class="item">
                            <div class="images" style="height: 185px">
                                <a href="https://vineco.net.vn/dua-hau-do-co-hat"><img style="height: 185px"
                                                                                       src="{{asset('images/dua_chuot.png')}}"
                                                                                       alt=""></a>
                            </div>
                            <h3 class="title" style="height: 90px;">Dưa Chuột Nhật</h3>
                            <p class="maso" style="height: 34px"></p>
                            <p class="date1" style="height:  42px;"><i class="fa fa-calendar"></i>Tháng: 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12
                            </p>

                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-6">
                        <div class="item">
                            <div class="images" style="height: 185px">
                                <a href="https://vineco.net.vn/dua-hau-do-co-hat"><img style="height: 185px"
                                                                                       src="{{asset('images/thuy-canh.png')}}"
                                                                                       alt=""></a>
                            </div>
                            <h3 class="title" style="height: 90px;">Thủy Canh</h3>
                            <p class="maso" style="height: 34px"></p>
                            <p class="date1" style="height:  42px;"><i class="fa fa-calendar"></i>Tháng: 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12
                            </p>

                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-6">
                        <div class="item">
                            <div class="images" style="height: 185px">
                                <a href="https://vineco.net.vn/dua-hau-do-co-hat"><img style="height: 185px"
                                                                                       src="{{asset('images/buoi.png')}}"
                                                                                       alt=""></a>
                            </div>
                            <h3 class="title" style="height: 90px;">Bưởi Phúc Trạch</h3>
                            <p class="maso" style="height: 34px"></p>
                            <p class="date1" style="height:  42px;"><i class="fa fa-calendar"></i>Tháng: 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12
                            </p>

                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-6">
                        <div class="item">
                            <div class="images" style="height: 185px">
                                <a href="https://vineco.net.vn/dua-hau-do-co-hat"><img style="height: 185px"
                                                                                       src="{{asset('images/oi.png')}}"
                                                                                       alt=""></a>
                            </div>
                            <h3 class="title" style="height: 90px;">Ổi Nữ Hoàng</h3>
                            <p class="maso" style="height: 34px"></p>
                            <p class="date1" style="height:  42px;"><i class="fa fa-calendar"></i>Tháng: 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12
                            </p>

                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-6">
                        <div class="item">
                            <div class="images" style="height: 185px">
                                <a href="https://vineco.net.vn/dua-hau-do-co-hat"><img style="height: 185px"
                                                                                       src="{{asset('images/duahau.png')}}"
                                                                                       alt=""></a>
                            </div>
                            <h3 class="title" style="height: 90px;">Dưa
                                    hấu</h3>
                            <p class="maso" style="height: 34px"></p>
                            <p class="date1" style="height:  42px;"><i class="fa fa-calendar"></i>Tháng: 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12
                            </p>

                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>
@endsection