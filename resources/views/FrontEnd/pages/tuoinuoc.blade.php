@extends('FrontEnd.layouts.master')

@section('main-content')
    <section class="content-info content-new-detailt">
        <div class="khung-content">
            <h1 class="primary-title">GIỚI THIỆU HỆ THỐNG TƯỚI NHỎ GIỌT CỦA ISRAEL</h1>
            <div class="date-detailt">
                <div class="left">
                    <div class="date">13/07/2019</div>
                </div>
                <div class="right">

                </div>
                <div class="clearfix"></div>
            </div>
            <div class="hc2-item-col hc2-item-col-8 hc2-item-no-margin">
                <div class="entry-content" id="entry-content">

                    <div class="entry-content-body">
                        <div class="entry"><p>Đất nước Israel với diện tích nhỏ khoảng trên 20 nghìn km2, lớn hơn tỉnh
                                Nghệ An một chút. Với đất chất không được thiên nhiên ưu ái khi chỉ có khoảng 20% diện
                                tích có thể trồng trọt còn lại phần lớn là cao nguyên đá và cát. Nhưng con người Israel
                                luôn làm thế giới phải nể phục khi áp dụng công nghệ kỹ thuật vào nông nghiệp. Tuy là
                                một phần rất nhỏ nhưng hệ thống tưới nhỏ giọt Israel chính là một phần làm nên thành
                                công đấy.</p>
                            <p>Nước là thứ tài nguyên mà đất nước này luôn thiếu và quý như vàng. Bài toán tận dụng
                                nguồn nước triệt để và mang lại hiệu quả cao được cân nhắc trong mọi lĩnh vực và cuộc
                                sống. Trong nông nghiệp thì nước lại càng cần thiết khi sử dụng vậy làm cách nào để có
                                thể cung cấp đủ nước cho nông nghiệp khi đó hệ thống tưới nhỏ giọt Israel đã chứng tỏ là
                                một giải pháp hữu hiệu cho vùng đấy này. Bằng chứng là nông nghiệp Israel mang về 3 tỷ
                                USD mỗi năm. Và là nước xuất khẩu nông sản hàng đầu thế giới. Đến cả những nền nông
                                nghiệp của các đất nước tiên tiến như Mỹ, Nhật,… cũng áp dụng &nbsp;<span
                                        data-sheets-value="{&quot;1&quot;:2,&quot;2&quot;:&quot;h\u1ec7 th\u1ed1ng t\u01b0\u1edbi nh\u1ecf gi\u1ecdt theo c\u00f4ng ngh\u1ec7 israel&quot;}"
                                        data-sheets-userformat="{&quot;2&quot;:2749,&quot;3&quot;:{&quot;1&quot;:0},&quot;5&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:{&quot;1&quot;:2,&quot;2&quot;:0}},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;6&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:{&quot;1&quot;:2,&quot;2&quot;:0}},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;7&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:{&quot;1&quot;:2,&quot;2&quot;:0}},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;8&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:{&quot;1&quot;:2,&quot;2&quot;:0}},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;10&quot;:2,&quot;12&quot;:0,&quot;14&quot;:{&quot;1&quot;:2,&quot;2&quot;:0}}">hệ thống tưới nhỏ giọt theo công nghệ Israel trong trồng trọt.</span>
                            </p>
                            <p>Mọi hoạt động đều xung quanh tiêu chí: Tiết kiệm nước. Sử dụng nước với người dân Israel
                                không đơn thuần là công nghệ mà là nghệ thuật. Tìm hiểu cách họ sử dụng nước hiệu quả
                                thế nào có thể khiến chúng ta thấy xấu hổ và cảm thấy may mắn. Tuy nhiên đây là vấn đề
                                sớm hay muộn chúng ta phải quan tâm đến.</p>
                            <p style="text-align: center;"><img class="alignnone size-full wp-image-482"
                                                                src="http://truongphuthuan.com/wp-content/uploads/2017/05/hệ-thống-tưới-nhỏ-giọt-3.png"
                                                                alt="Giới thiệu hệ thống tưới nhỏ giọt của Israel"
                                                                width="524" height="300"
                                                                srcset="http://truongphuthuan.com/wp-content/uploads/2017/05/hệ-thống-tưới-nhỏ-giọt-3.png 524w, http://truongphuthuan.com/wp-content/uploads/2017/05/hệ-thống-tưới-nhỏ-giọt-3-300x172.png 300w"
                                                                sizes="(max-width: 524px) 100vw, 524px"></p>
                            <p><strong>Hệ thống tưới nước nhỏ giọt theo công nghệ Israel.</strong></p>
                            <p>Bí quyết trong việc sử dụng&nbsp;hệ thống tưới nhỏ giọt theo công nghệ Israel&nbsp;này
                                của người dân vẫn là tiết kiệm nước thôi. Nhưng đây là công nghệ và thiết bị hiện đại từ
                                van điều khiển tự động, lọc nhiều tầng, vòi phun áp lực thấp để&nbsp;phun mưa loại nhỏ.
                                Nhờ hệ thống tưới nhỏ giọt Israel giúp nông dân tiết kiệm được 60% nước.</p>
                            <p>Những cánh đồng của Israel đều được sử dụng mạng lưới dẫn nước này, gồm nhiều ống nhỏ như
                                những mao mạnh trong cơ thể để dẫn nước tới từng gốc cây. Hệ thống <span
                                        data-sheets-value="{&quot;1&quot;:2,&quot;2&quot;:&quot;h\u1ec7 th\u1ed1ng t\u01b0\u1edbi nh\u1ecf gi\u1ecdt c\u1ee7a israel&quot;}"
                                        data-sheets-userformat="{&quot;2&quot;:2749,&quot;3&quot;:{&quot;1&quot;:0},&quot;5&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:{&quot;1&quot;:2,&quot;2&quot;:0}},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;6&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:{&quot;1&quot;:2,&quot;2&quot;:0}},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;7&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:{&quot;1&quot;:2,&quot;2&quot;:0}},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;8&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:{&quot;1&quot;:2,&quot;2&quot;:0}},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;10&quot;:2,&quot;12&quot;:0,&quot;14&quot;:{&quot;1&quot;:2,&quot;2&quot;:0}}">tưới nhỏ giọt của israel</span>&nbsp;thông
                                mình này được điều khiển bởi máy tính và cảm biến sẽ tự động đóng mở van tưới khi nhận
                                thấy độ ẩm của rễ cây đạt mức thích hợp.</p>
                            <p>Hệ thống này còn thực hiện luôn nhiệm vụ bón phân cung cấp chất dinh dưỡng tới rễ cây khi
                                người ta hòa phân bón vào bể chứa nước và nước sẽ chuyển tới từng bộ rễ. Kết hợp với hệ
                                thống phun sương cho những loại cây cần tưới vào lá thì hệ thống này đem lại hiệu quả
                                thật tuyệt vời.</p>
                            <p style="text-align: center;"><img class="alignnone size-full wp-image-483"
                                                                src="http://truongphuthuan.com/wp-content/uploads/2017/05/hệ-thống-tưới-nhỏ-giọt-4.png"
                                                                alt="hệ thống tưới nhỏ giọt " width="658" height="258"
                                                                srcset="http://truongphuthuan.com/wp-content/uploads/2017/05/hệ-thống-tưới-nhỏ-giọt-4.png 658w, http://truongphuthuan.com/wp-content/uploads/2017/05/hệ-thống-tưới-nhỏ-giọt-4-300x118.png 300w"
                                                                sizes="(max-width: 658px) 100vw, 658px"></p>
                            <p><strong>Áp dụng hệ thống tưới nhỏ giọt của Israel cho Việt Nam</strong></p>
                            <p>Nhận thấy lợi ích lớn và hiệu quả cao, những người dân Việt Nam đã học hỏi và vận dụng
                                công nghệ tưới nhỏ giọt này về nước ta với những vật liệu hoàn toàn địa phương và đã có
                                những thành công bước đầu như:</p>
                            <p style="text-align: center;"><img class="alignnone size-full wp-image-484"
                                                                src="http://truongphuthuan.com/wp-content/uploads/2017/05/tuoi-nho-giot-tiet-kiem-nuoc.jpg"
                                                                alt="Áp dụng hệ thống tưới nhỏ giọt của Israel"
                                                                width="660" height="330"
                                                                srcset="http://truongphuthuan.com/wp-content/uploads/2017/05/tuoi-nho-giot-tiet-kiem-nuoc.jpg 660w, http://truongphuthuan.com/wp-content/uploads/2017/05/tuoi-nho-giot-tiet-kiem-nuoc-300x150.jpg 300w"
                                                                sizes="(max-width: 660px) 100vw, 660px"></p>
                            <p>+ Giảm chi phí điện, xăng dầu cho tưới tiêu.</p>
                            <p>+ Giảm đáng kể lượng nước tưới.</p>
                            <p>+ Giảm thời gian và công sức vận chuyển nước.</p>
                            <p>+ Và rất nhiều lợi ích khác đang được hệ thống này chứng minh.</p>
                            <p><strong>Những điều khó khăn chưa làm được:</strong></p>
                            <p>+ Chưa làm được hệ thống dẫn mao mạch thực sự khi mà hệ thống lọc còn kém dẫn đến tình
                                trạng tắc, tăng chi phí bảo trì.</p>
                            <p>+ Kể cả những ống lớn theo thời gian cũng có hiện tượng tắc ngẽn gây cản trở việc tưới
                                tiêu đồng đều.</p>
                            <p>+ Điều khiển hệ thống vẫn còn thủ công.</p>
                            <p>Hy vọng rằng trong tương lại chúng ta sẽ vận dụng tốt hơn mô hình công nghệ này vào thực
                                tiễn.</p>
                            <p>Trên&nbsp;đây là những thông tin tổng quát về hệ thống tưới nhỏ giọt&nbsp;Israel mà chúng
                                tôi tổng hợp&nbsp;được.</p></div>
                    </div>
                </div>

            </div>
        </div>
    </section>
@endsection