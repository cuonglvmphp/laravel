@extends('FrontEnd.layouts.master')

@section('main-content')
    <section class="new-home">
        <div class="container">
            <h2 class="title1 wow fadeInUp"><a href="https://vineco.net.vn/news">Tin Tức</a></h2>
            <div class="nav-new-home">
                <div class="row">
                    <div class="col-md-5 col-sm-12 col-xs-12">
                        <div class="left wow bounceInLeft">
                            <div class="images">
                                <a href="https://vineco.net.vn/rau-sach-bao-la-chi-tu-2k-duy-nhat-tai-vinmart-vinmart">
                                    <img style="height: 220px"
                                         src="https://vineco.net.vn/uploads/Tin tuc/93849024_2277099029263428_7737704817232969728_o.jpg"
                                         alt=""></a>
                            </div>
                            <div class="nav-images">
                                <h3 class="title2"><a
                                            href="https://vineco.net.vn/rau-sach-bao-la-chi-tu-2k-duy-nhat-tai-vinmart-vinmart">RAU
                                        SẠCH BAO LA - CHỈ TỪ 2K - DUY NHẤT TẠI VINMART, VINMART+</a></h3>
                                <p class="date">04/05/2020
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7 col-sm-12 col-xs-12">
                        <div class="right wow bounceInRight">
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="item-new">
                                        <div class="left1">
                                            <div class="images">
                                                <a href="https://vineco.net.vn/dai-su-my-thich-thu-voi-nong-san-sach-vineco"><img
                                                            style="height: 92px"
                                                            src="https://vineco.net.vn/uploads/A1.jpg"
                                                            alt=""></a>
                                            </div>
                                        </div>
                                        <div class="right1">
                                            <h3 class="title1"><a
                                                        href="https://vineco.net.vn/dai-su-my-thich-thu-voi-nong-san-sach-vineco">Đại
                                                    sứ Mỹ thích thú với nông sản sạch VinEco</a>
                                            </h3>
                                            <p class="date">13/07/2019</p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="item-new">
                                        <div class="left1">
                                            <div class="images">
                                                <a href="https://vineco.net.vn/vineco-ap-dung-cong-nghe-tu-dong-hoa-co-gioi-hoa-trong-trong-trot"><img
                                                            style="height: 92px"
                                                            src="https://vineco.net.vn/uploads/Picture1_2.jpg"
                                                            alt=""></a>
                                            </div>
                                        </div>
                                        <div class="right1">
                                            <h3 class="title1"><a
                                                        href="https://vineco.net.vn/vineco-ap-dung-cong-nghe-tu-dong-hoa-co-gioi-hoa-trong-trong-trot">VinEco
                                                    áp dụng công nghệ tự động hóa, cơ giới hóa trong trồng trọt</a>
                                            </h3>
                                            <p class="date">06/07/2019</p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="item-new">
                                        <div class="left1">
                                            <div class="images">
                                                <a href="https://vineco.net.vn/nong-truong-vineco-chao-don-500-khach-hang-cua-vinmart-trong-thang-62019"><img
                                                            style="height: 92px"
                                                            src="https://vineco.net.vn/uploads/Tin tuc/Tin tuc - VinEco tham quan NT 062019 - 01.jpg"
                                                            alt=""></a>
                                            </div>
                                        </div>
                                        <div class="right1">
                                            <h3 class="title1"><a
                                                        href="https://vineco.net.vn/nong-truong-vineco-chao-don-500-khach-hang-cua-vinmart-trong-thang-62019">Nông
                                                    trường VinEco chào đón 500 khách hàng của VinMart trong
                                                    tháng 6/2019</a>
                                            </h3>
                                            <p class="date">20/06/2019</p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="item-new">
                                        <div class="left1">
                                            <div class="images">
                                                <a href="https://vineco.net.vn/vineco-tham-gia-ngay-hoi-song-xanh-2019"><img
                                                            style="height: 92px"
                                                            src="https://vineco.net.vn/uploads/Tin tuc/Tin tuc - VinEco ngay hoi song xanh 2019 - 01.jpg"
                                                            alt=""></a>
                                            </div>
                                        </div>
                                        <div class="right1">
                                            <h3 class="title1"><a
                                                        href="https://vineco.net.vn/vineco-tham-gia-ngay-hoi-song-xanh-2019">VinEco
                                                    tham gia &quot;Ngày hội Sống xanh” 2019</a>
                                            </h3>
                                            <p class="date">20/06/2019</p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="product-home">
        <div class="container">
            <h2 class="title1 wow fadeInUp"><a href="https://vineco.net.vn/danh-muc/rau-mam"> VinEco có gì?</a></h2>
            <p class="desc-desc wow fadeInUp">Những nhóm sản phẩm chúng tôi phân phối và hợp tác</p>
        </div>
        <div class="item-product wow fadeInUp">
            <div class="item-product1">
                <div class="container">
                    <div class="list-fuproducts">
                        <div class="item">
                            <div class="images">
                                <a href="https://vineco.net.vn/danh-muc/san-pham-hot"><img style="height: 115px;"
                                                                                           src="https://vineco.net.vn/uploads/11-san-pham-HOT.png"
                                                                                           alt=""></a>
                            </div>
                            <h3 class="title1"><a href="https://vineco.net.vn/danh-muc/san-pham-hot">Sản phẩm
                                    HOT</a></h3>
                        </div>
                        <div class="item">
                            <div class="images">
                                <a href="https://vineco.net.vn/danh-muc/rau-mam"><img style="height: 115px;"
                                                                                      src="https://vineco.net.vn/uploads/Icon/02-rau-mam.png"
                                                                                      alt=""></a>
                            </div>
                            <h3 class="title1"><a href="https://vineco.net.vn/danh-muc/rau-mam">Rau mầm</a></h3>
                        </div>
                        <div class="item">
                            <div class="images">
                                <a href="https://vineco.net.vn/danh-muc/nam"><img style="height: 115px;"
                                                                                  src="https://vineco.net.vn/uploads/Icon/sp2.png"
                                                                                  alt=""></a>
                            </div>
                            <h3 class="title1"><a href="https://vineco.net.vn/danh-muc/nam">Nấm</a></h3>
                        </div>
                        <div class="item">
                            <div class="images">
                                <a href="https://vineco.net.vn/danh-muc/cay-duoc-lieu"><img style="height: 115px;"
                                                                                            src="https://vineco.net.vn/uploads/Icon/04-thuy-canh.png"
                                                                                            alt=""></a>
                            </div>
                            <h3 class="title1"><a href="https://vineco.net.vn/danh-muc/cay-duoc-lieu">Thủy canh</a>
                            </h3>
                        </div>
                        <div class="item">
                            <div class="images">
                                <a href="https://vineco.net.vn/danh-muc/rau-an-la"><img style="height: 115px;"
                                                                                        src="https://vineco.net.vn/uploads/Icon/05-rau-an-la.png"
                                                                                        alt=""></a>
                            </div>
                            <h3 class="title1"><a href="https://vineco.net.vn/danh-muc/rau-an-la">Rau ăn lá</a></h3>
                        </div>
                        <div class="item">
                            <div class="images">
                                <a href="https://vineco.net.vn/danh-muc/rau-an-than-hoa"><img style="height: 115px;"
                                                                                              src="https://vineco.net.vn/uploads/Icon/06-rau-an-than-hoa.png"
                                                                                              alt=""></a>
                            </div>
                            <h3 class="title1"><a href="https://vineco.net.vn/danh-muc/rau-an-than-hoa">Rau ăn thân
                                    hoa</a></h3>
                        </div>
                        <div class="item">
                            <div class="images">
                                <a href="https://vineco.net.vn/danh-muc/rau-an-qua"><img style="height: 115px;"
                                                                                         src="https://vineco.net.vn/uploads/07-rau-an-qua.png"
                                                                                         alt=""></a>
                            </div>
                            <h3 class="title1"><a href="https://vineco.net.vn/danh-muc/rau-an-qua">Rau ăn quả </a>
                            </h3>
                        </div>
                        <div class="item">
                            <div class="images">
                                <a href="https://vineco.net.vn/danh-muc/rau-an-cu"><img style="height: 115px;"
                                                                                        src="https://vineco.net.vn/uploads/08-rau-an-cu.png"
                                                                                        alt=""></a>
                            </div>
                            <h3 class="title1"><a href="https://vineco.net.vn/danh-muc/rau-an-cu">Rau ăn củ</a></h3>
                        </div>
                        <div class="item">
                            <div class="images">
                                <a href="https://vineco.net.vn/danh-muc/rau-gia-vi"><img style="height: 115px;"
                                                                                         src="https://vineco.net.vn/uploads/Icon/09-rau-gia-vi.png"
                                                                                         alt=""></a>
                            </div>
                            <h3 class="title1"><a href="https://vineco.net.vn/danh-muc/rau-gia-vi">Rau gia vị </a>
                            </h3>
                        </div>
                        <div class="item">
                            <div class="images">
                                <a href="https://vineco.net.vn/danh-muc/trai-cay"><img style="height: 115px;"
                                                                                       src="https://vineco.net.vn/uploads/Icon/10-trai-cay.png"
                                                                                       alt=""></a>
                            </div>
                            <h3 class="title1"><a href="https://vineco.net.vn/danh-muc/trai-cay">Trái cây</a></h3>
                        </div>
                        <div class="item">
                            <div class="images">
                                <a href="https://vineco.net.vn/danh-muc/rau-coc"><img style="height: 115px;"
                                                                                      src="https://vineco.net.vn/uploads/Rau coc.png"
                                                                                      alt=""></a>
                            </div>
                            <h3 class="title1"><a href="https://vineco.net.vn/danh-muc/rau-coc">Rau cốc</a></h3>
                        </div>
                        <div class="item">
                            <div class="images">
                                <a href="https://vineco.net.vn/danh-muc/trai-cay-say"><img style="height: 115px;"
                                                                                           src="https://vineco.net.vn/uploads/Trai cay say.png"
                                                                                           alt=""></a>
                            </div>
                            <h3 class="title1"><a href="https://vineco.net.vn/danh-muc/trai-cay-say">Trái cây
                                    sấy </a></h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <section class="technology-home">
        <div class="container">
            <h2 class="title1 wow fadeInUp"><a style="color: rgb(255, 255, 255);"
                                               href="https://vineco.net.vn/cong-nghe">Công nghệ</a></h2>
            <p class="desc-desc wow fadeInUp">Những sản phẩm trên được sản xuất dựa trên nền tảng công nghệ mới nhất
                của VinEco</p>
            <div class="nav-technology sliderowl-custom wow fadeInUp owl-carousel">
                <div class="item">
                    <div class="images">
                        <a href="https://vineco.net.vn/cong-nghe-sau-thu-hoach-va-che-bien">
                            <img class="iblur" style="border-radius: 70px; height: 140px;width: auto;"
                                 src="https://vineco.net.vn/uploads/Công nghệ/_HNE2494.jpg" alt="">
                            <img src="https://vineco.net.vn/uploads/Icon/icon-cn5.png" alt="" class="iconshow"/>
                        </a>


                    </div>
                    <a href="https://vineco.net.vn/cong-nghe-sau-thu-hoach-va-che-bien ">
                        <h4 class="title"><a style="color: #fff"
                                             href="https://vineco.net.vn/cong-nghe-sau-thu-hoach-va-che-bien">Công
                                nghệ sau thu hoạch và chế biến</a></h4>
                    </a>
                </div>
                <div class="item">
                    <div class="images">
                        <a href="https://vineco.net.vn/cong-nghe-tuoi-tieu-tu-dong-khoa-hoc-tiet-kiem-nuoc">
                            <img class="iblur" style="border-radius: 70px; height: 140px;width: auto;"
                                 src="https://vineco.net.vn/uploads/Công nghệ/tưới tiêu tự động.jpg" alt="">
                            <img src="https://vineco.net.vn/uploads/Icon/icon-cn4.png" alt="" class="iconshow"/>
                        </a>


                    </div>
                    <a href="https://vineco.net.vn/cong-nghe-tuoi-tieu-tu-dong-khoa-hoc-tiet-kiem-nuoc ">
                        <h4 class="title"><a style="color: #fff"
                                             href="https://vineco.net.vn/cong-nghe-tuoi-tieu-tu-dong-khoa-hoc-tiet-kiem-nuoc">Công
                                nghệ tưới tiêu tự động, khoa học</a></h4>
                    </a>
                </div>
                <div class="item">
                    <div class="images">
                        <a href="https://vineco.net.vn/cong-nghe-san-xuat-gia-nam-an-nam-duoc-lieu">
                            <img class="iblur" style="border-radius: 70px; height: 140px;width: auto;"
                                 src="https://vineco.net.vn/uploads/Công nghệ/cong nghe - nam.jpg" alt="">
                            <img src="https://vineco.net.vn/uploads/Icon/icon-cn2.png" alt="" class="iconshow"/>
                        </a>


                    </div>
                    <a href="https://vineco.net.vn/cong-nghe-san-xuat-gia-nam-an-nam-duoc-lieu ">
                        <h4 class="title"><a style="color: #fff"
                                             href="https://vineco.net.vn/cong-nghe-san-xuat-gia-nam-an-nam-duoc-lieu">Công
                                nghệ sản xuất giá, nấm ăn, nấm dược liệu</a></h4>
                    </a>
                </div>
                <div class="item">
                    <div class="images">
                        <a href="https://vineco.net.vn/cong-nghe-trong-rau-thuy-canh-gia-the-rau-mam-huu-co">
                            <img class="iblur" style="border-radius: 70px; height: 140px;width: auto;"
                                 src="https://vineco.net.vn/uploads/Công nghệ/thủy canh.jpg" alt="">
                            <img src="https://vineco.net.vn/uploads/Icon/icon-cn3.png" alt="" class="iconshow"/>
                        </a>


                    </div>
                    <a href="https://vineco.net.vn/cong-nghe-trong-rau-thuy-canh-gia-the-rau-mam-huu-co ">
                        <h4 class="title"><a style="color: #fff"
                                             href="https://vineco.net.vn/cong-nghe-trong-rau-thuy-canh-gia-the-rau-mam-huu-co">Công
                                nghệ trồng rau thủy canh, giá thể, rau mầm, hữu cơ</a></h4>
                    </a>
                </div>
                <div class="item">
                    <div class="images">
                        <a href="https://vineco.net.vn/cong-nghe-trong-rau-trong-nha-kinh-nha-mang-nha-luoi">
                            <img class="iblur" style="border-radius: 70px; height: 140px;width: auto;"
                                 src="https://vineco.net.vn/uploads/Công nghệ/_HNE0856.jpg" alt="">
                            <img src="https://vineco.net.vn/uploads/Icon/icon-cn2.png" alt="" class="iconshow"/>
                        </a>


                    </div>
                    <a href="https://vineco.net.vn/cong-nghe-trong-rau-trong-nha-kinh-nha-mang-nha-luoi ">
                        <h4 class="title"><a style="color: #fff"
                                             href="https://vineco.net.vn/cong-nghe-trong-rau-trong-nha-kinh-nha-mang-nha-luoi">Công
                                nghệ trồng rau trong nhà kính</a></h4>
                    </a>
                </div>
                <div class="item">
                    <div class="images">
                        <a href="https://vineco.net.vn/cong-nghe-tu-dong-hoa-co-gioi-hoa">
                            <img class="iblur" style="border-radius: 70px; height: 140px;width: auto;"
                                 src="https://vineco.net.vn/uploads/Công nghệ/thoi-tiet-nong-vu-04-09.jpg" alt="">
                            <img src="https://vineco.net.vn/uploads/Icon/icon-cn1.png" alt="" class="iconshow"/>
                        </a>


                    </div>
                    <a href="https://vineco.net.vn/cong-nghe-tu-dong-hoa-co-gioi-hoa ">
                        <h4 class="title"><a style="color: #fff"
                                             href="https://vineco.net.vn/cong-nghe-tu-dong-hoa-co-gioi-hoa">Công
                                nghệ tự động hoá, cơ giới hóa</a></h4>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <section class="nongtruong">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="left wow bounceInLeft">
                        <a href="https://vineco.net.vn/nong-truong">
                            <h2 class="title1">Nông Trường</h2>
                        </a>
                        <p>Hệ thống nông trường của VinEco nằm tại những vùng có khí hậu và thổ nhưỡng được thiên
                            nhiên ưu đãi với chất lượng nông sản thuộc top đầu cả nước.</p>
                        <p>Hàng năm hệ thống nông trường này luôn được chăm sóc để đảm bảo cung ứng ra thị trường
                            những sản phẩm đạt tiêu chuẩn chất lượng quốc tế để phục vụ thị trường trong nước và
                            quốc tế.</p>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="right wow bounceInRight">
                        <img src="https://vineco.net.vn/uploads/38643-map.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection