@extends('FrontEnd.layouts.master')

@section('main-content')
    <section class="content-info content-new-detailt">
        <div class="khung-content">
            <h1 class="primary-title">Dưa lưới trong nhà màng được cung cấp dinh dưỡng theo bữa</h1>
            <div class="date-detailt">
                <div class="left">
                    <div class="date">13/07/2019</div>
                </div>
                <div class="right">

                </div>
                <div class="clearfix"></div>
            </div>
            <div class="hc2-item-col hc2-item-col-8 hc2-item-no-margin">
                <div class="entry-content" id="entry-content">
                    <div class="entry-content-body">
                        <strong>Cây dưa được tưới nước nhỏ giọt theo công nghệ Israel giúp giảm chi phí nhân công, quản
                            lý tốt dư lượng thuốc bảo vệ thực vật.</strong>
                        <p>Xã Gia Phú, huyện Bảo Thắng, Lào Cai có diện tích đất nông nghiệp rộng, nhiều sông suối, tạo
                            điều kiện thuận lợi cho việc phát triển nông nghiệp công nghệ cao. Tại đây, một trong những
                            mô hình canh tác kiểu mới vừa được tiến hành là trồng dưa lưới trong nhà màng.</p>
                        <p>Giống dưa lên vân lưới loại vỏ vàng, ruột cam được trồng thử nghiệm theo công nghệ Israel đã
                            bước đầu cho kết quả khả quan, chất lượng quả tốt, hương vị thơm ngon.</p>
                        <div class="hc2-content-single-featured-image fade-in text-center" style="text-align: center;">
                            <img width="640" height="426" src="{{asset('images/duaa.jpg')}}"
                                 style="text-align: center;">
                        </div>
                        <p>Sau quá trình thụ phấn, các nhánh cây đều cho quả nhưng người trồng chỉ chọn giữ lại một quả
                            trên mỗi cây. Để cắt bỏ các nhánh quả còn lại, người ta phải dùng loại kéo đã qua khử trùng
                            để đảm bảo vết cắt không bị nhiễm khuẩn, các cây không bị lây khuẩn chéo.</p>
                        <p>Tuân thủ phương pháp trồng áp dụng công nghệ cao nên toàn bộ dưa lưới đều được canh tác bán
                            thủy canh trong nhà màng. Giá thể chỉ sử dụng than trấu trộn với xơ dừa. Người trồng sẽ chủ
                            động được lịch canh tác, giảm tác động bởi yếu tố thời tiết, tiết kiệm công chăm sóc và hạn
                            chế sâu bệnh.</p>
                        <p>Ngoài ra, dưa được tưới nước nhỏ giọt theo công nghệ Israel - hệ thống tưới tự động với phân
                            bón hòa sẵn trong nước giúp giảm chi phí nhân công, quản lý được dư lượng thuốc bảo vệ thực
                            vật. Cách tưới này cũng giúp cho chất dinh dưỡng từ phân bón và lượng nước cung cấp cho cây
                            đến tận gốc, đáp ứng tốt cho từng giai đoạn sinh trưởng của cây. Dinh dưỡng cho cây cũng vì
                            thế mà được phân bổ theo bữa.</p>
                        <div class="hc2-content-single-featured-image fade-in text-center" style="text-align: center;">
                            <img width="640" height="426" src="{{asset('images/duanhamang.jpg')}}"
                                 style="text-align: center;">
                        </div>
                        <p>
                            Do dưa lưới trồng trong hệ thống nhà màng nên người trồng không phải sử dụng đến thuốc trừ
                            sâu mà chỉ dùng các loại bẫy để dính côn trùng.
                        </p>
                        <p>Với đặc thù thời tiết khí hậu của vùng, quy trình trồng một lứa dưa tại Bảo Thắng kéo dài
                            khoảng 85 đến 90 ngày. Khi quan sát thấy lưới quả căng tròn đều, lá bao ở cuống quả có màu
                            vàng nghĩa là dưa đã già, có thể cho thu hoạch. Quả đạt chất lượng là loại có lưới nổi đều
                            trên bề mặt, lưới càng dày, quả càng ngon. Dưa trồng ứng dụng công nghệ cao, quy trình chăm
                            sóc sạch, an toàn nên cho trọng lượng đạt chuẩn mùi thơm, vị ngọt thanh mát.</p>
                        <p>Hiện nay, tổng diện tích dưa lưới được trồng tại Bảo Thắng lên tới 15ha, cho sản lượng khoảng
                            45 - 50 tấn quả mỗi năm. Sản phẩm tiêu thụ chủ yếu trong tỉnh và một số địa bàn khu vực phía
                            Bắc</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection