@extends('FrontEnd.layouts.master')
@section('title', 'Thanh toán - '.  $setting->st_title)
@section('keywords', 'Thanh toán - '.  $setting->st_title)
@section('description', 'Thanh toán - '.  $setting->st_title)

@section('main-content')
    <div id="main">
        <div class="breadcrumb-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 menu-left nav-child">
                         @include('FrontEnd.layouts.menu_left')
                    </div>
                    <div class="col-md-9 slide-right">
                        <div class="col-md-9">
                            <ol class="breadcrumb">
                                <li><a href="{{ url('/') }}"><i class="fa fa-home"></i></a></li>
                                <li><a href="{{ route('cart.index') }}">Giỏ hàng </a></li>
                                <li class="active">Thanh toán </li>
                            </ol>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
        <style>
            .checkout_box{border:1px solid #ddd;border-radius:3px;overflow:hidden}.title_cart{padding:5px;border-bottom:1px solid #ddd;background:#dff0d8;color:#0f692f}.sub_title_cart h2,.title_cart h2{font-size:12px;font-weight:600;padding-left:10px;display:inline-block;line-height:26px;vertical-align:bottom;margin:0}.form_input_purchase{margin:30px 40px}.form_input_purchase input.form-control,.form_input_purchase textarea.form-control{border-radius:0}.checkout_cart{border:1px solid #ddd;border-radius:3px;overflow:hidden;background:#fff}.checkout_cart_title{border-bottom:1px dashed #ddd;line-height:30px;font-weight:500;overflow:hidden;padding-top:15px}.checkout_cart_title li{float:left;border-left:1px solid #fff;margin-right:10px}.checkout_cart_list .cart_product_name,.checkout_cart_title .cart_product_name{width:42%;text-align:left;margin-right:10px}.group_cart_product_item .cart_product_item{border-bottom:1px dotted #f7f7f7;padding-bottom:5px;width:100%;float:left;padding-top:8px}.cart_product_item li{float:left}.cart_product_images{width:50px;height:50px;text-align:center;margin-right:10px;overflow:hidden;display:inline-block;float:left}.img_mx100{max-width:100%!important;height:auto}.cart_product_title{line-height:20px;font-size:1.1em;font-weight:700;color:#ef7d00;display:block;overflow:hidden}.cart_product_title a{color:#008A97;display:block}.ui_spinner_input.txtQty_cart{border-radius:3px;height:31px;width:60px;max-width:100%;text-align:left;padding:0 10px 0 15px;font-size:16px;border:1px solid #aaa}.checkout_cart_list .cart_product_money,.checkout_cart_list .cart_product_price{width:16%;text-align:center;margin:0 5px}.checkout_footer{border-top:1px solid #eee;background-color:#f8f8f8;width:100%;padding-bottom:15px;padding-top:15px}.checkout_footer_left{float:left;width:42%;border:1px solid #e04e19;padding:10px 4px;border-radius:3px}.buy_continue:before,.checkout_footer_left:before{border-right:5px solid #ef7d00;border-top:5px solid transparent;border-bottom:5px solid transparent;position:absolute;content:"";margin-top:6px}.buy_other a{color:#ef7d00;font-weight:500;margin-left:10px;vertical-align:middle;font-size:14px;display:inline-block}.checkout_cart_list{overflow:hidden;background:#fff}.checkout_footer_right{float:left;text-align:right;width:52%;margin-right:10px;line-height:25px}.bill_row .text_right{width:62%;display:inline-block;float:left;font-size:12px}.total_order{color:#444;border-top:1px solid #ddd;clear:both;margin-top:10px;font-size:14px;padding-top:10px;font-weight:700}.checkout_submit{border:transparent;background:#ef7d00;width:114px;height:34px;line-height:34px;text-align:center;color:#fff}
        </style>
        <div class="pt-20" style="margin:0 0 100px 0">
            <div class="container">
                <div class="row">
                    <form method="POST">
                        {{ csrf_field() }}
                    <div class="col-md-7 col-sm-12 col-xs-12">
                        <div class="checkout_left" id="checkout_info">
                            <div class="checkout_box">
                                <div class="box_purchase_info">
                                    <div class="title_cart">
                                        <h2>Thông tin người nhận hàng</h2>
                                    </div>
                                    <div class="purchase_info_body form_input_purchase">
                                        <div class="form-group">
                                            <label for="tst_name">Họ tên:</label>
                                            <input type="text" class="form-control" value="{{ old('tst_name') }}" placeholder="Nguyễn văn a .." name="tst_name" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="tst_email">Email:</label>
                                            <input type="email" class="form-control" id="email" placeholder="nguyenvana@gmail.com" value="{{ old('tst_email') }}" name="tst_email" required>
                                        </div>
                                    
                                        <div class="form-group">
                                            <label for="tst_phone">Phone:</label>
                                            <input type="number" class="form-control" id="email" placeholder="0987654321" value="{{ old('tst_phone') }}" name="tst_phone" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="tst_address">Địa chỉ:</label>
                                            <input type="text" class="form-control" value="{{ old('tst_address') }}" placeholder="Nhập địa chỉ đầy đủ: số nhà, tên đường, phường xã" name="tst_address" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="province_dt">Tỉnh/Thành phố:</label>
                                            <select onchange="getdistrict($(this))" name="province_dt" id="province" class="form-control" required>
                                                <option selected="selected" value=""> --- Chọn tỉnh / thành  --- </option>
                                                @php
                                                    $getLC = DB::table('devvn_tinhthanhpho')->get();
                                                @endphp
                                                @foreach($getLC as $value)
                                                    <option value="{{ $value->matp }}">{{ $value->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="email">Quận/Huyện</label>
                                            <select id="district" name="district_dt" onchange="get_ward($(this))" class="form-control" required>
                                                <option selected="selected" value=""> --- Chọn quận/huyện --- </option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="email">Phường/Xã</label>
                                            <select id="li_check_district" name="ward_dt" class="form-control" required>
                                                <option selected="selected" value=""> --- Chọn phường/xã --- </option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="email">Ghi chú :</label>
                                            <textarea  id="" cols="" rows="4" name="tst_messages"  class="form-control">{{ old('tst_messages') }}</textarea>
                                        </div>
                                        <button type="submit" class="btn btn-default" style="border:none;border-radius:0;background:#008A97;color:#ffffff">Gửi đơn hàng</button>        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5 col-sm-12 col-xs-12 hidden-sm hidden-xs">
                        <div id="right-float" style="" class="sticky">
                            <div class="row-prod prod-item" data-pid="33373">
                                <div class="checkout_cart">
                                    <div class=" checkout_cart_border">
                                        <div class=" title_cart">
                                            <h2>Đơn hàng ({{ Cart::count() }} sản phẩm)</h2>
                                        </div>
                                        <ul class="checkout_cart_title">
                                            <li class="col cart_product_name" style="padding-left:10px">Sản phẩm</li>
                                            <li class="col cart_product_count">Số lượng</li>
                                            <li class="col cart_product_price hidden-xs">Đơn giá (đ)</li>
                                            <li class="col cart_product_money">Thành tiền (đ)</li>
                                        </ul>
                                        <div class="col checkout_cart_list" style="padding-left:10px">
                                            <ul class="group_cart_product_item">
                                                @php
                                                    $cart = Cart::content();
                                                @endphp
                                                @foreach( $cart as $k => $ca)
                                                <li class="cart_product_item">
                                                    <ul>
                                                        <li class="cart_product_name">
                                                            <span class="col cart_product_images hidden-480">
                                                                <img class="img_mx100" src="{{ asset('uploads/images/Products/'.$ca->options->hinhanh) }}" alt="{{ $ca->name }}"> 
                                                            </span>
                                                            <span class="cart_product_info">
                                                                <p class="cart_product_title">
                                                                    <a href="">{{ $ca->name }}</a>
                                                                </p>
                                                            </span>
                                                        </li>
                                                        <li class="cart_product_count" style="width: 50px;text-align: center;">
                                                            <span class="add_number_cart">
                                                                <span class="ui_spinner ui_widget ui_widget_content ui_corner_all">
                                                                    {{ $ca->qty }}
                                                                </span>
                                                            </span>
                                                        </li>
                                                        <li class="cart_product_price hidden-xs">
                                                            <p class="cart_product_price_meta">{{ formatPrice($ca->price) }}</p>
                                                        </li>
                                                        <li class="cart_product_money"><span id="item_total997">{{ formatPrice($ca->price * $ca->qty) }}</span></li>
                                                    </ul>
                                                </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                        <div class="col checkout_footer clearfix" style="padding-left:10px">
                                            <div class="row_item">
                                                <div class="checkout_footer_left">
                                                    <span class="buy_other">
                                                        <a href="/" target="_parent" class="link_other_choice">Chọn thêm sản phẩm khác</a>
                                                    </span>
                                                </div>
                                                <div class="checkout_footer_right">
                                                    <div class="bill_infomation" id="bill_info_mb">
                                                        <div class="bill_row">
                                                            <div class="text_right">Tạm tính:</div>
                                                            <div class="col text_bill">
                                                                <span id="sub_total">{{ number_format((str_replace(',','',\Cart::subtotal(0))),0,",",".") }}</span>  đ
                                                            </div>
                                                        </div>
                                                        <div class="bill_row">
                                                            <div class="text_right">Phí vận chuyển (tạm tính):</div>
                                                            <div class="col text_bill">
                                                                <span id="ship_pay">Chưa rõ</span>
                                                                <input type="hidden" name="shipping" id="shipping" value="0">
                                                            </div>
                                                        </div>
                                                        <div class="bill_row cod">
                                                            <div class="text_right">COD :</div>
                                                            <div class="col text_bill">
                                                                <span id="cod">Chưa rõ</span>
                                                                <input type="hidden" name="cod" id="cod_val" value="0">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="bill_row total_order">
                                                        <div class="text_right">Tổng tiền:</div>
                                                        <div class="col text_bill">
                                                            <span id="total_cart">{{ number_format((str_replace(',','',\Cart::subtotal(0))),0,",",".") }}</span> đ
                                                            <input type="hidden" name="total_money" id="ct_total" value="800000">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix clearfix-60"></div>
                                            <div class="row_item checkout_finish" style="margin-right: 10px">
                                                <button type="submit" class="btn btn-default" style="border:none;border-radius:0;background:#008A97;color:#ffffff">Gửi đơn hàng</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="banner_x">
                                <img src="https://phonglien.vn/upload/img/banner/may_bat_vit11111.jpg" class="w_100 thumbnail" alt="qc" title="qc">
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
<script>
    function getdistrict(source){
        var province = $(source).val();
        $('#district').html('');
        // get info
        $.ajax({
            url: '/get_districts',
            type: 'get',
            dataType: 'html',
            data: {province: province},
            success : function(result){
                $('#district').html(result);
            }
        })

    }
    function get_ward(source){
        var province = $(source).val();
        $('#li_check_district').html('');
        // get info
        $.ajax({
            url: '/get_ward_dt',
            type: 'get',
            dataType: 'html',
            data: {province: province},
            success : function(result){
                $('#li_check_district').html(result);
            }
        });

    }

</script>
@endsection