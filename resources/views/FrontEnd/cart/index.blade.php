@extends('FrontEnd.layouts.master')
@section('title', 'Giỏ hàng - '.  $setting->st_title)
@section('keywords', 'Giỏ hàng - '.  $setting->st_title)
@section('description', 'Giỏ hàng - '.  $setting->st_title)

@section('main-content')
    <div id="main">
        <div class="breadcrumb-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 menu-left nav-child">
                         @include('FrontEnd.layouts.menu_left')
                    </div>
                    <div class="col-md-9 slide-right">
                        <div class="col-md-9">
                            <ol class="breadcrumb">
                                <li><a href="{{ url('/') }}"><i class="fa fa-home"></i></a></li>
                                <li class="active">Giỏ hàng </li>
                            </ol>
                        </div>
                        <div class="clearfix"></div>

                    </div>



                </div>
            </div>
        </div>

        <div class="pt-20">
            <div class="container">
                <div class="row">
                    <form action="{{ route('cart.update') }}" method="POST">
                    <div class="col-sm-12">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>STT</th>
                                <th style="width: 20%">Tên Sản Phẩm </th>
                                <th>Hình Ảnh</th>
                                <th> Giá </th>
                                <th> Số Lượng </th>
                                <th> Thành Tiền </th>
                                <th> Thao Tác </th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach( $cart as $k => $ca)
                            <tr>
                                <td>{{ $ca->id }}</td>
                                <td>{{ $ca->name }}</td>
                                <td>
                                    <img src="{{ asset('uploads/images/Products/'.$ca->options->hinhanh) }}" alt="" class="img img-thumbnail" style="width: 80px;height: 50px">
                                </td>
                                <td>{{ formatPrice($ca->price) }} VNĐ</td>
                                <td>
                                    <input type="hidden" name="rowID[]" value="{{ $ca->rowId }}">
                                    <input type="number" value="{{ $ca->qty }}" name="qty[]" min="1" style="width: 40px" class="form-control">
                                </td>
                                <td>{{ formatPrice($ca->price * $ca->qty) }} VNĐ</td>
                                <th>
                                    <a href="{{ route('cart.delete',$ca->rowId) }}" class="btn btn-xs btn-danger remove-item-cart"> Huỷ Bỏ  </a>
                                </th>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                        <div class="col-sm-12" style="text-align: right">
                            <p> Tổng tiền : {{ number_format((str_replace(',','',\Cart::subtotal(0))),0,",",".") }} ₫</p>
                        </div>
                        <div class="col-sm-12 pull-right" style="text-align: right">
                            <input type="hidden" value="{{ csrf_token() }}" name="_token">
                            <input class="btn btn-success"  type="submit"  value="Cập nhật"/>
                            <a  class="btn btn-success" href="/">Tiếp tục mua hàng</a>
                            <a  class="btn btn-success" href="{{ route('cart.pay') }}">Thanh Toán</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection