<html lang="en-US">
<head>
    <title>Dưa lưới TÂN HÙNG</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Favicon -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Asap:400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lobster&amp;subset=cyrillic,cyrillic-ext,latin-ext,vietnamese"
          rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Prata" rel="stylesheet">
    <!-- Style CSS -->
    <link rel="stylesheet" href="{{asset('site_asset/css/owl.carousel.css')}}">
    <link rel="stylesheet" href="https://vineco.net.vn/public/vineco/css/vinhome-logo.css">
    <link rel="stylesheet" href="{{asset('site_asset/css/animate.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('site_asset/css/style.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('site_asset/css/responsive.css')}}"/>
</head>
<body>
<div class="wrapper">
    <div id="fb-root"></div>
    <script>(function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v3.0&appId=318314091987717&autoLogAppEvents=1';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>

    @include('FrontEnd.layouts.header')

    <main id="main" class="site-main">
        <div class="main-slider">
            <div class="slider-home owl-carousel">
                <div class="item">
                    <a href="">
                        <img src="{{asset('images/banner 03.png')}}" alt="VinEco">
                    </a>
                </div>
                <div class="item">
                    <a href=""><img src="{{asset('images/banner.jpg')}}" alt="VinEco">
                    </a>
                </div>
                <div class="item">
                    <a href="">
                        <img src="{{asset('images/banner 03.png')}}" alt="VinEco">
                    </a>
                </div>
            </div>
            <div class="posive-slider">
                <div class="container">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="logo">
                                <a href="/"><img src="{{asset('images/logo.png')}}" alt=""></a>
                            </div>
                        </div>
                        <div class="col-md-10">
                            <div class="menu-menu">
                                <nav class="main-menu">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12">
                                            <div class="memu-left">
                                                <ul>
{{--                                                    <li><a href="/kiem-soat">Kiểm soát chất lượng</a>--}}
{{--                                                        <ul></ul>--}}
{{--                                                    </li>--}}
                                                    <li><a href="/news">Tin tức</a>
                                                        <ul></ul>
                                                    </li>
                                                    <li><a href="/lien-he">Liên hệ</a>
                                                        <ul></ul>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </nav>
                                <!-- .main-menu -->
                                <nav class="main-menu2">
                                    <ul>
                                        <li><a href="/gioi-thieu">GIỚI THIỆU</a>
                                            <ul></ul>
                                        </li>
                                        <li><a href="/san-pham">SẢN PHẨM</a>
                                            <ul></ul>
                                        </li>
                                        <li><a href="/cong-nghe">CÔNG NGHỆ</a>
                                            <ul></ul>
                                        </li>
{{--                                        <li><a href="/nong-truong">NÔNG TRƯỜNG</a>--}}
{{--                                            <ul></ul>--}}
{{--                                        </li>--}}
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @yield('main-content')
    </main>

@include('FrontEnd.layouts.footer')

<!--end footer-->

</div>
<script type="text/javascript" src="{{asset('site_asset/js/jquery-1.10.2.js')}}"></script>
<script type="text/javascript" src="{{asset('site_asset/js/bootstrap.min.js')}}"></script>
<!-- orther script -->
<script type="text/javascript" src="{{asset('site_asset/js/owl.carousel.min.js')}}"></script>
<script type="text/javascript" src="{{asset('site_asset/js/jquery.dd.min.js')}}"></script>

<script type="text/javascript" src="{{asset('site_asset/js/wow.min.js')}}"></script>
<script type="text/javascript" src="{{asset('site_asset/js/jquery-validate.js')}}"></script>
<script type="text/javascript" src="{{asset('site_asset/js/main.js')}}"></script>
<script type="text/javascript" src="{{asset('site_asset/js/contact.js')}}"></script>

<script type="text/javascript">
    jQuery(function () {
        $('.nav-donghanh').owlCarousel({
            loop: true,
            margin: 34,
            dots: false,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 2
                },
                1000: {
                    items: 4
                }
            }
        });
        $('.nav-technology').owlCarousel({
            loop: true,
            margin: 40,
            nav: true,
            dots: false,
            responsive: {
                0: {
                    items: 2
                },
                600: {
                    items: 3
                },
                1000: {
                    items: 6
                }
            }
        });
    });
</script>
<script>
    $(function () {
        $('#select').on('change', function () {
            var url = $(this).val(); // get selected value
            if (url) { // require a URL
                window.location = url; // redirect
            }
            return false;
        });
    });
</script>
<style>
    .owl-nav{
        display: none;
    }
</style>
</body>
</html>