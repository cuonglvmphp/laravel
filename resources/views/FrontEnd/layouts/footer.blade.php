<footer id="footer" class="site-footer">
    <div class="container">
        <div class="logo-footer">
            <a href=""><img src="{{asset('images/logo-green.png')}}" alt=""></a>
        </div>
        <div class="top-footer wow fadeInUp">
            <div class="row">
                <div class="col-md-5 col-sm-5 col-xs-12">
                    <div class="item">
                        <div class="icon"><i class="fa fa-map-marker"></i></div>
                        <div class="noidung"><p>Số Nhà 382 Đường Trần Phú, Thị Trấn Hương Khê, Tỉnh Hà Tĩnh</p></div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="item ">
                        <div class="icon"><i class="fa fa-phone"></i></div>
                        <div class="noidung nd-phone">MR Hùng: 0349979888<br>MS Tân: 0357997888</div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-3 col-xs-12">
                    <div class="item">
                        <div class="icon"><i class="fa fa-envelope"></i></div>
                        <div class="noidung nd-phone">dualuoitanhung@gmail.com</div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-botton">
            <div class="row">
                <div class="col-md-5 col-sm-12 col-xs-12">
                    <div class="item1 wow bounceInLeft">
                        <div class="share">
                            <h4>Mạng xã hội</h4>
                            <ul>
                                <li><a href="https://www.facebook.com/lehung.votan" target="_blank"><img
                                                src="https://vineco.net.vn/public/vineco/images/fb.png" alt=""></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-8 col-xs-12">
                    <div class="item1 wow fadeInUp">
                        <h3 class="title">
                            Sản phẩm
                        </h3>
                        <div class="nav-item1">
                            <ul>
                                <li><a href="/danh-muc/san-pham-hot">Dưa lưới</a></li>
                                <li><a href="/danh-muc/rau-mam">Dưa Chuột Nhật</a></li>
                                <li><a href="/danh-muc/cay-duoc-lieu">Thủy canh</a></li>
                                <li><a href="/danh-muc/rau-an-la">Bưởi Phúc Trạch</a></li>
                                <li><a href="/danh-muc/rau-an-than-hoa">Ổi Nữ Hoàng</a></li>
                                <li><a href="/danh-muc/rau-an-qua">Dưa Hấu</a></li>
                        </div>
                    </div>
                </div>
            </div>
        <div class="copy-right">
            Copyright © 2020 by DuaLuoiTanhung. All rights reserved.| Designed by CuongLVM</a>
        </div>
    </div>
    <!-- .container -->
</footer>