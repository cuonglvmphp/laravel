<div id="vnt-footer" style="background: #785743">
    <!-- box mail -->
    @include('FrontEnd.widget.box-mail')

    <div class="container vnt-wrapper">
        <div class="row menu-footer">
            <div class="col-md-6">
                <div style="width:100%;text-align:center;padding:20px">
                     <img src="{{ asset('images/logo.png') }}" style="max-width:200px">
                </div>
                <h3 class="title-info-mx05">
                    <span style="color: rgb(8, 82, 151);"><span style="font-size: 12px;">TP. Hà Nội</span></span>
                </h3>
                <ul class="list-address">
                    <li>
                        <i class="fa fa-home" aria-hidden="true"></i>
                        <span><label>Văn phòng:</label> Số 18 dãy B, Khu Hà Trì 5, P.Hà Cầu, Q.Hà Đông, TP. Hà Nội</span>
                    </li>
                    <li>
                        <i class="fa fa-bar-chart" aria-hidden="true"></i>
                        <span><label>Nhà máy:</label> KCN Khai Sơn, H.Thuận Thành, TP.Bắc Ninh</span>
                    </li>
                     <li>
                        <i class="fa fa-phone" aria-hidden="true"></i>
                        <span><label>Hotline:</label> Mr Dũng 0945.109.099</span>
                    </li>
                    <li>
                        <i class="fa fa-envelope" aria-hidden="true"></i>
                        <span><label>CSKH:</label> hoaanpro@gmail.com</span>
                    </li>
                    <li>
                        <i class="fa fa-phone" aria-hidden="true"></i>
                        <span><label>Website:</label> {{ url('/') }}</span>
                    </li>
                </ul>
            </div>
            <div class="col-md-6 box-maps" style="padding: 0">
                {!! $more_st->ms_maps !!}
            </div>
        </div>
    </div>
    <div class="menu_copyright">
        <div class="text-center">
            <div class="copyright" style="color:#fff">Copyright © 2018 
                <strong><a href="http://www.maxad.vn" target="_blank" rel="dofollow" style="color:#fff;font-weight:600">Maxad.vn</a> </strong>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</div>

