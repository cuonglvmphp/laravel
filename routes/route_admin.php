<?php

//$routerAdmin = env('URL_ADMIN', '404');
//Route::get($routerAdmin, function() {
//	if (Auth::guard('admin')->check()) { return redirect('mx-admin'); }
//	else { return redirect('mx-login'); }
//});



Route::group([ 'prefix' => 'mx-login' , 'namespace' => 'BackEnd', 'middleware' => ['web', 'CheckLogin'] ], function() {
   	Route::get('/', 'LoginController@login');
	Route::post('/', 'LoginController@postlogin');
});

//'middleware' =>  ['web', 'CheckLogout']
Route::group([ 'prefix' => 'mx-admin', 'namespace' => 'BackEnd', 'middleware' => ['web', 'CheckAdmin'] ], function()
{
    Route::get('email-dang-ky', 'PageController@nhanDK');
    Route::get('xoa-email/{id}', 'PageController@xoaNhanDK');
    Route::get('yeu-cau-goi-lai', 'PageController@phoneCall');
    Route::get('phone-call-back/{id}', 'PageController@xoaphoneCall');
    
    
	Route::get('/logout', 'LoginController@logout');
	Route::get('/', 'PageController@dashboard');
	Route::get('/cau-hinh-chung', 'PageController@settingWebsite');
	Route::post('/cau-hinh-chung', 'PageController@postSettingWebsite');

	Route::get('tai-khoan', 'UserController@accAdmin');
	Route::get('sua-tai-khoan', 'UserController@editAccAdmin');
	Route::post('sua-tai-khoan', 'UserController@updateAccAdmin');

	Route::get('don-hang','TransactionsController@index')->name('AdminTransactionIndex');
	Route::get('delete/{id}','TransactionsController@delete')->name('AdminTransactionDelete');
	Route::get('view/{id}','TransactionsController@view')->name('AdminTransactionDetail');
	Route::get('sua-don-hang/{id}','TransactionsController@edit');
	Route::post('sua-don-hang/{id}','TransactionsController@update');

	Route::get('/binh-luan','CommentController@index')->name('AdminCommentIndex');
    Route::get('/bl/{id}','CommentController@delete')->name('AdminCommentDelete');

	Route::get('/quan-ly-thanh-vien', 'UserController@listUsers');
	Route::get('/them-thanh-vien', 'UserController@addUser');
	Route::post('/them-thanh-vien', 'UserController@postAddUser');
	Route::get('/sua-thanh-vien/{user_id}', 'UserController@editUser');
	Route::post('/sua-thanh-vien/{user_id}', 'UserController@postEditUser');
	Route::get('/xoa-thanh-vien/{user_id}', 'UserController@deleteUser');

	Route::get('/ban-quan-tri', 'UserController@listAdmin');
	Route::get('/ban-quan-tri/them', 'UserController@addAdmin');
	Route::post('/ban-quan-tri/them', 'UserController@postAddAdmin');
	Route::get('/ban-quan-tri/sua/{user_id}', 'UserController@editAdmin');
	Route::post('/ban-quan-tri/sua/{user_id}', 'UserController@postEditAdmin');
	Route::get('/ban-quan-tri/xoa/{user_id}', 'UserController@deleteAdmin');

	// Danh Mục
	Route::get('/danh-muc', 'CategoryController@category');
	Route::get('/them-danh-muc', 'CategoryController@addCategory');
	Route::post('/them-danh-muc', 'CategoryController@postAddCategory');
	Route::get('/sua-danh-muc/{cate_id}', 'CategoryController@editCategory');
	Route::post('/sua-danh-muc/{cate_id}', 'CategoryController@postEditCategory');
	Route::get('/xoa-danh-muc/{cate_id}', 'CategoryController@deleteCategory');

	// Thương Hiệu
	Route::get('/thuong-hieu', 'ThuongHieuController@ThuongHieu');
	Route::get('/them-thuong-hieu', 'ThuongHieuController@addThuongHieu');
	Route::post('/them-thuong-hieu', 'ThuongHieuController@postAddThuongHieu');
	Route::get('/sua-thuong-hieu/{cate_id}', 'ThuongHieuController@editThuongHieu');
	Route::post('/sua-thuong-hieu/{cate_id}', 'ThuongHieuController@postEditThuongHieu');
	Route::get('/xoa-thuong-hieu/{cate_id}', 'ThuongHieuController@deleteThuongHieu');

	// Xuất Xứ
	Route::get('/xuat-xu', 'XuatXuController@XuatXu');
	Route::get('/them-xuat-xu', 'XuatXuController@addXuatXu');
	Route::post('/them-xuat-xu', 'XuatXuController@postAddXuatXu');
	Route::get('/sua-xuat-xu/{cate_id}', 'XuatXuController@editXuatXu');
	Route::post('/sua-xuat-xu/{cate_id}', 'XuatXuController@postEditXuatXu');
	Route::get('/xoa-xuat-xu/{cate_id}', 'XuatXuController@deleteXuatXu');

	Route::get('/logo', 'PageController@logo');
	Route::post('/logo', 'PageController@postLogo');

	Route::get('/quan-ly-ma-nhung', 'EmbedController@settingEmCode');
	Route::post('/quan-ly-ma-nhung', 'EmbedController@postEmbedCode');

	Route::get('/san-pham/them-moi', 'ProductsController@addProduct');
	Route::post('/san-pham/them-moi', 'ProductsController@postAddProduct');
    Route::get('/san-pham', 'ProductsController@product');
    Route::get('/sua-san-pham/{pro_id}', 'ProductsController@editProduct');
	Route::post('/sua-san-pham/{pro_id}', 'ProductsController@postEditProduct');
	Route::get('/xoa-san-pham/{pro_id}', 'ProductsController@deleteProduct');

	Route::get('/slider', 'PageController@banner');
	Route::post('/slider', 'PageController@setBanner');

	Route::get('/video/dang-moi', 'PageController@video');
	Route::post('/video/dang-moi', 'PageController@postVideo');
	Route::get('/video', 'PageController@listVideo');
	Route::post('/update-video/{id_video}', 'PageController@updateVideo');
	Route::get('/video/xoa/{id_video}', 'PageController@deleteVideo');

	Route::get('mo-rong', 'PageController@widgetSetting');
	Route::post('mo-rong', 'PageController@pWidgetSetting');
	
	Route::get('phi-van-chuyen', 'PageController@vanchuyen');
	Route::post('phi-van-chuyen', 'PageController@updatevanchuyen');

	/**  Viết bài mới  **/
	Route::get('/viet-bai-moi', 'NewsController@creatNews');
	Route::post('/viet-bai-moi', 'NewsController@postCreatNews');
	Route::get('/quan-ly-bai-viet', 'NewsController@listNews');
	Route::get('/xem-bai-viet/{news_id}', 'NewsController@viewNews');
	Route::get('/sua-bai-viet/{news_id}', 'NewsController@editNews');
	Route::post('/sua-bai-viet/{news_id}', 'NewsController@postEditNews');
	Route::get('/xoa-bai-viet/{news_id}', 'NewsController@deleteNews');

	Route::get('pages', 'QLPagesController@listPages');
	Route::get('cap-nhat-noi-dung/{id}', 'QLPagesController@updateContentPages');
	Route::post('cap-nhat-noi-dung/{id}', 'QLPagesController@updateContent');
	
});