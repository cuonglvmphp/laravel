<?php











@include('route_admin.php');

/*
|--------------------------------------------------------------------------
| Front-End
|--------------------------------------------------------------------------
|
| 
|
*/
Route::group(['prefix' => '/', 'namespace' => 'FrontEnd'], function()
{
    Route::get('get_districts', 'ShoppingCartController@getDis' );
    Route::get('get_ward_dt', 'ShoppingCartController@getward' );
    
    Route::get('/', 'PagesController@home');
    Route::get('tim-kiem', 'PagesController@search');
 	Route::get('/danh-muc/{id}/{slug}.html', ['as' => 'getcategory', 'uses'=>'PagesController@getCategory']);
 	Route::get('/san-pham/{id}-{slug}.html', ['as' => 'getDetails', 'uses'=>'PagesController@getDetails']);
 	Route::post('/comment','PagesController@save_comment')->name('save_comment');
 	
 	Route::post('loc-san-pham', 'PagesController@locSP');

    Route::post('/dang-ky-email','PagesController@dkEmail')->name('dang-ky-nhan-email');
    Route::post('/mobile-callback','PagesController@phoneCallBack');
    
    Route::get('tin-tuc.html', 'PagesController@list_news');
 	Route::get('tin-tuc/{id}/{slug}.html', 'PagesController@details_news');
    Route::get('lien-he', 'PagesController@contact');
    Route::get('cong-nghe', 'PagesController@technology');
    Route::get('gioi-thieu', 'PagesController@introduce');
    Route::get('san-pham', 'PagesController@product');
    Route::get('lien-he', 'PagesController@contact');
    Route::get('loi-ich-dua-luoi', 'PagesController@news1');
    Route::get('ky-thuat-trong-dua', 'PagesController@news2');
    Route::get('cong-nghe-tuoi-israel', 'PagesController@news3');
    Route::get('dua-luoi-nha-mang', 'PagesController@news4');

    Route::group(['prefix' => 'cart'], function (){
        //add
        Route::post('/add','ShoppingCartController@add')->name('cart.add');
        Route::post('/add-return','ShoppingCartController@addReturn')->name('cart.add-return');

        Route::get('/','ShoppingCartController@index')->name('cart.index');
        Route::post('/','ShoppingCartController@update')->name('cart.update');

        Route::get('/pay','ShoppingCartController@getPay')->name('cart.pay');
        Route::post('/pay','ShoppingCartController@postPay');

        Route::get('/delete/{id}','ShoppingCartController@delete')->name('cart.delete');
    });

    Route::post('dang-nhap', 'PagesController@postlogin');
    Route::get('thoat', 'PagesController@logout');
    Route::post('dang-ky', 'PagesController@dangKy');

    Route::get('pg{id}-{slug}.html', 'PagesController@morePages');
});