<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $admins = array(
            [
                'name'              => 'admin',
                'email'             => 'admin12345@gmail.com',
                'password'          => 'admin12345'
            ]
        );

        foreach ($admins as $value)
        {
            \Illuminate\Support\Facades\DB::table('sp_admin')->insert([
                'name' 			=> $value['name'],
                'email' 		=> $value['email'],
                'password' 		=> bcrypt($value['password']),
            ]);
        }
    }
}
