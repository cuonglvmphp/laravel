<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction', function (Blueprint $table) {
            $table->increments('id');
            $table->string("tst_email")->nullable()->comment(' email nguoi dung  ');
            $table->string("tst_name")->nullable()->comment(' ten nguoi dung  ');
            $table->char("tst_phone")->nullable()->comment(' so dien thoai nguoi dung  ');
            $table->string("tst_address")->nullable()->comment(' dia chi  nguoi dung  ');
            $table->integer("tst_total")->nullable()->default(0)->comment(' Tong tien nguoi dung  ');
            $table->string("tst_messages")->nullable()->default(0)->comment(' ghi chu  ');
            $table->tinyInteger("tst_status")->nullable()->default(0)->comment("  trang thai don hang ");
            $table->integer("tst_admin_id")->nullable()->comment(" admin xu ly");
            $table->date("tst_date_payment")->nullable()->comment(" ngay thanh toan ");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction');
    }
}
