<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('od_transaction_id')->unsigned()->comment(" khoa ngoai lien ket bang don hang");
            $table->integer('od_product_id')->unsigned()->comment(" khoa ngoai bang san pham");
            $table->tinyInteger('od_qty')->unsigned()->comment(" so luong sp ung vs tung sp cua don hang");
            $table->integer('od_price')->comment(" gia sp o thoi diem mua hang ");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
