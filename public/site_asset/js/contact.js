(function ($) {
    "use strict";
    $(document).ready(function () {
        $('#contact_form').validate({
            rules: {
                fullname: {
                    required: true
                },
                phone: {
                    required: true,
                    number: true,
                    minlength: 10,
                    maxlength: 11
                },
                address: {
                    required: true
                },
                email: {
                    required: true,
                    email : true
                },
                message:{
                    required: true
                },
                CaptchaCode:{
                    required: true
                }
            },
            messages: {
                fullname: {
                    required: 'Vui lĂ²ng nháº­p Ä‘áº§y Ä‘á»§ há» vĂ  tĂªn'
                },
                phone: {
                    required: 'Vui lĂ²ng nháº­p sá»‘ Ä‘iá»‡n thoáº¡i',
                    number: 'Vui lĂ²ng nháº­p Sá»‘ Ä‘iá»‡n thoáº¡i lĂ  chá»¯ sá»‘',
                    minlength: 'Vui lĂ²ng nháº­p Sá»‘ Ä‘iá»‡n thoáº¡i tá»‘i thiá»ƒu 10 chá»¯ sá»‘',
                    maxlength: 'Vui lĂ²ng nháº­p Sá»‘ Ä‘iá»‡n thoáº¡i tá»‘i Ä‘a 11 chá»¯ sá»‘',
                },
                address: {
                    required: 'Báº¡n cáº§n nháº­p Ä‘á»‹a chá»‰ liĂªn láº¡c'
                },
                email: {
                    email: 'Vui lĂ²ng nháº­p email Ä‘Ăºng Ä‘á»‹nh dáº¡ng',
                    required: 'Vui lĂ²ng nháº­p email'
                },
                message: 'Vui lĂ²ng nháº­p ná»™i dung',
                CaptchaCode: 'Vui lĂ²ng nháº­p captcha'
            }
        });
        $('#contact_form1').validate({
            rules: {
                fullname: {
                    required: true
                },
                phone: {
                    required: true,
                    number: true,
                    minlength: 10,
                    maxlength: 11
                },
                address: {
                    required: true
                },
                cong_ty: {
                    required: true
                },
                email: {
                    required: true,
                    email : true
                },
                message:{
                    required: true
                },
                CaptchaCode:{
                    required: true
                }
            },
            messages: {
                fullname: {
                    required: 'Vui lĂ²ng nháº­p Ä‘áº§y Ä‘á»§ há» vĂ  tĂªn'
                },
                phone: {
                    required: 'Vui lĂ²ng nháº­p sá»‘ Ä‘iá»‡n thoáº¡i',
                    number: 'Vui lĂ²ng nháº­p Sá»‘ Ä‘iá»‡n thoáº¡i lĂ  chá»¯ sá»‘',
                    minlength: 'Vui lĂ²ng nháº­p Sá»‘ Ä‘iá»‡n thoáº¡i tá»‘i thiá»ƒu 10 chá»¯ sá»‘',
                    maxlength: 'Vui lĂ²ng nháº­p Sá»‘ Ä‘iá»‡n thoáº¡i tá»‘i Ä‘a 11 chá»¯ sá»‘',
                },
                address: {
                    required: 'Báº¡n cáº§n nháº­p Ä‘á»‹a chá»‰ liĂªn láº¡c'
                },
                email: {
                    email: 'Vui lĂ²ng nháº­p email Ä‘Ăºng Ä‘á»‹nh dáº¡ng',
                    required: 'Vui lĂ²ng nháº­p email'
                },
                cong_ty: {
                    required: 'Vui lĂ²ng nháº­p tĂªn cĂ´ng ty cá»§a báº¡n'
                },
                message: 'Vui lĂ²ng nháº­p ná»™i dung',
                CaptchaCode: 'Vui lĂ²ng nháº­p mĂ£ captcha bĂªn cáº¡nh'
            }
        });
        $('#email_sub').validate({
            rules: {
                email_sub: {
                    required: true,
                    email : true
                }
            },
            messages:{
                email_sub: {
                    email: 'Vui lĂ²ng nháº­p email Ä‘Ăºng Ä‘á»‹nh dáº¡ng',
                    required: 'Vui lĂ²ng nháº­p email'
                }
            }
        });
        $(".icon-dangky").click(function(){

            $( "#email_sub" ).submit();
        });

    });
})(jQuery);