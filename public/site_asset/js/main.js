(function($) {
    "use strict";
    $(document).ready(function() {
        $('.slider-top').owlCarousel({
            loop:true,
            center:true,
            margin:10,
            nav:true,
            slideSpeed: 3000,
            paginationSpeed: 5000,
            dots:false,
            navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],

            responsive:{
                0:{
                    items:2
                },
                600:{
                    items:5
                },
                1000:{
                    items:11
                }
            }

        });
        $(".click-vin").click(function(){
            $(".nav-vin").toggle();
        });
        $("#countries").msDropdown();
        var nsdetail = $('.nav-slider-detail').owlCarousel({
            items:7,
            loop:false,
            center:false,
            margin:10,
            nav:false,
            dots:false,
            URLhashListener:true,
            autoplayHoverPause:true,
            autoplayTimeout:9000,
            slideSpeed: 3000,
            paginationSpeed: 3000,
            autoplaySpeed: 3000,
            startPosition: 'URLHash'
        });
        $('.nav-slider-detail a').on('click',function() {
            $('.nav-slider-detail .owl-item').removeClass('onactive');
            $(this).parent().addClass('onactive');
        });
        $('.slide-product-detail').owlCarousel({
            items:1,
            center:true,
            loop:false,
            nav:false,
            dots:false,
            margin:10,
            URLhashListener:true,
            autoplayHoverPause:true,
            autoplayTimeout:9000,
            slideSpeed: 3000,
            paginationSpeed: 3000,
            autoplaySpeed: 3000,
            startPosition: 'URLHash'
        });


        $('.slider-doitac').owlCarousel({
            loop:true,
            margin:10,
            nav:true,
            dots:false,

            navText: ['<i class="fa fa-chevron-left"></i>', '<i class="fa fa-chevron-right"></i>'],
            responsive:{
                0:{
                    items:2
                },
                600:{
                    items:3
                },
                1000:{
                    items:6
                }
            }
        });
        var carousel = $(".slider-top-product");
        carousel.owlCarousel({
            loop : true,
            margin:0,
            navText: ['<i class="fa fa-chevron-left"></i>', '<i class="fa fa-chevron-right"></i>'],
            nav : true,
            dots : false,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:2
                },
                1000:{
                    items:6
                }
            }
        });


        // checkClasses();
        // carousel.on('load', function(event) {
        // checkClasses();
        // });

        // function checkClasses(){
        // var total = $('.slider-top-product .owl-stage .owl-item.active').length;

        // $('.slider-top-product .owl-stage .owl-item').removeClass('onactive');

        // $('.slider-top-product .owl-stage .owl-item.active').each(function(index){
        // if (index === 0) {
        // $(this).addClass('onactive');
        // }
        // });
        // }

        // $('.slider-top-product').on('click', '.owl-item', function(e) {
        // e.preventDefault();
        // $('.slider-top-product .owl-stage .owl-item').removeClass('onactive');
        // $(this).addClass('onactive');
        // var idclick = $(this).find('a').attr('href');
        // $('.nav-slider-product ul li').removeClass('active');
        // $(idclick).find('li').first().addClass('active');
        // $('.tab-content .main-product').removeClass('active');
        // $(idclick + 'a').addClass('active');
        // });
        $('.slider-qt').owlCarousel({
            loop:true,
            margin:10,
            dots:true,
            nav:false,
            items:1

        });
        $(".toggle-menu").click(function (e) {
            e.preventDefault();
            $(this).toggleClass("active");
            $(this).parent().find('.nav-toggle-menu').toggleClass('open');
        });
        $( '.submenu' ).each(function() {
            $( this ).parent().addClass( 'has-child ' ).find( '> a' ).append( '<span class="arrow"><i class="fa fa-angle-right"></i></span>' );
        });
        $( '.nav-toggle-menu .arrow' ).on( 'click', function(e) {
            e.preventDefault();
            $( this ).parents( 'li' ).find( '> .sub-menu' ).slideToggle( 'fast' );
        });
        $( '.main-menu-mobile>li> a>.arrow' ).on( 'click', function(e) {
            e.preventDefault();
            $( this ).parents( 'li' ).find( '> .submenu' ).slideToggle( 'fast' );
            // $(this).find(".main-menu-mobile>li> a>.arrow >i").addClass('fa fa-angle-down').removeClass('fa fa-angle-down');
            $(this).toggle();
            $(this).toggleClass('active').css("display","block");
            console.log(this);
        });
        $(".click-search").click(function (e) {
            e.preventDefault();

            $(this).parent().find('.nav-search').toggleClass('open1');
        });
        //   $(this).on("click", ".click-icon1", function() {
        //   $(this).parent().find(".koh-faq-answer").toggle();
        //   $(this).find(".fa").toggleClass('active');
        // });

        $(".set > a").on("click", function(e) {
            e.preventDefault();
            if ($(this).hasClass("active")) {
                $(this).removeClass("active");
                $(this)
                    .siblings(".content")
                    .slideUp(200);
                $(".set > a i")
                    .removeClass("fa-minus")
                    .addClass("fa-plus");
            } else {
                $(".set > a i")
                    .removeClass("fa-minus")
                    .addClass("fa-plus");
                $(this)
                    .find("i")
                    .removeClass("fa-plus")
                    .addClass("fa-minus");
                $(".set > a").removeClass("active");
                $(this).addClass("active");
                $(".content").slideUp(200);
                $(this)
                    .siblings(".content")
                    .slideDown(200);
            }
        });






        $('.slider-home').owlCarousel({
            loop:true,
            margin:10,
            nav:false,
            dots:true,
            items:1,
            autoplay:true,
            autoplayTimeout:9000,
            slideSpeed: 3000,
            paginationSpeed: 3000,
            autoplaySpeed: 3000,
        });





        new WOW().init();


    });
    $(window).load(function() {
        $('.nav-slider-detail .owl-item').first().addClass('onactive');
    });
    $(function(){
        // bind change event to select
        $('#countries').on('change', function () {
            var url = $(this).val(); // get selected value
            if (url) { // require a URL
                window.location = url; // redirect
            }
        });
    });
})(jQuery);
